<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
	<meta name="keywords" content="@if (isset($meta_tags)) {{$meta_tags}} @else {{ "" }} @endif">
	<meta name="description" content="@if (isset($meta_description)) {{$meta_description}} @else {{ "" }} @endif">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if (isset($title)) {{$title}} @else {{ env('APP_NAME') }} @endif</title>

    <!-- Scripts -->

	<link rel="shortcut icon" href="{{ asset('images\favicon.ico') }}">

	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/flexslider.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('layerslider/css/layerslider.css') }}">
	<link rel="stylesheet" href="{{ asset('layerslider/css/style.css') }}">

	@yield('css')
	@stack('style')

</head>
<body class="body-class index_1 home1">
	<div id="homebg">
		<div id="wrapper">
	<div id="app"></div>
    @yield('content')
	<footer class="footer">
			<!--Start Footer Top-->
			<div class="footer-top">
				<!--Start Container-->
				<div class="container">
					<div class="row">
						{{-- <div class="col-lg-3 col-sm-6">
							<div class="footer-about">
								<img class="f-logo img-fluid" src="{{ asset('images/travelshopturkey-logo.png') }}" alt="Image">
								@if ($setting['footer_description']!="")
								<p>{{ $setting['footer_description'] }}</p>
								@endif

								<h3 class="title">@lang('site.footer_follow')</h3>
								<ul>
									@if ($setting['facebook']!="")
									<li><a href="{{ $setting['facebook'] }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
									@endif
									@if ($setting['twitter']!="")
										<li><a href="{{ $setting['twitter'] }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
									@endif
									@if ($setting['instagram']!="")
										<li><a href="{{ $setting['instagram'] }}" target="_blank"><i class="fab fa-instagram"></i></a></li>
									@endif
									@if ($setting['googleplus']!="")
										<li><a href="{{ $setting['googleplus'] }}" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
									@endif
									@if ($setting['youtube']!="")
										<li><a href="{{ $setting['youtube'] }}" target="_blank"><i class="fab fa-youtube"></i></a></li>
									@endif
									@if ($setting['linkedin']!="")
										<li><a href="{{ $setting['linkedin'] }}" target="_blank"><i class="fab fa-linkedin"></i></a></li>
									@endif
								</ul>
							</div>
						</div> --}}
						{{-- <div class="col-lg-3 col-sm-4">
							<ul class="footer-pages">
								@if ($bottommenus)
									@foreach ($bottommenus as $menu)

										@if (count($menu->subpages)>0)
											<h4>{{$menu->title}}</h4>
											@foreach ($menu->subpages as $subpage)
												<li><a href="{{ route('a.page',['name'=>str_slug($subpage->title),'id'=>$subpage->id]) }}">{{$subpage->title}}</a></li>
											@endforeach
										@else
											<li><a href="{{ route('a.page',['name'=>str_slug($menu->title),'id'=>$menu->id]) }}">{{$menu->title}}</a></li>
										@endif
									@endforeach
								@endif
							</ul>
						</div>
						<div class="col-lg-3 col-md-4">
							<div class="footer-pages">
								<h4>İletişim</h4>
								<p><strong>Adres:</strong> Esentepe Mah. Büyükdere Cad.<br/>No: 88/A, Şişli/İstanbul <br/>
								<strong>Telefon:</strong> 0212 273 10 16</p>
							</div>
						</div>
						<div class="col-lg-6 col-md-4 text-right">
							<img src="{{ asset('visa-mastercard-logos-feature.png') }}" alt="Visa Master">
						</div> --}}

						{{-- <div class="col-lg-3 col-sm-6">
							<div class="footer-insta">
								<h3>@lang('site.footer_instagram')</h3>

							</div>
						</div> --}}
						{{-- <div class="col-lg-3 col-sm-6">
							<div class="footer-contact-info">
								<h3>@lang('site.footer_contact')</h3>
								<ul>
										<li><a href="#">
											<i class="flaticon-location-pointer"></i>
											{{ $setting['footer_address'] }}</a>
									</li>
										<li><a href="mailto:{{ $setting['footer_email'] }}">
											<i class="flaticon-mail-black-envelope-symbol"></i>
											{{ $setting['footer_email'] }}
											</a>
										</li>
										<li><a href="#">
											<i class="flaticon-phone-receiver"></i>
											{{ $setting['footer_phone'] }}
										</a>
										</li>
									</ul>
							</div>
						</div> --}}
					</div>
				</div>
				<!--End Container-->
			</div>
			<!--End Footer Top-->

			<!--Start Footer Bottom-->
			{{-- <div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="copyright-text">
							<p class="color-white">{{ $setting['footer_copy_right'] }}</a>
							</div>
						</div>
						<div class="col-md-6">
							<div class="copyright-links">
								@if ($bottommenus)
								<ul>
									@foreach ($bottommenus as $pageitem)
									<li>
										<a href="{{ route('a.page',['name'=>str_slug($pageitem->title),'id' => $pageitem->id]) }}">{{$pageitem->title}}</a>
									</li>
                                    @endforeach

								</ul>
								@endif

							</div>
						</div>
					</div>
				</div>
			</div> --}}
			<!--End Footer Bottom-->

			<!--Start ClickToTop-->
			{{-- <div class="totop">
				<a href="#top"><i class="fa fa-arrow-up"></i></a>
			</div> --}}
			<!--End ClickToTop-->
		</footer>
		<!--End Footer-->
		</div>
	</div>
</div>
	<script src="{{ asset('js\app.js') }}"></script>
  <script src="{{ asset('js\flexslider\jquery.flexslider.js') }}"></script>
	<script src="{{ asset('js\inputmask\dist\jquery.inputmask.bundle.js') }}"></script>

	<script src="{{ asset('layerslider\js\greensock.js') }}"></script>
	<script src="{{ asset('layerslider\js\greensock-text.js') }}"></script>
	<script src="{{ asset('layerslider\js\layerslider.kreaturamedia.jquery.js') }}"></script>
	<script src="{{ asset('layerslider\js\layerslider.transitions.js') }}"></script>
	<script>

		$("#homebg").animate({
		  opacity: 100
		},500,function(){});
	  
		</script>
	@yield('js')
	@stack('scripts')
	
</body>
