<header id="mainHeader" class="header">
    <!-- Start Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light p-0 ">
        <div class="container-fluid">
           <div class="row align-items-start hundred-percent">
                <div class="col-md-6  col-6">
                    <a class="navbar-brand" href="{{ route('index') }}">
                        <img class="logo-dark" src="{{ asset('images/logo.png') }} " alt="">
                    </a>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">
                                @if ($topmenus)
                                    @foreach ($topmenus as $pageitem)

                                        @if (count($pageitem->subpages)>0)
                                            <li class="nav-item dropdown @if (in_array($pageitem->id,$activemenu)) active @endif">
                                            <a class="nav-link dropdown-toggle"  href="#" id="down{{ str_slug($pageitem->title) }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $pageitem->title }}</a>

                                            <div class="dropdown-menu" aria-labelledby="down{{ str_slug($pageitem->title) }}">
                                                @foreach ($pageitem->subpages as $subpage)
                                                <a class="dropdown-item" href="{{ route('a.page',['name'=>str_slug($subpage->title),'id'=>$subpage->id]) }}">{{$subpage->title}}</a>
                                                @endforeach
                                            </div>
                                        @elseif($pageitem->page_type_id==8)

                                            <li class="nav-item dropdown norelative @if (in_array($pageitem->id,$activemenu)) active @endif">
                                                <a class="nav-link dropdown-toggle"  href="{{ route('a.page',['name' => str_slug($pageitem->title),'id'=>$pageitem->id]) }}" id="down{{ str_slug($pageitem->title) }}" >{{ $pageitem->title }}</a>
                                                <ul class="topfull">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12 dropdown-menu dropdownfull down{{ str_slug($pageitem->title) }}" aria-labelledby="down{{ str_slug($pageitem->title) }}">
                                                                @foreach ($menucategories as $cat)
                                                                <li class="downlist-item"><a class="menu-link-icon" href="{{route('category',['slug' => $cat->slug])}}">{{$cat->title}}</a>
                                                                    @if ($cat->subcategories)
                                                                        <ul>
                                                                            @foreach ($cat->subcategories as $subcat)
                                                                            <li><a class="" href="{{route('category',['slug' => $subcat->slug])}}">{{$subcat->title}}</a></li>
                                                                            @endforeach
                                                                        </ul>
                                                                    @endif
                                                                @endforeach
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ul>
                                                

                                        @else
                                        <li class="nav-item">
                                            @if ($pageitem->redirect_url!=null)
                                            <a class="nav-link" href="{{ $pageitem->redirect_url }}">{{ $pageitem->title }}</a>
                                            @else
                                            <a class="nav-link" href="{{ route('a.page',['name' => str_slug($pageitem->title),'id'=>$pageitem->id]) }}">{{ $pageitem->title }}</a>
                                            @endif
                                        @endif
                                        </li>
                                    @endforeach
                                @endif
                            </ul>

                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fas fa-bars"></i>
                        </button>
                </div>
                <div class="col-md-6 col-6 align-top">
                    <div class="right-box">
                        <div class="row">

                            <div class="col-lg-3 offset-lg-1 col-sm-6">
                                <form action="#" method="get" class="search-container" >
                                    <div class="row no-gutters align-items-center">

                                        <div class="col search-box">
                                            <input class="" type="search" placeholder="Arama">
                                        </div>

                                    </div>
                                </form>
                            </div>
                            @if ($langs)
                            <div class="col-lg-4 col-sm-6">
                                <div class="lang-box">
                                @foreach ($langs as $item)
                                    <a class="lang-item @if ($lang->id == $item->id) active @endif" href="{{route('lang',['id' => $item->short])}}"> {{ $item->short }}</a>
                                @endforeach
                                </div>
                            </div>
                            @endif

                            <div class="col-lg-4 col-sm-6">
                                <ul class="social-box">
                                    @if ($setting['facebook']!="")
                                    <li><a href="{{ $setting['facebook'] }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    @endif
                                    @if ($setting['twitter']!="")
                                        <li><a href="{{ $setting['twitter'] }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    @endif
                                    @if ($setting['instagram']!="")
                                        <li><a href="{{ $setting['instagram'] }}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                    @endif
                                    @if ($setting['googleplus']!="")
                                        <li><a href="{{ $setting['googleplus'] }}" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                                    @endif
                                    @if ($setting['youtube']!="")
                                        <li><a href="{{ $setting['youtube'] }}" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                    @endif
                                    @if ($setting['linkedin']!="")
                                        <li><a href="{{ $setting['linkedin'] }}" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                                    @endif
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </nav>
    <!-- End Navigation -->
    <div class="clearfix"></div>
</header>
