<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/font-awesome.min.css') }}">

		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/smartadmin-production-plugins.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/smartadmin-production.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/smartadmin-skins.min.css') }}">

		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/smartadmin-rtl.min.css') }}">

		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/demo.min.css') }}">

		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('admin/css/your_style.css') }}">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">
		<link rel="icon" href="{{ asset('admin/img/favicon/favicon.ico') }}" type="image/x-icon">

		<!-- GOOGLE FONT -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<link rel="apple-touch-icon" href="{{ asset('admin/img/splash/sptouch-icon-iphone.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('admin/img/splash/touch-icon-ipad.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('admin/img/splash/touch-icon-iphone-retina.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('admin/img/splash/touch-icon-ipad-retina.png') }}">

		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="{{ asset('admin/img/splash/ipad-landscape.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="{{ asset('admin/img/splash/ipad-portrait.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
        <link rel="apple-touch-startup-image" href="{{ asset('admin/img/splash/iphone.png') }}" media="screen and (max-device-width: 320px)">
		@yield('css')
		@stack('style')


</head>
<body>

    {{-- <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <ul class="navbar-nav mr-auto">

                    </ul>


                    <ul class="navbar-nav ml-auto">

                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div> --}}

    <!-- @include('layouts.admin.menutop')

    @include('layouts.admin.menu') -->

    <div id="main" role="main">
        @include('layouts.admin.breadcrumb')

        @yield('content')
    </div>
    @include('layouts.admin.footer')



    <script src="{{ asset('admin/js/plugin/pace/pace.min.js') }}"></script>

	<script src="{{ asset('admin/js/libs/jquery-2.1.1.min.js') }}"></script>

		<script src="{{ asset('admin/js/libs/jquery-ui-1.10.3.min.js') }}"></script>
		<script src="{{ asset('admin/js/app.config.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') }}"></script>
        <script src="{{ asset('admin/js/bootstrap/bootstrap.min.js') }}"></script>
		<script src="{{ asset('admin/js/notification/SmartNotification.min.js') }}"></script>
		<script src="{{ asset('admin/js/smartwidgets/jarvis.widget.min.js') }}"></script>
        <script src="{{ asset('admin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/sparkline/jquery.sparkline.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/jquery-validate/jquery.validate.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/masked-input/jquery.maskedinput.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/select2/select2.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/bootstrap-slider/bootstrap-slider.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/msie-fix/jquery.mb.browser.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/fastclick/fastclick.min.js') }}"></script>

		<script src="{{ asset('admin/js/demo.min.js') }}"></script>
		<script src="{{ asset('admin/js/app.min.js') }}"></script>

		<script src="{{ asset('admin/js/speech/voicecommand.min.js') }}"></script>
		<script src="{{ asset('admin/js/smart-chat-ui/smart.chat.ui.min.js') }}"></script>
		<script src="{{ asset('admin/js/smart-chat-ui/smart.chat.manager.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/flot/jquery.flot.cust.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/flot/jquery.flot.resize.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/flot/jquery.flot.time.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/flot/jquery.flot.tooltip.min.js') }}"></script>

		<script src="{{ asset('admin/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

		<script src="{{ asset('admin/js/plugin/moment/moment.min.js') }}"></script>
		<script src="{{ asset('admin/js/plugin/fullcalendar/jquery.fullcalendar.min.js') }}"></script>
		<script src="{{ asset("admin/js/laravel.js") }}"></script>

		<script>
				$( document ).ready(function() {
					var status = $("#status-box").data('status');
					console.log(status);
					var msj="";
					var alert = "";
					switch (status) {
						case 0:
							msj="An error occurred. Please try again later.";
							alert ="alert-danger";
							break;
						case 1:
							msj="The operation is successful.";
							alert ="alert-success";
							break;

						default:
							break;
					}
					if (msj!="") {
						$("#status-box").html('<div class="alert '+alert+'" role="alert">'+msj+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
					}
				});
			</script>


		@yield('js')
		@stack('scripts')
</body>
</html>
