@extends('layouts.homeindex')
@section('content')


  @include(template_path_check('/layouts/topmenu'))
  <div style="background-color:white;color:grey;" class=" container-fluid">
@if($product)
<div class="row justify-content-center">
  <div style="padding-top:2vw;padding-bottom:2vw;text-align:right;color:#737373!important;" class="col-10">
  <a style="margin-right:2vw;color:#737373!important;"  href="{{route('index')}}" class="router">@lang('site.home')</a> <p style="margin-right:4px;margin-left:4px;" class="router">/</p> <a style="color:#737373!important;" href="{{route('category',['slug' => $product->categories[0]->slug])}}" class="router">{{$product->categories[0]->title}}</a><p style="margin-right:4px;margin-left:4px;" class="router">/</p><a style="color:#737373!important;" href="#" class="router">{{$product->name}}</a>
  </div>

<div class=" col-11 col-sm-10 col-md-9 row justify-content-center">
  <?php foreach($images as $img){if($img->path)break;}?>
  <div class="col-12 col-md-4">
<img class="prodImage" src="{{$img->path}}"/>
  </div>
  <div class="col-12 col-md-8">
    <h3 class="prodName">{{$product->name}}</h3>

    <p style="display:inline-block;" class="prodProp">@lang('site.product_code'): &nbsp;</p><p style="display:inline-block;" class="prodPropVal">{{$product->name}}</p>

    <br>
    <p class="prodDesc">{{$product->short_description}}</p>
    <br>
    <div class="row">
    <a class="col-6 col-sm-6 col-md-12 col-lg-6 col-xl-12" href="{{$product->pdf}}"><img class="pdfimg" src="/images/pdf.png"><p style="display:inline-block;" class="prodPropVal pdftext"> @lang('site.product_prodtecs')</p></a>
    <br>
    <br>
      <a class="col-6 col-sm-6 col-md-12 col-lg-6 col-xl-12" href="{{$product->family_pdf}}"><img class="pdfimg" src="/images/pdf.png"><p style="display:inline-block;" class="prodPropVal pdftext"> @lang('site.product_cattecs')</p></a>
</div>
  </div>
  <div style="color:grey;margin-top:2rem;" class="col-12">
    <div  class="row justify-content-center">
		@if ($product->product_properties)
			@php
				$propertycount = $product->product_properties()->count();
				@endphp
				<div class="col-12 col-md-6">
		    	<table>
				@foreach ($product->product_properties as $key => $property)
					
					@if (ceil($propertycount/2)==$key)
						</table>
					</div>
					<div class="col-12 col-md-6">
						<table id="secondtable">
					@endif
					

					<tr>
						<td><p class="prodProp">{{ $property->property->name }}</p></td>
						<td><p class="prodPropVal">{{$property->value}}</p></td>
					</tr>
				@endforeach
			</table>
		</div>
		@endif
    
  
  </div>
  <br>
  <p style="margin-left: 5px;display:inline;" class="prodProp">@lang('site.product_detail'): &nbsp;</p><p style="display:inline;padding-bottom: 60px;" class="prodPropVal">{{$product->short_description}}</p>


  </div>
  @else
<div class="row justify-content-center">
  <h2 class="col-10">
    Product not found
  </h2>
  <a href="{{route('index')}}" class="col-10">Home</a>
</div>

@endif

</div>
</div>
</div>
@push('style')
<style>
.prodImage{width:100%;height: auto;}
td, th {
  /* border: 1px solid #dddddd; */
  text-align: left;
  padding: 8px;

}
td p{    vertical-align: middle;
    display: table-cell;margin:0;
  heigth:30px;}
table {
    border-collapse: collapse;
    border-radius: 30px;
    border-style: hidden; /* hide standard table (collapsed) border */
    box-shadow: 0 0 0 1px #fff; /* this draws the table border  */

}

td:nth-child(odd) {
  width: 35%;
}

tr:nth-child(odd) {
  background-color: #e5f0f8;
}
.router{text-align:right;color:#737373!important;font-style:italic;font-size:small;float:right;}
.prodName{font-weight: 200;color:#0070b7;}
.pdfimg{width: 60px;height:60px;margin-bottom: 13px;}
table{width:100%;}
.prodProp, .prodPropVal{display:inline-block;}
.prodProp{font-weight: 600;}
.prodDesc{width: 60%;}
.prodPropVal{color:gray;}
.pdftext{max-width: 150px;
    margin-left: 20px;}

    @media screen
    and (max-width:660px){
    .pdftext{margin-left:7px;}

    }

    @media screen
    and (max-width:765px){
.prodName{
  margin-top:2rem;
}
    table#secondtable{margin-top:3rem;}
    .prodDesc{width:100%;}
    }



</style>

@endpush
@endsection
