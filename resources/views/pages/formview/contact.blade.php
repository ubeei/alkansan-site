{!! Form::open(['url' => url()->full(), 'method' => "POST", "class" =>"smart-form"]) !!}
<div class="row">
    <div class="col-md-12">
        @if (Session::get('form_success'))
            <div class="alert alert-success" role="alert">@lang('site.form_success')<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
        @endif
        @if (Session::get('form_error'))
            <div class="alert alert-danger" role="alert">@lang('site.form_error')<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
        @endif
    </div>
    
    <div class="col-md-6">
        
	        {{ Form::sText('namesurname',null,trans('site.contact_namesurname')) }}
            {{ Form::sText('phone',null,trans('site.contact_phone')) }}
            {{ Form::sText('email',null,trans('site.contact_email')) }}
    </div>
    <div class="col-sm-6">
            {{ Form::sTextarea('massage',null,trans('site.contact_massage')) }}
            {{ Form::hidden('form_type',"contact_form") }}
            @if ($errors->has('g-recaptcha-response'))
                <span class="help-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
            @endif
            {!! NoCaptcha::display() !!}
            {{ Form::dSubmit('save',trans('site.contact_sendmassage')) }}
        
    </div>
    
</div>
{!! Form::close() !!}

@section('js')
{!! NoCaptcha::renderJs() !!}
    {{-- <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
    <script>
        /*$("form").each(function() {
            $('.btn').prop('disabled', true);
        });*/
        function correctCaptcha() {
            $("form").each(function() {
                $('.btn').prop('disabled', false);
            });
        }
    </script> --}}
@endsection

