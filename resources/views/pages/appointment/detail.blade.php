<div class="coupon">
    <div class="col-md-12"><h2>Kupon</h2></div>
   @if(Session::has('couponerror'))
   <div class="col-md-12">
        <div class="alert alert-danger">
            <p style="margin-bottom: 0px;">{{ Session::get('couponerror') }}</p>
        </div>
    </div>
   @endif
    @if(Session::has('coupon'))
    <table class="table table-hover">
            <thead>
                <tr>
                    <td class="col-md-2"  style="max-width: 25px"><a href="#" id="removecoupon" data-id="{{ Session::get('coupon')->id }}">X</a></td>
                    <td class="col-md-8" style="min-width: 340px;max-width: 100%;">{{ Session::get('coupon')->code }}</td>
                </tr>
            </thead>
         
    @else
    
    <div class="col-md-12">
        <input type="text" class="couponinput">
    </div>
    <div class="col-md-12">
        <input name="couponbtn" id="couponbtn" class="btn btn-primary" type="button" value="Uygula">
    </div>
    @endif
    
</div>
<table class="table table-hover">
        <thead>
            <tr>
                <th class="col-md-8">Hizmet</th>
                <th class="col-md-2 text-center" style="min-width: 120px">Ücret</th>
            </tr>
        </thead>
        <tbody>
            @php
                $total = 0;
            @endphp
            @if ($extras)
                @foreach ($extras as $extra)
                    @if ($extra->id==99)
                        @if ($package)
                            <tr>
                                <td ><em>{{ $extra->name }}</em></h4></td>
                                <td class="text-center">{{ number_format(0,2) }} TL</td>
                            </tr>
                            @php
                                $total += 0;
                            @endphp
                        @else
                            <tr>
                                <td ><em>{{ $extra->name }}</em></h4></td>
                                <td class="text-center">{{ number_format($extra->price,2) }} TL</td>
                            </tr>
                            @php
                                $total += $extra->price;
                            @endphp
                        @endif
                       
                    @else
                        <tr>
                            <td ><em>{{ $extra->name }}</em></h4></td>
                            <td class="text-center">{{ number_format($extra->price,2) }} TL</td>
                        </tr>
                        @php
                            $total += $extra->price;
                        @endphp
                    @endif
                   
                   
                    
                @endforeach
            @endif
            @php
                $discount = 0;
            @endphp
            @if (Session::has('coupon'))
            @php
                $coupon = Session::get('coupon');
                
                if ($coupon->coupon_type_id==1) {
                    $discount = $coupon->value;
                } else if($coupon->coupon_type_id==2) {
                    $discount = $total * ($coupon->value/100);                    
                }
            @endphp
            <tr>
                <td ><em>{{ $coupon->code }}</em></h4></td>
                <td class="text-center">- {{ number_format($discount,2) }} TL</td>
            </tr>
            @endif
            <tr>
               
                <td class="text-right">
                <p>
                    <strong>Alt Toplam: </strong>
                </p>
                </td>
                <td class="text-center">
                <p>
                    <strong>{{ number_format($total-$discount,2) }} TL</strong>
                </p>
               </td>
            </tr>
            <tr>
                
                <td class="text-right"><h4><strong>Toplam: </strong></h4></td>
                <td class="text-center text-danger"><h4><strong>{{ number_format($total-$discount,2) }} TL</strong></h4></td>
            </tr>
        </tbody>
    </table>
    <div class="col-md-12">
        <style>
            .extradetail .btn{
                width: 100%;
            }
            
        </style>
        {{ Form::dSubmit2('send',trans('site.appointmnent_pymnt_btn')) }}
    </div>