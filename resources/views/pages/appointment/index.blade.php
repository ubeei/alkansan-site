@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu2'))
    <div class="container">
            {!! Form::open(['route' => "appointment.store", 'method' => "POST", "class" =>"appointmnent", "id" =>"appointmnent", "autocomplete"=>"off"]) !!}
        <div class="row">
            <div class="col-md-12">
                <h1 class="slidertitle">@lang('site.sliderbtn')</h1>
            </div>
           
            <div class="col-xl-8 col-lg-8 col-md-8">
                
                    <div class="row">
                        <div class="col-md-8">
                            {{ Form::dDateTime2('date', null, trans('site.appointmnent_date')) }}
                        </div>
                        <div class="col-md-4">
                            {{ Form::dSelect2('time', null, array() ,trans('site.appointmnent_time')) }}
                        </div>
                        <div class="col-md-12">
                            <div class="scrollbox boxdisable">
                            @if ($package)
                                <label class="extracontainer ec-disable fonextra" group-id="1">Fön {{ number_format(0,2) }} TL
                                    <input class="extrabox" type="checkbox" data-group-id="1" name="extra[]"  value="99">
                                    <span class="extracheckmark"></span>
                                </label>
                                
                            @else
                                <label class="extracontainer ec-disable fonextra" group-id="1">Fön {{ number_format(40,2) }} TL
                                    <input class="extrabox" type="checkbox" data-group-id="1" name="extra[]" value="99">
                                    <span class="extracheckmark"></span>
                                </label>

                            @endif
                            

                            @foreach ($extras as $extra)
                                <label class="extracontainer ec-disable" group-id="{{ $extra->group_id }}">{{ $extra->name }} {{ number_format($extra->price,2) }} TL
                                    <input class="extrabox" type="checkbox" name="extra[]" data-group="{{ $extra->group }}" data-group-id="{{ $extra->group_id }}" value="{{ $extra->id }}" @if (old('extra')!="") @if (in_array($extra->id,old('extra'))) checked @endif @endif>
                                    <span class="extracheckmark"></span>
                                </label>
                            @endforeach
                            </div>
                        </div>
                        <div class="col-md-12">
                            {{ Form::dTextarea2('note',null , trans('site.appointmnent_note')) }}
                        </div>
                        
                    </div>
                
            </div>
            <div class="col-md-4">
                <div class="row extradetail">
                        
                </div>

            </div>
            
        </div>
        {!! Form::close() !!}
    </div>

</div>
@endsection
@push('scripts')
    
    <script>
        
        
        var ranges = [ {start: new Date({{date('Y',strtotime($firstday))}},{{date('n',strtotime($firstday))}}-1,{{date('d',strtotime($firstday))}}), end: new Date({{date('Y',strtotime($endday))}},{{date('n',strtotime($endday))}}-1,{{date('d',strtotime($endday))}})},];
        var activeday = {!! $datelist !!};
        function datecheck(date) {
            for(var i=0; i < activeday.length; ++i) {
                console.log(activeday[i] +' - '+ date);
               if (activeday[i] == date) {
                    return true;
                }
            }
            return false;
        }

        
        $('.sliderbtn').on('click',function() {
            $('#slider').hide();
            $('.sliderbtn').hide();
            $('#appointment').show();
        });
        $.fn.datepicker.dates['tr'] = {
            days: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
            daysShort: ["Pz", "Pzt", "Sal", "Çrş", "Prş", "Cu", "Cts"],
            daysMin: ["Pz", "Pzt", "Sa", "Çr", "Pr", "Cu", "Ct"],
            months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            monthsShort: ["Oca", "Şub", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
            today: "Bugün",
            clear: "Temizle",
            weekStart: 1,
            format: "dd.mm.yyyy"
        };
        console.log(ranges);
        $('.date').datepicker({
            format: 'dd/mm/yyyy',
            startDate: ranges[0].start,
            endDate: ranges[ranges.length -1].end,
            datesDisabled: {!! $disableList !!},
            language: 'tr',
        }).on('changeDate', function (ev) {
            $.ajax({
                method: "POST",
                url: '{{ route("hour") }}',
                dataType:'json',
                data: { date: ev.target.value }, 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function( data ) {
                var time = $("#time");
                time.find('option').remove().end().append('<option value="">{{ trans("site.appointmnent_time") }}</option>').val('');
                for (k = 0; k < data.length; k++){
                    time.append(
                        $('<option></option>').val(data[k]).html(data[k])
                    );
                }
                $('.scrollbox').addClass('boxdisable');

            });
        });
        $('#time').on('change',function() {
            var t = this.value;
            var d = $('#date').val();
            $.ajax({
                method: "POST",
                url: '{{ route("extracheck") }}',
                dataType:'json',
                data: { date: d+' '+t }, 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function( data ) {
                $('.extrabox').attr('disabled','disabled');
                $('.extrabox').prop("checked", false);
                $('.extracontainer').addClass('ec-disable');
                
                $.each(data, function( index, value ) {
                    $('.extrabox[data-group-id="'+value+'"]').removeAttr('disabled');
                    $('.extracontainer[group-id="'+value+'"]').removeClass('ec-disable');

                });
                if (t) {
                    $('.scrollbox').removeClass('boxdisable');
                }else{
                    $('.scrollbox').addClass('boxdisable');
                }
            });
            
        })
       
        $(document).ready(function(){
            $('#phone').inputmask("0(999) 999 99 99");  //static mask
            extrabox();
        
            $('.extrabox').on('change',function(event){
                //console.log(event.currentTarget.attributes[4].nodeValue); 
                var gi = event.currentTarget.attributes[4].nodeValue;

                if (gi==1) {
                    if ($(this).is(':checked')) {
                        $('.extrabox[value="99"]').prop("checked", true); 
                        //$('.extrabox[value="99"]').attr('disabled','disabled');
                        $('.fonextra').addClass('ec-disable');
                        extrabox();

                    }else{
                        $('.extrabox[value="99"]').prop("checked", false); 
                        $('.extrabox[value="99"]').removeAttr('disabled');
                        $('.fonextra').removeClass('ec-disable');
                        
                        extrabox();

                    }
                }else{
                    extrabox();
                }
            });
        });
        function extrabox() {
           
            if ($('.extrabox').is(':checked')) {
                
                $.ajax({
                    method: 'POST',
                    url: '{{ route('appointment.detail') }}',
                    dataType:'html',
                    data: $('.appointmnent').serialize(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        
                        $('.extradetail').html(data);
                        couponfun();
                    },
                    error: function(errors) {
                        
                    },
                });
                
                return false;
            }else{
                $('.extradetail').html('');
            }
        }
    </script>
@endpush
	
@push('scripts')

    <script>
lottie.loadAnimation({
    container: document.getElementById('slider'), // the dom element that will contain the animation
    renderer: 'svg',
    loop: false,
    autoplay: true,
    path: "{{ asset('data.json') }}" // the path to the animation json
  });
  $( document ).ready(function() {
    var h = $(window).height();
    $('#appointment').css('min-height',h);
    $('.form-bg').css('min-height',h);
  });
</script>
@endpush
<style>
.scrollbox{
    position: relative;
}
.boxdisable{
    overflow: hidden;
}
.scrollbox.boxdisable::before{
    position: absolute;
    content: '';
    width: 100%;
    height: 100%;
    left: 0px;
    top: 0px;
    z-index: 10;
    background-color: rgba(204, 204, 204, 0.25);
    min-height: 780px;
}
</style>


@push('scripts')
    <script>
        function couponfun() {
            
        
            $('#couponbtn').on('click',function() {
                
                var coupon = $('.couponinput').val();
                if (coupon!=null) {
                    $.ajax({
                        method: 'POST',
                        url: '{{ route('ajax.coupon') }}',
                        dataType:'html',
                        data: 'coupon='+coupon,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            
                            $('.extradetail').html(data);
                            extrabox();
                        },
                        error: function(errors) {
                            
                        },
                    });
                }
            })
            $('#removecoupon').on('click',function() {
                $.ajax({
                    method: 'GET',
                    url: '{{ route('ajax.coupon.remove') }}',
                    dataType:'html',
                    //data: 'id='+id,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        $('.extradetail').html(data);
                        extrabox();
                    },
                    error: function(errors) {
                            
                    },
                });
            })
        }
    </script>
@endpush