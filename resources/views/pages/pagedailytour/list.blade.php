@if ($tours)
    <div class="speaker_area">
		<div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="tourtitle">We are found {{$tours->count}} results</h2>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-3">
                    <div class="tours-filter">
                        <div class="filter-title">Filter by:</div>
                        {{ Form::open(['url' => url()->full(), 'method' => "GET", "class" =>""]) }}
                        <div class="filter-field">
                                {{ Form::sText('tour_name_like',trans('site.tour_name_like'),NULL,$tourfilter['get']['tour_name_like']) }}
                                {{ Form::sSelect('continent_id',trans('site.continent'),$tourfilter['contiment_id'],'',$tourfilter['get']['continent_id']) }}
                                {{ Form::sSelect('country_id',trans('site.country'),$tourfilter['countries'],'',$tourfilter['get']['country_id']) }}
                                {{ Form::sSelect('style_id',trans('site.style_id'),$tourfilter['style'],'',$tourfilter['get']['style_id']) }}
                                {{-- {{ Form::sSelect('city_name',trans('site.search_tour_city'),[],'',$tourfilter['get']['city_name']) }} --}}
                                {{ Form::sSelect('duration',trans('site.duration'),$tourfilter['duration'],'',$tourfilter['get']['duration']) }}
                                {{ Form::sSelect('budget',trans('site.budget'),$tourfilter['budget'],'',$tourfilter['get']['budget']) }}
                                <input type="submit" class="btn btn-primary" value="Search" style="width: 100%;margin-bottom: 15px;" />
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-9"> 
                    <div class="row">
                        @foreach ($tours->tours as $key => $touritem)
                            @if ($touritem!=null)
                                
                                <div class="col-md-4 item text-center">
                                    <div class="touritem">
                                        <a href="{{ route('a.pagedetail',['name'=>str_slug($pageitem->title),'id' => $pageitem->id,'name2' =>str_slug($touritem->Name),'id2' => $touritem->ID ])}}">
                                        <div class="listitem">
                                            @if ($pageitem->Discount_Flat!=0)
                                                <div class="discount-flat"><div class="discount-flat-text">-{{$pageitem->Discount_Flat}}%</div></div> 
                                            @endif
                                            <img src="{{ image_check($tours->_config_data_->thumb_image_path.'/'.$touritem->Image_Long) }}" alt="{{ $touritem->Name }}" class="img-fluid" />
                                            <div class="tour-route">{!! str_limit($touritem->Tour_Route,70) !!}</div>
                                        </div>
                                        <h3 class="touritem-title">{!! str_limit($touritem->Name,41) !!}</h3>
                                        <div class="touritem-pricetext">{{ trans('site.touritem_pricetext') }}</div>
                                       
                                        <div class="touritem-price">{!! PriceCurrency($touritem->Invoice_Currency,$touritem->Price_Default) !!}</div>
                                        <div class="touritem-from"><strong>{{ trans('site.touritem_from_from') }} :</strong> {{ $touritem->Tour_Start_City }} <strong>{{ trans('site.touritem_from_to') }} :</strong> {{ $touritem->Tour_End_City }} <br/><strong>{{ trans('site.touritem_from_duration') }} :</strong> {{ $touritem->Duration }} {{ trans('site.touritem_from_days') }}</div>
                                    </div>
                                </div>
                                
                            @endif
                        @endforeach
                    </div>

                    <style>
                        .page-item.active .page-link {
                            z-index: 1;
                            color: #fff;
                            background-color: #409cd1;
                            border-color: #409cd1;
                        }
                        .page-item.active .page-link:hover {
                            z-index: 1;
                            color: #fff;
                            background-color: #02acba;
                            border-color: #02acba;
                        }
                    </style>
                    <?php $sayfa= $offset_esit =0; ?>
                    @for ($i = 1; $i <= $number_of_pages; $i++)
                        <?php 
                        if (($i-1)*$tourfilter['get']['limit'] == $tourfilter['get']['offset']) {
                            $offset_esit =  $tourfilter['get']['offset'];
                            $sayfa = $i;
                        }
                        ?>
                    @endfor
                    <nav aria-label="...">
                        <ul class="pagination justify-content-end">
                            @if ($sayfa!=1)
                                <li class="page-item">
                                    <a class="page-link" href="?offset={{ $offset_esit-$tourfilter['get']['limit'] }}&limit={{ $tourfilter['get']['limit'] }}" tabindex="-1">
                                        Previous
                                    </a>
                                </li>
                            @endif
                            <?php $degisken = -4 ?>
                            @for ($i = $sayfa-4; $i < $sayfa+4; $i++)
                                @if ($i == $sayfa)
                                    <li class="page-item active">
                                        <a class="page-link" href="?offset={{ $offset_esit }}&limit={{ $tourfilter['get']['limit'] }}">
                                            {{ $i }}
                                        </a>
                                    </li>
                                @else
                                    @if ($i>0 && $i<$number_of_pages)
                                        <li class="page-item">
                                            @if ($i<$sayfa)
                                                <a class="page-link" href="?offset={{ $offset_esit+($degisken*$tourfilter['get']['limit']) }}&limit={{ $tourfilter['get']['limit'] }}">
                                            @else
                                                <a class="page-link" href="?offset={{ $offset_esit+($degisken*$tourfilter['get']['limit']) }}&limit={{ $tourfilter['get']['limit'] }}">
                                            @endif
                                                {{ $i }}
                                            </a>
                                        </li>
                                    @endif
                                @endif
                                <?php $degisken++ ?>
                            @endfor
                            @if ($sayfa!=43)
                                <li class="page-item">
                                    <a class="page-link" href="?offset={{ $offset_esit+$tourfilter['get']['limit'] }}&limit={{ $tourfilter['get']['limit'] }}">
                                        Next
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endif
<!--End venues Area-->
@section('js')
    <script>
    $( document ).ready(function() {
        $('#continent_id').on('change',function() {
            var continent_id = $('#continent_id').val();
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.tourcountry') }}",
                data:'continent_id='+continent_id,
                success: function(data){
                    $('#country_id').empty();
                    $('#country_id').append($('<option>').text('').attr('value', ""));
                    $.each(data, function(i, value) {
                       $('#country_id').append($('<option>').text(value).attr('value', i));
                });
                }
            });
        });
    });
    </script>
@endsection