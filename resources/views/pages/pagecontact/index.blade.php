@extends('layouts.homeindex')
@section('content')

  @include(template_path_check('/layouts/topmenu'))
  <div style="background-color:white;" class=" container-fluid">

<div class="row justify-content-center">
  <div style="padding-top:2vw;padding-bottom:2vw;text-align:right;color:#737373!important;" class="col-10">
  <a style="margin-right:2vw;color:#737373!important;"  href="{{route('index')}}" class="router">@lang('site.home')</a> <p style="margin-right:4px;margin-left:4px;" class="router">/</p> <a style="color:#737373!important;" href="{{route('contact')}}" class="router">@lang('site.contact_head')</a>
  </div>
<div class=" col-11 col-sm-10 col-md-9">

  <section style="color:#737373" class="mb-4">

    <!--Section heading-->


    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">@lang('site.contact_sub_head')</p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
            <form id="contact-form" name="contact-form" action="{{ route('brand.sendMessage') }}" method="POST">
@csrf
                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="">@lang('site.contact_form_name')</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="">@lang('site.contact_form_email')</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class="">@lang('site.contact_form_subject')</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="body" rows="2" class="form-control md-textarea"></textarea>
                            <label for="message">@lang('site.contact_form_message')</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->

            </form>

            <div class="text-center text-md-left">
                <a class="btn btn-primary" onclick="document.getElementById('contact-form').submit();">@lang('site.contact_form_submit')</a>
            </div>
            <div class="status"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p>@lang('site.contact_address')</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p><span>Tel.: </span>+90 212 222 75 38</p>
                    <p><span>Tel.: </span>+90 212 223 22 72</p>

                    <p><span>Fax.: </span>+90 212 210 06 33</p>
                    <p><span>Fax.: </span>+90 212 223 22 71</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p>nk@alkansan.com</p>
                    <p>nureddin.kolemen@alkansan.com</p>
                    <p>engin.tan@alkansan.com</p>
                    <p>gokay@alkansan.com</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>
<br><br>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1504.2113859396104!2d28.968204279453616!3d41.05975051747188!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab73b5b2401b1%3A0xfcc51bf4625d3928!2sAlkansan%20Teknik%20Ayd%C4%B1nlatma!5e0!3m2!1sen!2str!4v1572283622644!5m2!1sen!2str" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>


</section>

</div>

</div>

</div>
@endsection
@push('style')
<style>
p{margin-bottom: 0.1rem;}
.router{text-align:right;color:#737373!important;font-style:italic;font-size:small;float:right;}

</style>
@endpush
