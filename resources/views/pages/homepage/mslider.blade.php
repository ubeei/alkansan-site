
      <section class="slider">
          <div id="mlayerslider" class="ls-wp-container fitvidsignore" style="width:50vw;height:843px;margin:0 auto;margin-bottom: 0px;">
  @foreach ($sliders as $slider)

              <div class="ls-slide" data-ls="timeshift:-1000;kenburnsscale:1.2;">

                      <p class="ls-l lslayer1 centric" style="top:164px;
                      left: 25%;
                      text-align:left;
                      font-weight:400;
                      font-style:normal;
                      text-decoration:none;
                      width:430px;
                      white-space:normal;
                      color:#0070b7;
                      font-size:300%;
                      font-weight: 900;" data-ls="showinfo:1;delayin:500;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">{{$slider->title}}</p>

  <?php $text = $slider->title_desc;

  $middle = strrpos(substr($text, 0, floor(strlen($text) / 2)), ' ') + 1;

  $desc1 = substr($text, 0, $middle);
  $desc2 = substr($text, $middle); ?>

                      <p class="ls-l lslayer2 centric" style="top:240px;
                      left: 25%;
                      text-align:left;
                      font-style:normal;
                      text-decoration:none;
                      width:430px;
                      white-space:normal;
                      color:#0070b7;
                      font-size:190%;
                      font-weight: normal;" data-ls="showinfo:1;delayin:750;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">{{$desc1}}</p>

                      <p class="ls-l lslayer3 centric" style="top:295px;
                      left: 25%;
                      text-align:left;
                      font-style:normal;
                      text-decoration:none;
                      width:430px;
                      white-space:normal;
                      color:#0070b7;
                      font-size:190%;
                      font-weight: normal;" data-ls="showinfo:1;delayin:1000;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">{{$desc2}}</p>

                      <p style="top:405px;
                      left: 25%;
                      text-align:left;
                      font-style:normal;
                      text-decoration:none;
                      width:430px;
                      white-space:normal;
                      color:#737373;
                      font-size:90%;
                      font-weight: 300;" class="ls-l centric" data-ls="showinfo:1;delayin:1250;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">{{$slider->text}}</p>

  <?php $button = json_decode($slider->button,true)?>
                      <a style="top:505px;left: 18.5%;background: #0070b7;line-height: 3rem;text-align:center;#fff;width: 192px;" class="ls-l centric" href="{{$button[0]['button_url']}}" target="_self" data-ls="showinfo:1;delayin:1500;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">
                          {{$button[0]['button_name']}}
                      </a>
                      <img width="513" height="931" src="{{$slider->image}}" class="ls-l ls-img" alt="" sizes="(max-width: 513px) 100vw, 513px" srcset="{{$slider->image}}"  style="bottom:0px;left:45%;text-align:initial;font-weight:400;font-style:normal;text-decoration:none;" data-ls="showinfo:1;offsetyin:80%;delayin:100;easingin:easeOutCubic;rotatein:0;scalexin:1.15;scaleyin:.9;transformoriginin:0% 100% 0;offsetxout:left;easingout:easeInBack;skewxout:25;">

                  </div>
                  @endforeach
  </div>
      </section>

      @push('scripts')
          <script>


      //     $( window ).resize(function(){
      //
      //       $blwidth = parseInt($("img.ls-img.ls-layer").eq(1).parent().css('width'));
      //       $bwid = $blwidth/2;
      //                   $empty = parseInt($( window ).width());
      //                   $empty = $empty/2;
      //                  $emptyy = $empty - $bwid;
      //     $wid = parseInt($(".ls-img").eq(1).parent().css('left'));
      //     $ofset = $("#layerslider").offset().left;
      // $wid = $wid + $ofset;
      //     $fin = $emptyy - $wid;
      //
      //       $("img.ls-img.ls-layer").each(function(){
      //         event.stopPropagation();
      //       $( this ).css("margin-left",($fin)+"px");
      //
      //       });
      //
      //
      // $ww = parseInt($(".slider").width());
      // console.log("slider width:" + $ww);
      //
      //
      // // $(".ls-in-out:not(:has(img))").each(function(index){
      // //   event.stopPropagation();
      // //
      // //   $ew = parseInt($( ".ls-in-out:not(:has(img))" ).width());
      // //   console.log("li width:" + $ew);
      // //   $el = parseInt($( ".ls-in-out:not(:has(img))" ).css('left'));
      // //   console.log("li left:" + $el);
      // //   $fw = $ww - $ew;
      // //   console.log($fw);
      // //   $fw = $fw/2;
      // //   console.log($fw);
      // //   $eml = $fw - $el;
      // //   console.log("li margin-left should be:" + $eml);
      // //
      // //   $( ".ls-wrapper.ls-in-out:not(:has(img))" ).css("margin-left", ($eml)+"px!important");
      // //   console.log($( ".ls-in-out:not(:has(img))" ).css('margin-left'));
      // //   // $('.centric').eq(index).parent().css("margin-left", ($eml)+"px!important");
      // // });
      //
      //
      //
      //
      //     });
              $( document ).ready(function() {
                //
                // setTimeout(function(){
                //   if(($( window ).width()) < 1200){
                //   $(".ls-bottom-slidebuttons").css("margin-top","25px");
                // }
                //
                //   $blwidth = parseInt($("img.ls-img.ls-layer").eq(1).parent().css('width'));
                //   $bwid = $blwidth/2;
                //               $empty = parseInt($( window ).width());
                //               $empty = $empty/2;
                //              $emptyy = $empty - $bwid;
                // $wid = parseInt($(".ls-img").eq(1).parent().css('left'));
                // $ofset = $("#layerslider").offset().left;
                // $wid = $wid + $ofset;
                // $fin = $emptyy - $wid;
                //
                //   $("img.ls-img.ls-layer").each(function(){
                //     // event.stopPropagation();
                //   $( this ).css("margin-left",($fin)+"px");
                //
                //   });
                //
                //
                //
                //
                // },1000);











      // $blwidth = ("div > .ls-img").eq(0).css('width');
      // $bwid = $blwidth/2;
      //             $empty = $( window ).width();
      // //             console.log($empty);
      //           $empty = $empty - $bwid;
      // // console.log($empty);
      // console.log($empty/2);
      // $("img.ls-img.ls-layer").each(function(){
      // $( this ).css('width', '200px');
      // });
      //
      //             });
      // $empty = ($empty/2);
      // console.log($empty);
      // console.log($empty/2);
      // // $("div > .ls-img").parent().find('.ls-img').delay(10000).css({left: ($empty) + "px"});
      // $("img.ls-img.ls-layer").eq(1).parent().hide();


      var w = $('#mainHeader').width();
      var h = 843;
      console.log(h);
      $("#mlayerslider").layerSlider({
          sliderVersion: '6.5.7',
          type: 'fullsize',
          autoStart: false,
          skin: 'v6',
          globalBGAttachment: 'fixed',
          globalBGSize: 'cover',
          navPrevNext: false,
          hoverPrevNext: false,
          navStartStop: false,
          showCircleTimer: false,
          thumbnailNavigation: 'disabled',
          responsive: true,
          height: h,
          width:w

          // width: w+200,

      });


              });
              $( window ).on( "load", function() {
                  //slidernumber();
                  //setTimeout(slidernumber(), 3000);
              });
              $('#layerslider').on('sliderDidLoad', function(event, slider) {
                  slidernumber();



                  // $(".ls-in-out").each(function(){
                  //   $( this ).delay(1000).css('left', '10%');
                  // });







      });

              function slidernumber() {
                  console.log('asdasd');
                  var k = 1;
                  $( ".ls-bottom-slidebuttons a" ).each(function() {
                      $( this ).text( '0'+k );
                      k=k+1;

                  });
                  //setTimeout(slidernumber(), 10000);
              }
          </script>
          <style>
          #mlayerslider{
              width: 100% !important;
              margin: 0 !important;
          }
          </style>
      @endpush
