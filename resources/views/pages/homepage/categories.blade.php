<div class="col-lg-7 col-md-12 col-xl-8">


<div class="row justify-content-start">
<ul id="cats" class="cats_list col-12 col-sm-12 col-md-8 col-lg-10">
  @foreach($categories as $cat)
  <li class="home_category"><a href="{{ route('category',['slug'=>$cat->slug]) }}" class="home_cat_a"><p class="home_cat_name">{{strtoupper($cat->title)}}</p></a></li>

@endforeach





</ul>


</div>
</div>

@push('style')
<style>
.cats_list{background-color: #00609d;padding-top: 2rem;padding-bottom: 2rem;padding-left: inherit;padding-right: inherit;min-width: max-content;}
.home_category{display:table;list-style-type: none;min-width: max-content;transition:all 0.3s ease;padding-left: 2rem;padding-right: 2rem;width: 100%;}
.home_category:hover{outline:5px solid #0070b7;background-color: #ff6600;}
.home_cat_a{padding-top:0.5rem;padding-bottom:0.5rem;display: table-cell;vertical-align: middle;}
.home_category .home_cat_name{font-weight: 100;transition:all 0.3s ease}
.home_category:hover .home_cat_name{font-weight: 900;}
.home_cat_name{margin-bottom:0;margin-top: 0;}
.home_cats{margin-top: -86vw;}
@media screen
and (max-width:991px){
.cats-list{margin-top:30px;}
#cats{margin-top:-23vw;}
}

@media screen
and (max-width:765px){
.home_category{text-align: center;}
}
@media screen
and (max-width:591px){
#cats{margin-top:-37vw;}
}
@media screen
and (max-width:391px){
#cats{margin-top:-60vw;}
}


</style>

@endpush
@push('scripts')
<script>
$(window).ready(function(){
  console.log('ready');
  var height = $("#layerslider").height();
  var catsheight  = $("#cats").height();
  var el = $("#cats");
   var bottom = el.position().top + el.offset().top + el.outerHeight(true);
  var margin = (height-catsheight)/2;
if($( window ).width() > 992){
  $("#cats").css('margin-top', margin+"px");
}else{

  $("#cats").parent().removeClass("justify-content-start");
    $("#cats").parent().addClass("justify-content-center");

  }


});

$( window ).resize(function(){

  if(parseInt($( this ).width()) < 765) {

  $("#cats").parent().removeClass("justify-content-start");
    $("#cats").parent().addClass("justify-content-center");
}
});


</script>

@endpush
