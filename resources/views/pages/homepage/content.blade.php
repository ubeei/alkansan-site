@extends('layouts.homecontent')

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu'))
    <div id="main"   class="row justify-content-start">

    <!-- <div class="col-lg-8 col-md-7 d-none d-lg-block"> -->
    @include(template_path_check('/pages/homepage/slider'),['sliders' => $sliders])
  <!-- </div> -->
    <div class="homecats col-12 col-lg-4">
    @include(template_path_check('/pages/homepage/categories'),['categories' => $categories])
</div>
</div>

    @if ($page)
       @include(template_path_check('/pages/contentview/viewrouter'),['page',$page])
    @endif

</div>
@endsection
@push('style')
	<link href="{{ asset('js/flexslider/flexslider.css') }}" rel="stylesheet">
    <style>
    .flex-control-paging li a{
        border: 1px solid #bc5d3d;
        background-color: #fff;
        width: 25px;
        height: 25px;
        }
        .flex-control-paging li a.flex-active{
        background: #bc5d3d;
        }
        .flex-direction-nav a{
            height: 52px;
        }
        .flex-direction-nav{
            display: none;
        }

        @media screen
        and (max-width:770px){
        /* .homecats{display:none;} */
        }
        /* @media screen
        and (max-width:992px){
        #main{opacity: 0;}
        } */
/* #body-wrap{opacity: 0;} */
    </style>
@endpush
@push('scripts')
    <script src="{{ asset('js/flexslider/jquery.flexslider.js') }}"></script>
    <script>
    $(document).ready(function(){
      // $("#main").animate({
      //   opacity: 100
      // },2000,function(){});



        $('.flexslider').flexslider({
            animation: "slide"
        });


    });
    $( window ).on( "load", function() {
        $( ".slider-right" ).each(function() {
            var  h = $( this ).height();
            var ch = $( this).find('.slider-content').height();
            console.log(h+' '+ ch);
            $( this).find('.slider-content').css('padding-top',(h-ch)/2);

        });
    });

    </script>
@endpush
