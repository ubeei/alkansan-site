<div class="sliderpage col-lg-8 col-md-12 ">
  <div class="mobile-images" style="overflow-x:hidden;">
@foreach ($sliders as $slider)

  <img src="{{$slider->image}}" class="mimage d-lg-none" alt="" sizes="(max-width: 513px) 100vw, 513px" srcset="{{$slider->image}}" >
@endforeach
</div>

    <section class="slider">
        <div id="layerslider" class="ls-wp-container fitvidsignore" style="width:50vw;height:843px;margin:0 auto;margin-bottom: 0px;">
@foreach ($sliders as $slider)

            <div class="ls-slide" data-ls="timeshift:-1000;kenburnsscale:1.2;">

                    <p class="ls-l lslayer1 centric" style="top:164px;
                    left: 25%;
                    text-align:left;
                    font-weight:400;
                    font-style:normal;
                    text-decoration:none;
                    width:430px;
                    white-space:normal;
                    color:#0070b7;
                    font-size:300%;
                    font-weight: 900;" data-ls="showinfo:1;delayin:500;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">{{$slider->title}}</p>

<?php $text = $slider->title_desc;

$middle = strrpos(substr($text, 0, floor(strlen($text) / 2)), ' ') + 1;

$desc1 = substr($text, 0, $middle);
$desc2 = substr($text, $middle); ?>

                    <p class="ls-l lslayer2 centric" style="top:240px;
                    left: 25%;
                    text-align:left;
                    font-style:normal;
                    text-decoration:none;
                    width:430px;
                    white-space:normal;
                    color:#0070b7;
                    font-size:190%;
                    font-weight: normal;" data-ls="showinfo:1;delayin:750;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">{{$desc1}}</p>

                    <p class="ls-l lslayer3 centric" style="top:295px;
                    left: 25%;
                    text-align:left;
                    font-style:normal;
                    text-decoration:none;
                    width:430px;
                    white-space:normal;
                    color:#0070b7;
                    font-size:190%;
                    font-weight: normal;" data-ls="showinfo:1;delayin:1000;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">{{$desc2}}</p>

                    <p style="top:405px;
                    left: 25%;
                    text-align:left;
                    font-style:normal;
                    text-decoration:none;
                    width:430px;
                    white-space:normal;
                    color:#737373;
                    font-size:90%;
                    font-weight: 300;" class="ls-l centric texty" data-ls="showinfo:1;delayin:1250;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">{{$slider->text}}</p>

<?php $button = json_decode($slider->button,true)?>
                    <a style="top:505px;left: 18.5%;background: #0070b7;line-height: 3rem;text-align:center;#fff;width: 192px;" class="ls-l centric" href="{{$button[0]['button_url']}}" target="_self" data-ls="showinfo:1;delayin:1500;clipin:0 0 0 100%;scalexout:.5;scaleyout:0;transformoriginout:0% 100% 0;texttransitionin:true;texttypein:lines_asc;textshiftin:130;textoffsetyin:-30;textstartatin:transitioninend - 700;">
                        {{$button[0]['button_name']}}
                    </a>
                    <img width="513" height="931" src="{{$slider->image}}" class="ls-l ls-img d-none d-lg-block" alt="" sizes="(max-width: 513px) 100vw, 513px" srcset="{{$slider->image}}"  style="bottom:0px;left:45%;text-align:initial;font-weight:400;font-style:normal;text-decoration:none;" data-ls="showinfo:1;offsetyin:80%;delayin:100;easingin:easeOutCubic;rotatein:0;scalexin:1.15;scaleyin:.9;transformoriginin:0% 100% 0;offsetxout:left;easingout:easeInBack;skewxout:25;">

                </div>
                @endforeach

    </section>
</div>

@push('style')
<style>
.centrix{margin-left:20px;}

</style>

@endpush

@push('scripts')
    <script>

    // if( $( window ).width() < 991){
    //   var vals = {height : "100vw!important",right : "-41vw!important"right : "24vw!important"}
    // }else if($( window ).width() < 791){
    //   var vals = {height : "100vw!important",right : "-43vw!important"right : "24vw!important"}
    // }else if($( window ).width() < 591){
    //   var vals = {height : "100vw!important",right : "-47vw!important"right : "24vw!important"}
    // }else if($( window ).width() < 391){
    //   var vals = {height : "100vw!important",right : "-54vw!important"right : "24vw!important"}
    // }

    $( window ).resize(function(){



      if(($( window ).width()) < 730){
        $("img.ls-img.ls-layer").parent().css('display', 'none');
      }

      $blwidth = parseInt($("img.ls-img.ls-layer").eq(1).parent().css('width'));
      $bwid = $blwidth/2;
                  $empty = parseInt($( window ).width());
                  if($empty>992)
                  $empty = $empty/2;
                  else {
                    $empty = $empty/1.3;
                    $(".ls-in-out:not(:has(img))").each(function(index){

                    $( this ).css("margin-left", "10vw");

                  });

                }
                 $emptyy = $empty - $bwid;
    $wid = parseInt($(".ls-img").eq(1).parent().css('left'));
    $ofset = $("#layerslider").offset().left;
$wid = $wid + $ofset;
    $fin = $emptyy - $wid;

      $("img.ls-img.ls-layer").each(function(){
        event.stopPropagation();
      $( this ).css("margin-left",($fin)+"px");

      });


$ww = parseInt($(".slider").width());
console.log("slider width:" + $ww);





    });
        $( document ).ready(function() {



          $('#layerslider').on('slideChangeWillStart', function(event, slider) {
// $(".slider").animate({
// opacity: 0
// },0,function(){
// });
$(".slider").css("opacity",0);
$(".slider").animate({
opacity: 100
},1000,function(){
});

});





          $('#layerslider').on('slideChangeDidStart', function(event, slider) {
            setTimeout(function(){

              if( $( window ).width() < 991){

                var vals = "41vw";
              }else if($( window ).width() < 791){
          var vals = "41vw";
              }else if($( window ).width() < 591){
          var vals = "33vw";
        }else if($( window ).width() < 491){
          var vals = "33vw";
              }else if($( window ).width() < 391){
              var vals = "33vw";
                      }


            $(".ls-in-out:not(:has(img))").css("margin-left", vals);
          },1000);
          });


            var w = $('#mainHeader').width();
            var h = 843;
            console.log(h);
            $(".mimage").eq(0).animate({
              right: "24vw"
            },1000,function(){



            $("#layerslider").layerSlider({
                sliderVersion: '6.5.7',
                type: 'fixedsize',
                autoStart: false,
                skin: 'v6',
                globalBGAttachment: 'fixed',
                globalBGSize: 'cover',
                navPrevNext: false,
                hoverPrevNext: false,
                navStartStop: false,
                showCircleTimer: false,
                thumbnailNavigation: 'disabled',
                responsive: true,
                height: h,
                width:w,

                // width: w+200,

            });
              });

              $('#layerslider').on('slideChangeDidStart', function(event, slider) {

//                 $(".mimage").eq(slider.slides.prev.index).animate({
//                   right: "-75vw"
//                 },500,function(){
// });
$('.mimage').each(function(){
  if($( this ).css("right") != "75vw"){
  $( this ).animate({
    right: "-75vw"
  },500);
}
});

                $(".mimage").eq(slider.slides.current.index).animate({
                  right: "24vw"
                },1000,function(){
});

              });


$(".ls-in-out:not(:has(img))").each(function(index){

$( this ).css("margin-left", "10vw");
});
setTimeout(function(){

if( $( window ).width() < 991){

  var vals = "41vw";
}else if($( window ).width() < 791){
var vals = "38vw";
}else if($( window ).width() < 591){
var vals = "30vw";
}else if($( window ).width() < 491){
var vals = "33vw";
}else if($( window ).width() < 391){
var vals = "33vw";
}


$(".ls-in-out:not(:has(img))").each(function(){
  $( this ).animate({"margin-left": vals},500,function(){});
});


},1000);





        });
        $( window ).on( "load", function() {
            //slidernumber();
            //setTimeout(slidernumber(), 3000);
        });
        $('#layerslider').on('sliderDidLoad', function(event, slider) {
            slidernumber();
            if($( window ).width() < 500){
              setTimeout(function(){
              $(".texty").css("font-size", "12px");
            },1000,function(){});
            }
            if($( window ).width() < 1200){
              setTimeout(function(){


              $(".ls-bottom-slidebuttons").animate({"margin-top": "40px"},500,function(){

              });

            },1000);
            }

            setTimeout(function(){

              if(($( window ).width()) < 730){
                $("img.ls-img.ls-layer").parent().css('display', 'none');
              }


              $blwidth = parseInt($("img.ls-img.ls-layer").eq(1).parent().css('width'));
              $bwid = $blwidth/2;
                          $empty = parseInt($( window ).width());
                          if($empty>992)
                          $empty = $empty/2;
                          else {
                            $empty = $empty/1.3;

                        }

                         $emptyy = $empty - $bwid;
            $wid = parseInt($(".ls-img").eq(1).parent().css('left'));
            $ofset = $("#layerslider").offset().left;
            $wid = $wid + $ofset;
            $fin = $emptyy - $wid;

              $("img.ls-img.ls-layer").each(function(){
                // event.stopPropagation();
              $( this ).css("margin-left",($fin)+"px");


              });






            },800);

            setTimeout(function(){

              if( $( window ).width() < 991){

                var vals = "41vw";
              }else if($( window ).width() < 791){
      var vals = "41vw";
              }else if($( window ).width() < 591){
      var vals = "33vw";
      }else if($( window ).width() < 491){
      var vals = "33vw";
      }else if($( window ).width() < 391){
      var vals = "33vw";
              }


              $(".ls-in-out:not(:has(img))").each(function(){
                $( this ).animate({"margin-left": vals},500,function(){});
              });
      },1000);



            // $(".ls-in-out").each(function(){
            //   $( this ).delay(1000).css('left', '10%');
            // });







});

        function slidernumber() {
            console.log('asdasd');
            var k = 1;
            $( ".ls-bottom-slidebuttons a" ).each(function() {
                $( this ).text( '0'+k );
                k=k+1;

            });
            //setTimeout(slidernumber(), 10000);
        }
    </script>
    <style>



.mimage{
  text-align: initial;
    font-weight: 400;
    font-style: normal;
    text-decoration: none;
    position: absolute;
    height:100vw;
    top: 24vw;
    transform: rotate(270deg);
    right: -75vw;
}
    @media screen
    and (max-width:991px){
      .sliderpage{width:100%;overflow:hidden;}
      .slider{
    margin-top : 8vw!important;}
    .mimage{
  top: -38vw!important;}
    }
    @media screen
    and (max-width:791px){
      .slider{
    margin-top : 0vw!important;}
    .mimage{
    top: -39vw!important;
}
    }
    @media screen
    and (max-width:591px){
      .slider{
    margin-top : -6vw!important;}
    .mimage{
    top: -39vw!important;;
}
    }
    @media screen
    and (max-width:391px){
      .slider{
    margin-top : -20vw!important;}
    .mimage{
    top: -38vw!important;;
}
    }

    @media screen
    and (min-width:991px){
      #main{width:100%;}

    }

    #layerslider{
        width: 100% !important;
        margin: 0 !important;
    }

    @media screen
    and (max-width:992px){
    #homebg{background:white;}
    }
    </style>
@endpush

@push('scripts')


@endpush
