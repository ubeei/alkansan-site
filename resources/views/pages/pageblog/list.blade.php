@extends('layouts.homeindex')
@section('content')

  @include(template_path_check('/layouts/nmtopmenu'))
  <div class="container-fluid">
<div class="bg row justify-content-center">
<div style="padding-top:2vw;padding-bottom:2vw;text-align:right;" class="col-10">
<a style="margin-right:2vw;"  href="{{route('index')}}" class="router">@lang('site.home')</a> <p style="margin-right:4px;margin-left:4px;" class="router">/</p> <a href="{{route('site.blog.index')}}" class="router">Blog </a>
</div>

</div>

<div class="bg row justify-content-center">
<div class="col-10">
<div class="row justify-content-center">
  @if($blogs)

@foreach($blogs as $blogPost)
<a href="{{ route('blog.content', ['id' => $blogPost->id])}}" style="text-align:center;" class=" blogPost col-12 col-md-6 col-lg-4 col-xl-3">
  <img class="blogImg" src="{{$blogPost->path}}" />
  <div class="blogName" style="width:100%;text-align:center;">
  <p class="blogN" >{{$blogPost->title}}</p>
</div>
<p class="cNote" >@lang('site.blog_continue')</p>

</a>

@endforeach
@else
<div class="row justify-content-center">
  <h2 class="col-10">
  Blog not found
  </h2>
  <a href="{{route('index')}}" class="col-10">Home</a>
</div>

@endif


</div>
</div>
</div>
</div>


@push('style')
<style>
 .bg{background-color:white;}
 .router{text-align:right;color:#737373!important;font-style:italic;font-size:small;float:right;}
 .blogImg{width:100%;max-height:280px;max-width: 280px;}
 .blogName{min-height:60px;display: table;}
 .blogPost:hover .blogName{background-color:#0070b7;}
 .blogPost:hover .blogName .blogN{color:white;}
 .cNote{min-height:40px;transition:all 0.3s ease;padding-top:10px;opacity:0;color:#0070b7;text-align:center;}
 .blogN{text-align:center;color:#737373;display:table-cell;vertical-align:middle;}
</style>

@endpush
@push('scripts')
<script>
$( document ).ready(function() {
  $(".blogPost").mouseover(function(){
    $( this ).find(".cNote").css('opacity', 100);
  });
  $(".blogPost").mouseout(function(){
    $( this ).find(".cNote").css('opacity', 0);
  });
});
</script>
@endpush
@endsection
