@extends('layouts.homeindex')
@section('content')

  @include(template_path_check('/layouts/topmenu'))
<div style="background-color:white;" class="container-fluid">
<div class="row justify-content-center">
<div class="col-10 col-md-9 row justify-cotntent-center">

  <div style="color:#737373;padding-top:2vw;padding-bottom:2vw;text-align:right;" class="col-10">
  <a style="color:#737373;"  href="{{route('site.blog.index')}}" class="router"> Blog</a> <p style="margin-right:4px;margin-left:4px;" class="router">/</p> <a style="color:#737373;" href="{{route('blog.content', $blog->id)}}" class="router">{{$blog->title}}</a>
  </div>
<div class="col-12 row justify-content-center">
  <div class="col-12 col-md-6">

<!--
    <img src="{{$blog->path}}" style="width:100%;height:auto;"/> -->
    <?php

    $images = json_decode($blogImages,true);
// dd($images);

    ?>
    <div class="flexslider">
    <ul class="slides">
    @foreach($images as $image)



      <li data-thumb="{{$image['path_mthumbnail']}}">
        <img src="{{$image['path']}}" />
      </li>


    @endforeach
    </ul>
  </div>


    </div>
  <div style="color:#737373;" class="col-12 col-md-6">

    <h4>{{$blog->title}}</h4>


    <p style="font-weight:500;font-size:small;">{{$blog->created_at->toDateString()}}</p>
    <br>
    <p style="font-size:small;">{{$blog->content}}</p>

    </div>




  </div>
  </div>
  </div>
  <div style="margin-top:1rem;" class="bg row justify-content-center">
  <div class="col-10">
    <p style="    font-size: medium;
    text-align: center;
    color: #737373;
    font-weight: 500;
    margin-bottom: 3rem;">@lang('site.blog_other')</p>

  <div class="row justify-content-center">

  @foreach($blogs as $blogPost)
  <a href="{{ route('blog.content', ['id' => $blogPost->id])}}" style="text-align:center;" class=" blogPost col-12 col-md-6 col-lg-4 col-xl-3">
    <img class="blogImg" src="{{$blogPost->path}}" />
    <div class="blogName" style="width:100%;text-align:center;">
    <p class="blogN" >{{$blogPost->title}}</p>
  </div>
  <p class="cNote" >@lang('site.blog_continue')</p>

  </a>

  @endforeach



  </div>
  </div>
  </div>
</div>
@endsection
@push('style')
<style>
<style>
 .bg{background-color:white;}
 .router{text-align:right;color:#737373!important;font-style:italic;font-size:small;float:right;}
 .blogImg{width:100%;max-height:280px;max-width: 280px;}
 .blogName{min-height:60px;display: table;}
 .blogPost:hover .blogName{background-color:#0070b7;}
 .blogPost:hover .blogName .blogN{color:white;}
 .cNote{min-height:40px;transition:all 0.3s ease;padding-top:10px;opacity:0;color:#0070b7;text-align:center;}
 .blogN{text-align:center;color:#737373;display:table-cell;vertical-align:middle;}
</style>
</style>
@endpush
  @push('scripts')
<script>
$( document ).ready(function() {
  $('.flexslider').flexslider({
      animation: "slide",
      controlNav: "thumbnails"
    });


      $(".blogPost").mouseover(function(){
        $( this ).find(".cNote").css('opacity', 100);
      });
      $(".blogPost").mouseout(function(){
        $( this ).find(".cNote").css('opacity', 0);
      });

});
</script>

  @endpush
