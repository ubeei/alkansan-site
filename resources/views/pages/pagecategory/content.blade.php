<div class="row justify-content-center">



<div class="row justify-content-center">
    @foreach($products as $product)
		@if (!isset($category))
		<?php
		$category = $product->categories;
		foreach($category as $cat){
		  if($cat->slug){
				$category = $cat;
			}
		}
		?>
		@endif
      <a href="{{route('product',['slug'=>$category->slug,'id2' => $product->id,'slug2'=>$product->name])}}" class="blogOutside col-12 col-sm-12 col-md-6 col-lg-4">
        <div class="blogPost">
        @foreach ($product->product_images as $image)
          <img class="blogImg" src="{{image_resize($image->path,345,345)}}" />
          @php
              break;
          @endphp
        @endforeach
        <div class="blogName" style="width:100%;text-align:center;">
        <p class="blogN" >{{$product->name}}</p>
      </div>
    </div>
    
      </a>
    @endforeach
</div>


    @push('style')
    <style>
	.router{text-align:right;color:#737373!important;font-style:italic;font-size:small;float:right;}
	.blogImg{width:100%;max-height:345px;max-width: 345px;}
	.blogName{min-height:60px;display: table;}
	.blogPost:hover .blogName{background-color:#0070b7;}
	.blogPost:hover .blogName .blogN{color:white;}
	.blogPost:hover {border:1px solid #0070b7;background-color:#fff;}
	/* .blogPost:hover img{background-color:#f2f5f7;} */
	.blogPost{padding-right:0;padding-left: 0;border:1px solid #737373;}
	.cNote{min-height:40px;transition:all 0.3s ease;padding-top:10px;opacity:0;color:#0070b7;text-align:center;}
	.blogN{text-align:center;color:#737373;display:table-cell;vertical-align:middle;}
	.blogOutside{margin-bottom:30px;padding-bottom:0;text-align:center;}
    
    </style>
    @endpush