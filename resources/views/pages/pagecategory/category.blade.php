@extends('layouts.homeindex')
@section('content')

  @include(template_path_check('/layouts/topmenu'))
  <div style="background-color:white;" class="container-fluid">
<div class="bg row justify-content-center">
<div style="padding-top:2vw;padding-bottom:2vw;text-align:right;" class="col-11 col-md-10">
<a style="margin-right:2vw;"  href="{{route('index')}}" class="router">@lang('site.home')</a> <p style="margin-right:4px;margin-left:4px;" class="router">/</p> <a href="{{route('categories')}}" class="router">@lang('site.categories')</a>
</div>

</div>

<div class="bg row justify-content-center">
<div class="col-11 col-md-10">
<div class="row justify-content-center">
  <ul class="cats_ul col-6 col-md-3">

    @foreach($categories as $cat)
    @if($cat->parent_id==0)
    <li class="cats_li"><a href="{{route('category',['slug' => $cat->slug])}}" class="cats_a" ><p class="cats_p">{{$cat->title}}</p></a><input type="hidden" value="{{$cat->id}}"/></li>
    @if ($cat->subcategories()->count())
        
    <ul class="subcats_ul">
      @foreach($cat->subcategories as $subcat)
      <li class="subcats_li"><a href="{{route('category',['slug' => $subcat->slug])}}" class="subcats_a"><p class="subcats_p">{{$subcat->title}}</p></a><input type="hidden" value="{{$subcat->id}}"/></li>
      @endforeach

    </ul>

    @endif
    @endif
@endforeach
  </ul>
  <div id="catsList" class="col-6 col-md-9">
    @include(template_path_check('/pages/pagecategory/pcontent'),['category' => $category,'products' => $cproducts])


  </div>






</div>
</div>
</div>
</div>

@push('style')
<style>
.cats_li{transition:all 0.3s ease;margin-bottom:1rem;border-radius:8px;width:100%;display:block;list-style-type: none;background-color:#e5f0f8;background-image: url('/images/arrowClosedWh.png');background-position: 93% 50%;background-repeat: no-repeat;background-size: 3%;}
.cats_a{display: table-cell;vertical-align: middle;width: 90%;}
.cats_p{transition:all 0.3s ease;font-size:small;font-weight: 600;color:#737373;margin-top: 0;margin-bottom: 0;padding-top: 0.5rem;padding-bottom: 0.5rem;padding-left: 1rem;padding-right: 1rem;width:91%;}
.cats_li:hover{background-color:#0070b7;}
.cats_li:hover .cats_p{color:white;}
.subcats_ul{height:0;overflow:hidden;padding-left:0;padding-right:0;background-color: #f2f5f7; box-shadow: 0px 13px 20px #3f8bc6;margin-top: -1rem;margin-bottom: 1rem;}
.subcats_p{color:#0070b7;font-size: small;font-weight: 500;width:100%;}
.cats_p_open{color:white;}
.cats_li_open{color:white;background-color:#0070b7;background-image: url('/images/arrowOpen.png');background-size: 6%;background-position: 94.5% 50%;}
.cats_subcats{height:min-content!important;padding-top: 1rem;padding-bottom: 1rem;padding-left: 0;
    padding-right: 0;;min-width: min-content;}
.subcats_li:hover .subcats_p{color:#ff6600;}
.subcats_li{margin-left: 1.5rem;margin-right:1.5rem;}
li{cursor: pointer;}
.subcats_a{width: min-content;}

</style>
@endpush

@push('scripts')
<script>
$(document).ready(function(){





$('.cats_li').each(function(){
  $( this ).click(function(){
    console.log('cat pressed');
$( this ).toggleClass('cats_li_open');
$( this ).find('.cats_p').toggleClass('cats_p_open');
$( this ).next().toggleClass('cats_subcats');

  });
});

});
</script>
@endpush
@endsection
