@if ($packages)
<div class="container">
    <div class="row">
        <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-12 ">
            <div class="row">
            @foreach ($packages as $item)
                <div class="col-md-6 package">
                    <div class="package-box">
                        <div class="package-title">{{ $item->name }}</div>
                        <div class="package-content">
                           <div class="package-price">{{ $item->real_price }} TL</div>
                           <div class="package-discount-price">{{ $item->discount_price }} TL</div>
                           <div class="package-total">{{ $item->discount_price }} X {{ $item->total_use }} = {{ $item->discount_price*$item->total_use }} TL</div>
                        </div>
                        <a class="package-btn" href="{{ route('site.package.index', ['id'=>$item->id]) }}">@lang('site.package_buy')</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endif