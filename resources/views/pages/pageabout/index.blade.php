@extends('layouts.homeindex')
@section('content')

  @include(template_path_check('/layouts/topmenu'))
  <div style="background-color:white;color:#737373" class="container-fluid">
<div class="row justify-content-center">
  <div style="padding-top:2vw;padding-bottom:2vw;text-align:right;color:#737373!important;" class="col-10">
  <a style="margin-right:2vw;color:#737373!important;"  href="{{route('index')}}" class="router">@lang('site.home')</a> <p style="margin-right:4px;margin-left:4px;" class="router">/</p> <a style="color:#737373!important;" href="{{route('contact')}}" class="router">@lang('site.about_head')</a>
  </div>
<div class="col-11 col-sm-10 col-md-8">
  <p class="text-center w-responsive mx-auto mb-5">@lang('site.about_first_par')</p>

<div class="row justify-content-center">

<img src="/images/logo.png" class="aboutlogo col-9 col-sm-6 col-md-4"/>

</div>

      <p class="text-center w-responsive mx-auto mb-5">@lang('site.about_second_par')</p>

</div>

</div>


</div>
@endsection
@push('style')
<style>
.aboutlogo{
  margin-top:15px;
  margin-bottom:50px;
}
.router{text-align:right;color:#737373!important;font-style:italic;font-size:small;float:right;}

</style>
@endpush
