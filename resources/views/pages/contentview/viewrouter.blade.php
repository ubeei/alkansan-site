@if ($page->contents)
    @foreach ($page->contents as $contentitem)        
        @if ($contentitem->page_content_type->path == "content")
            @include(template_path_check('/pages/contentview/standart'),['contentitem',$contentitem])
        @elseif($contentitem->page_content_type->path == "imagegallery-1")
            @include(template_path_check('/pages/contentview/gallery1'),['contentitem',$contentitem])
        @elseif($contentitem->page_content_type->path == "map")
            @include(template_path_check('/pages/contentview/map'),['contentitem',$contentitem])
        @endif
    @endforeach
@endif