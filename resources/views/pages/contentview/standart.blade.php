@if ($contentitem)
{{-- 
@if ($contentitem->content_style_id == 1)
    <link rel="stylesheet" href="{{ asset('css/about-area.css') }}">
@endif

<div class="about-area">
    @if ($contentitem->title!="")
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!--Start Heading Title-->
                    <div class="heading-title">
                            
                            <h2 class="f-weight-700 margin-0">{{ $contentitem->title }}</h2>
                            <div class="bordershep"></div>
                    </div>
                </div>
            </div>
        </div>
    @endif --}}
    @if ($contentitem->html!="")
        {!! string_search($contentitem->html) !!}
    @endif
{{-- </div> --}}
@endif
@if ($contentitem)
@push('style')
    @if ($contentitem->css!="")
    <style>
        {!! $contentitem->css !!}
    </style>
    @endif

@endpush
@endif