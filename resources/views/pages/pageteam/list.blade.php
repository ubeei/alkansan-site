@if ($teams)
    <div class="speaker_area">
		<div class="container">
            <div class="row">
                @foreach ($teams as $teamitem)
                    <div class="col-sm-6 col-xl-3">
                        <div class="speaker">
                            <div class="sk-img">
                                <div class="img">
                                    <img class="img-fluid img-thumbnail img-responsive" src="{{ $teamitem->image }}" alt="">
                                    {{-- <div class="overlay d-flex justify-content-center">
                                        <div class="slink">
                                            <a href="#">
                                                <i class="fa fa-facebook"></i>
                                                <i class="fa fa-twitter"></i>
                                                <i class="fa fa-linkedin"></i>
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="sk-content">
                                <a href="javascript:void(0)"><h4>{{ $teamitem->first_name }} {{ $teamitem->last_name }}</h4></a>
                                <p>{{ $teamitem->department }}</p>
                                <p>{{ $teamitem->email }}</p>
                            </div>
                        </div>
                    </div>
            
                    @endforeach
                </div>
            </div>
        </div>
    @endif