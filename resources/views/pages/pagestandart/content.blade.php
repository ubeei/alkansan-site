@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
        @include(template_path_check('/layouts/topmenu2'))

    @if ($page)
        @include(template_path_check('pages.contentview.viewrouter'),['page',$page])
    @endif

</div>
@endsection