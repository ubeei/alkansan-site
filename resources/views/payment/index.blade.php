@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu2'))
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="slidertitle">@lang('site.sliderbtn')</h1>
            </div>
           
            <div class="col-xl-6 col-lg-6 col-md-6 offset-md-3">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div id="iyzipay-checkout-form" class="responsive"></div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
    
    {!! $checkoutFormInitialize->getCheckoutFormContent() !!}
    
@endpush