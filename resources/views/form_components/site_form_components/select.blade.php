<div class="select form-group {{ $errors->has($name) ? 'state-error' : '' }}">
	@php
		if ($errors->has($name)) { $labelclass=["class" => "control-label"]; 
	} else { 
		$labelclass=["class" => "control-label"]; }
	@endphp
	{{ Form::label($name, $label_name, $labelclass) }}
		
		
		{{ Form::select($name, $list , $value, array_merge(['class' => 'form-control','placeholder' => $placeholder])) }}
	
	@if ($errors->has($name))
		<div class="note note-error">
			{{ $errors -> first($name) }}
		</div>
	@endif
</div>
