<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}" >
	@if ($label_name!=null)
		{{ Form::label($name, $label_name, ["class" => "control-label ",'style' => 'text-align: left;']) }}
	@endif
	<?php $t=array_merge(['id' => $name ,'class' => 'form-control','placeholder' => $placeholder],(array)$attributes);  ?>
		{{ Form::text($name, $value, $t) }}
	@if ($errors->has($name))
		<span class="help-block">
			<strong> {{ $errors -> first($name) }} </strong>
		</span>
	@endif
</div>