<section>		
        @php
            if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; } else { $labelclass=["class" => "label"]; }
        @endphp
       
        <div class="inline-group">
            <label class="checkbox">{{ Form::checkbox($name,$value,$is_checked) }}<i></i>{{$label_name}}</label>
    
            @if ($errors->has($name))
                <div class="note note-error">
                    {{ $errors -> first($name) }}
                </div>
            @endif
        </div>
    </section>