@php
	if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; $formerrror='has-error'; } else { $labelclass=["class" => "label"]; $formerrror=''; }
@endphp
<div class="form-group {{ $formerrror }}">
	
	{{ Form::label($name, $label_name, $labelclass) }}
	
	{{ Form::select($name, $list , $value, array_merge(['class' => 'form-control','placeholder' => $placeholder])) }}
	@if ($errors->has($name))
		<div class="help-block">
			{{ $errors -> first($name) }}
		</div>
	@endif
</div>
