@php
	if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; $formerrror='has-error'; } else { $labelclass=["class" => "label"]; $formerrror=''; }
@endphp
<div class="form-group {{ $formerrror }}">
    <div class="checkbox">
        <label>{{ Form::checkbox($name,$value,$is_checked) }}<i></i>{{$label_name}}</label>
    </div>
    
    @if ($errors->has($name))
        <div class="help-block">
            {{ $errors -> first($name) }}
        </div>
    @endif
</div>