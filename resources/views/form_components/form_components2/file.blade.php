@php
	if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; } else { $labelclass=["class" => "label"]; }
@endphp
<section>
	{{ Form::label($name, $label_name, $labelclass) }}
	<div class="file_image">
		<!-- <div class="removeimage">
			<i class="fa fa-trash-o"></i>
		</div> -->
	<img src="https://dummyimage.com/200x200/b3b1b3/000000.png&text={{ $name }}" id="image_{{ $name }}" >
	<div id="progress" class="progress"">
			<div class="progress-bar progress-bar-success"></div>
		</div>
	</div>

	<div class="input input-file">
		<?php $t=array_merge(['style' => 'display:none;']);  ?>
		{{ Form::text($name, null, $t) }}


	<span class="button"><input type="file" id="upload_{{ $name }}" name="upload_{{ $name }}">@lang('form.form_input_browse')</span>
	

		@if ($errors->has($name))
		<div class="note note-error">
				{{ $errors -> first($name) }}
			</div>
		@endif
	</div>
</section>
<style>
.progress{
	height: 5px;
    margin-bottom: 0px;
}
.progress-bar-success{
	background-color: red;
}
section .file_image{
	width: 250px;
	position: relative;
}
section .file_image .removeimage{
	position: absolute;
	right: 5px;
    top: 2px;
    color: red;
	cursor: pointer;
}

section .file_image img{
	width: 100%;
}
</style>
