<section>
    @php
    if ($errors->has($name)) { $labelclass=["class" => "label state-error"]; } else { $labelclass=["class" => "label"]; }
@endphp
    @if ($label_name!=null)
	{{ Form::label($name, $label_name, $labelclass) }}
	@endif

        <?php $t=array_merge(['id'=> $name,'class' => '','placeholder' => $placeholder],(array)$attributes);  ?>
        {{ Form::password($name, $t) }}

        @if ($errors->has($name))

            <div class="note note-error">
                <strong>{{ $errors->first($name) }}</strong>
            </div>

        @endif

</section>