<section>

    @if ($label_name!=null)
	{{ Form::label($name, $label_name, $labelclass) }}
	@endif

    <div class="col-md-8">
        <?php $t=array_merge(['id'=> $name,'class' => '','placeholder' => $placeholder],(array)$attributes);  ?>
        {{ Form::password($name, $t) }}

        @if ($errors->has($name))

            <div class="note note-error">
                <strong>{{ $errors->first($name) }}</strong>
            </div>

        @endif

    </div>
</section>