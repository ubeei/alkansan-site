'<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <i class="fa fa-user"></i>
        </div>
        <div class="pull-left info">
          <p>{{ env('APP_NAME') }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li>
          <a href="{{ route('page.index') }}">
            <i class="fa fa-th"></i> <span>Pages</span>
          </a>
		</li>
		<li>
			<a href="{{ route('property.index') }}">
			  <i class="fa fa-th"></i> <span>Ürün Özellikleri</span>
			</a>
		</li>
		<li>
			<a href="{{ route('category.index') }}">
			  <i class="fa fa-th"></i> <span>Kategoriler</span>
			</a>
    </li>
    <li>
        <a href="{{ route('product.index') }}">
          <i class="fa fa-th"></i> <span>Ürünler</span>
        </a>
      </li>
        <li>
          <a href="{{ route('slider.index') }}">
            <i class="fa fa-pie-chart"></i>
            <span>Slider</span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.blog.index') }}">
            <i class="fa fa-th"></i> <span>Blog</span>
          </a>
        </li>
        <li>
          <a href="{{ route('setting.index') }}">
            <i class="fa fa-calendar"></i> <span>Setting</span>
          </a>
        </li>
        <li>
          <a href="{{ url('admin/member-list') }}">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
          </a>
        </li>
        <li>
            <a href="{{ route('admin.package.index') }}">
                <i class="fa fa-archive"></i> <span>Packege</span>
            </a>
        </li>
        <li>
            <a href="{{ route('extra.index') }}">
                <i class="fa fa-plus"></i> <span>Extra</span>
            </a>
        </li>
        <li>
            <a href="{{ route('time.index') }}">
                <i class="fa fa-clock-o"></i> <span>Time</span>
            </a>
		</li>
		<li>
            <a href="{{ route('admin.appointment.index') }}">
                <i class="fa fa-calendar"></i> <span>Randevu</span>
            </a>
		</li>
		<li>
			<a href="{{ route('admin.accounting.index') }}">
				<i class="fa fa-calendar"></i> <span>Randevu Muhasebe</span>
			</a>
		</li>
		<li>
			<a href="{{ route('admin.accounting.paymentindex') }}">
				<i class="fa fa-calendar"></i> <span>Paket Muhasebe</span>
			</a>
		</li>
		<li>
			<a href="{{ route('admin.coupon.index') }}">
				<i class="fa fa-ticket"></i> <span>Kupon</span>
			</a>
		</li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
