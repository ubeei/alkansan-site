@extends('admin.layouts.app')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
	  <h3 class="box-title">Ürün Ekle</h3>
	</div>
		{!! Form::open(['route' => ['product.store' ,null], 'method' => "POST", "class" =>"smart-form", 'files' => true]) !!}
		@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
		<fieldset>
			{{ Form::dText('name','Ürün Adı') }}
			{{ Form::dText('meta_desc','Meta Desc') }}
			{{ Form::dText('meta_keyword','Meta Keyword') }}
			{{ Form::dText('code','Ürün Kodu') }}
			<div class="form-group ">
				<label for="meta_desc" class="label">Bağlı Kategori</label>
				<div class="overhidebox">
					<ul class="tree2">
						{{ subcat($categories,old('category_id',array())) }}
					</ul>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="label">Ürün özellikleri</label>
				<div class="properties">
					
				</div>
			</div>
			<div style="clear:both;"></div>
			{{ Form::dText('brand','Marka') }}
			{{ Form::dTextarea('short_description','Kısa Açıklama') }}

			
			{{ Form::dTextarea('description','Açıklama') }}
			<div id="progress" class="progress">
				<div class="progress-bar"></div>
				<div class="progress-bar2"></div>
			</div>
			<p class="newphototext">Eklediğiniz fotoğraf ededi <span id="fotoadet">0/8</span></p>
			<ul id="editable">
				<?php $xk=0; ?>
				@if (old('images'))
				@foreach (old('images') as $item)
				<li id="{{ $xk }}" class="fileitem"><img src="{{ $item }}" /><input type="hidden" name="images[]" value="{{ $item }}"><a class="fileitemdelete fa fa-trash-o" href="javascript:void(0);" rel="{{ $xk }}"></a></li>
				@php
					$xk=$xk+1;
				@endphp
				@endforeach
				@endif
				<li id="9999" class="fileitem fileup">
					<div class="fileinput-button">
						<span class="photo-icon"></span>
						<span class="photo-text">Fotoğraf ekle</span>
						<input id="fileupload" type="file" name="upload_image" accept="image/*" multiple>
					</div>
				</li>
			</ul>
			<div style="clear:both;"></div>
			{{ Form::dFile2('pdf','PDF') }}
			{{ Form::dFile2('family_pdf','PDF') }}

		<br/>

		</fieldset>	
		{{ Form::dSubmit('save','Insert') }}
		<br/>
	{!! Form::close() !!}


</div>
@php
	function subcat($categories,$selectedin=array())
	{
		if ($categories) {
			echo "<ul>";
			foreach ($categories as $category){
				echo '<li><input type="checkbox" class="catlist" name="category_id[]" value="'.$category->id.'"';
				if (in_array($category->id,$selectedin)) {
					echo 'checked="checked"';
				}
				echo ' >'.$category->title.'</li>';
				if ($category->subcategories) {
					subcat($category->subcategories,$selectedin);
				}
			}
			echo "</ul>";
		}
	}
@endphp
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
<style>
	.overhidebox{
		height: 200px;
		overflow: overlay;		
	}
	ul.tree2, ul.tree2 ul {
    list-style: none;
     margin: 0;
     padding: 0;
   } 
   ul.tree2 ul {
     margin-left: 10px;
   }
   ul.tree2 li {
     margin: 0;
     padding: 0 7px;
     line-height: 20px;
     color: #369;
     font-weight: bold;
     border-left:1px solid rgb(100,100,100);

   }
   ul.tree2 li:last-child {
       border-left:none;
   }
   ul.tree2 li:before {
      position:relative;
      top:-0.3em;
      height:1em;
      width:12px;
      color:white;
      border-bottom:1px solid rgb(100,100,100);
      content:"";
      display:inline-block;
      left:-7px;
   }
   ul.tree2 li:last-child:before {
      border-left:1px solid rgb(100,100,100);   
   }
	.input-file {
		position: relative;
		overflow: hidden;
		display: inline-block;
		
		padding: 6px 12px;
		margin-bottom: 0;
		font-size: 14px;
		font-weight: 400;
		line-height: 1.42857143;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		-ms-touch-action: manipulation;
		touch-action: manipulation;
		cursor: pointer;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		background-image: none;
		border: 1px solid transparent;
		border-radius: 4px;
		color: #fff;
		background-color: #5cb85c;
		border-color: #4cae4c;
	}
	.input-file input {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		opacity: 0;
		-ms-filter: 'alpha(opacity=0)';
		font-size: 200px !important;
		direction: ltr;
		cursor: pointer;
	}
</style>
@endsection
@section('js')
<script>
	$(function () {
		$('.catlist').on('change',function() {
			
			property();
		})
	});
	function property() {
		$.ajax({
				url:"{{ route('admin.product.property') }}",
				type:'POST',
				data: $('.smart-form').serialize(),
				dataType: 'html',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function( data ) {
					$('.properties').html(data);
				}
			});
	} 
</script>
<script src="{{ asset('admin/js/sortable/Sortable.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="{{ asset('admin/js/sortable/ng-sortable.js') }}"></script>
<script>
	var xk = {{ $xk }};
		$(function () {
			'use strict';
			// Change this to the location of your server-side upload handler:
			filefonk();
			property();
		});
		function filefonk () {
			var url = "{{ route('product.image') }}";
			$('#fileupload').fileupload({
				url: url,
				dataType: 'json',
				method: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				done: function (e, data) {
					if (data.result.error==0) {
	
						var st = $('#fotoadet').text();
						var my = st.split('/');
						var xy=parseInt(my[0], 10)+1;
	
						$('#fotoadet').text(xy+'/'+my[1]);
					//$.each(data.result, function (index, file) {
						var file = data.result;
						//alert(file.id);
						$('.fileup').remove();
						var h='<li id="'+xk+'" class="fileitem"><img src="'+file.url+'" /><input type="hidden" name="images[]" value="'+file.url+'"><a class="fileitemdelete fa fa-trash-o" href="javascript:void(0);" rel="'+xk+'"></a></li>';
						$('#editable').append(h);
						$('#editable').append('<li id="9999" class="fileitem fileup" draggable="false" style="display: block;"><div class="fileinput-button" style="display: inline-block;"><span class="photo-icon"></span><span class="photo-text">Fotoğraf ekle</span><input id="fileupload" type="file" name="upload_image" accept="image/*" multiple=""></div></li>');
							filefonk();
						if (xy==my[1]) {
							$('.fileinput-button').addClass('hide');
							$('.fileinput-button').removeClass('show');
							$('.fileup').addClass('hide');
							$('.fileup').removeClass('show');
						}else{
							$('.filinput-buttone').addClass('show');
							$('.filinput-buttone').removeClass('hide');
							$('.fileup').addClass('show');
							$('.fileup').removeClass('hide');
							$('#progress .progress-bar2').width(0);
							$('#progress .progress-bar2').addClass('hide');
							$('#progress .progress-bar2').removeClass('show');
							$('#progress .progress-bar').addClass('show');
							$('#progress .progress-bar').removeClass('hide');
							var w = $('#progress').width();
							var k = w/my[1];
							var xyz = k*xy;
							$('#progress .progress-bar').width(xyz);
							
							
	
						}
						//$('<p/>').text(file.name).appendTo('#editable');
					//});
						xk = xk+1;
					}else{
						alert(data.result.error);
					}
				},
				progressall: function (e, data) {
						$('#progress .progress-bar').addClass('hide');
						$('#progress .progress-bar').removeClass('show');
						$('#progress .progress-bar2').addClass('show');
						$('#progress .progress-bar2').removeClass('hide');
	
						var progress = parseInt(data.loaded / data.total * 100, 10);
						$('#progress .progress-bar2').css(
							'width',
							progress + '%'
						);
										
				}
			}).prop('disabled', !$.support.fileInput)
				.parent().addClass($.support.fileInput ? undefined : 'disabled');
		}
		</script>
<script>
		//$(document).ready(function(){

	// Editable list
	if ($('#editable').length>0) {
	var editableList = Sortable.create(editable, {
	  filter: ".fileitemdelete, .fileitemrotate",
	  onFilter: function (evt) {
		if (evt.item.className=='fileitem') {
			var id = evt.item.id;
			var imgx = $('#'+id).children('img')
			var path = imgx[0].src;
			var ctrl = evt.target;
			if (Sortable.utils.is(ctrl, ".fileitemdelete")) {
				$.ajax({
					url:"{{ route('product.imageremove') }}",
					type:'POST',
					data: 'img=' + path,
					dataType: 'json',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					success: function( json ) {
						if (json.error==0) {
							var st = $('#fotoadet').text();
							var my = st.split('/');
							var xy=parseInt(my[0], 10)-1;
							$('#fotoadet').text(xy+'/'+my[1]);
							var el = editableList.closest(evt.item); // get dragged item
							el && el.parentNode.removeChild(el);
							if (xy==my[1]) {
								
								$('.fileinput-button').addClass('hide');
								$('.fileinput-button').removeClass('show');
								$('.fileup').addClass('hide');
								$('.fileup').removeClass('show');
								$('#progress .progress-bar2').addClass('hide');
								$('#progress .progress-bar2').removeClass('show');
								$('#progress .progress-bar').addClass('show');
								$('#progress .progress-bar').removeClass('hide');
								var w = $('#progress').width();
								var k = w/my[1];
								var xyz = k*xy;
								$('#progress .progress-bar').width(xyz);
							}else{
								$('.fileinput-button').addClass('show');
								$('.fileinput-button').removeClass('hide');
								$('.fileup').addClass('show');
								$('.fileup').removeClass('hide');
								$('#progress .progress-bar2').width(0);
								$('#progress .progress-bar2').addClass('hide');
								$('#progress .progress-bar2').removeClass('show');
								$('#progress .progress-bar').addClass('show');
								$('#progress .progress-bar').removeClass('hide');
								var w = $('#progress').width();
								var k = w/my[1];
								var xyz = k*xy;
								$('#progress .progress-bar').width(xyz);

							}
						}
					}
				});
			}else{}
		}
	}
	});
	}
	
	</script>
<script src="{{ asset('js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/upload/jquery.fileupload.js') }}"></script>
	<script >
		
			$(function () {
				'use strict';
				

				pdfupload();
				$('#image_pdf').on('click',function() {
					$('#upload_pdf').trigger('click');
				});
				var i = $('#pdf').val();
				if (i!="") {
					$('#image_pdf').attr('src',i);
				}

				family_pdfupload();
				$('#image_family_pdf').on('click',function() {
					$('#upload_family_pdf').trigger('click');
				});
				var i = $('#family_pdf').val();
				if (i!="") {
					$('#image_family_pdf').attr('src',i);
				}
			});
		

			function pdfupload () {
				var url = "{{ route('product.pdf') }}";
				$('#upload_pdf').fileupload({
					url: url,
					dataType: 'json',
					method: "POST",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					done: function (e, data) {
						if (data.result.error==0) {
							var file = data.result;
							//alert(file.id);
							$('#image_pdf').attr('src',file.url);
							$('#pdf').val(file.url);
						}else{
							alert(data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('#progress .progress-bar').css(
                			'width',
                			progress + '%'
            			);
											
					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}

			$(function () {
				$('.removepdf').on('click',function() {
					var imgpath = $('#pdf').val();
					
					$.ajax({
						method: "POST",
						url: "{{ route('product.pdfremove') }}",
						data: { img: imgpath },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {
						$('#image_pdf').attr('src',data.url);
						$('#pdf').val('');
					});
				});
			});
			

			function family_pdfupload () {
				var url = "{{ route('product.familypdf') }}";
				$('#upload_family_pdf').fileupload({
					url: url,
					dataType: 'json',
					method: "POST",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					done: function (e, data) {
						if (data.result.error==0) {
							var file = data.result;
							//alert(file.id);
							$('#image_family_pdf').attr('src',file.url);
							$('#family_pdf').val(file.url);
						}else{
							alert(data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('#progress .progress-bar').css(
                			'width',
                			progress + '%'
            			);
											
					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}

			$(function () {
				$('.removefamily_pdf').on('click',function() {
					var imgpath = $('#family_pdf').val();
					
					$.ajax({
						method: "POST",
						url: "{{ route('product.pdfremove') }}",
						data: { img: imgpath },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {
						$('#image_family_pdf').attr('src',data.url);
						$('#family_pdf').val('');
					});
				});
			});
	</script>
		<link rel="stylesheet" href="{{ asset('admin/css/bootstrap-multiselect.css') }}" type="text/css">
		<script type="text/javascript" src="{{ asset('admin/js/bootstrap-multiselect.js') }}"></script>
		<script>
		$(document).ready(function() {
			$('#lists').multiselect({
				buttonClass: 'form-control',
				  buttonWidth: '100%',
				  buttonText: function(options) {
					if (options.length == 0) {
						return 'None selected';
					}
					else if (options.length > 999) {
						return options.length + ' selected ';
					}
					else {
						var selected = '';
						options.each(function() {
							selected += $(this).text() + ', ';
						});
						return selected.substr(0, selected.length -2) + '';
					}
				},
			});
		});
		</script>


<!-- Include CSS for icons. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
 
<!-- Include Editor style. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />
 
<!-- Create a tag that we will use as the editable area. -->
<!-- You can use a div tag as well. -->
 

 
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('admin/js/froala/froala_editor.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/froala/plugins/image_manager.min.js') }}"></script>
<!-- Initialize the editor. -->
<script>
	
  $(function() {
	$.FroalaEditor.DefineIcon('btn-rows', {NAME: 'plus'});
	$.FroalaEditor.RegisterCommand('btn-rows', {
		title: 'Advanced options',
		type: 'dropdown',
		focus: true,
		  undo: true,
		  refreshAfterCallback: true,

	});
	$('#description').froalaEditor({
		heightMin: 500,
		toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo','|','btn-rows'],
		// Set the image upload parameter.
		imageUploadParam: 'image_param',

		// Set the image upload URL.
		imageUploadURL: "{{ route('contetupload.image') }}",

		// Additional upload params.
		imageUploadParams: {id: 'my_editor'},
		requestHeaders: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('description')
		},

		// Set request type.
		imageUploadMethod: 'POST',

		// Set max image size to 5MB.
		imageMaxSize: 5 * 1024 * 1024,

		// Allow to upload PNG and JPG.
		imageAllowedTypes: ['jpeg', 'jpg', 'png']
		})
		.on('froalaEditor.image.beforeUpload', function (e, editor, images) {
		// Return false if you want to stop the image upload.
		})
		.on('froalaEditor.image.uploaded', function (e, editor, response) {
		// Image was uploaded to the server.
		})
		.on('froalaEditor.image.inserted', function (e, editor, $img, response) {
		// Image was inserted in the editor.
		})
		.on('froalaEditor.image.replaced', function (e, editor, $img, response) {
		// Image was replaced in the editor.
		})
		.on('froalaEditor.image.error', function (e, editor, error, response) {
		// Bad link.
		/*if (error.code == 1) { ... }

		// No link in upload response.
		else if (error.code == 2) { ... }

		// Error during image upload.
		else if (error.code == 3) { ... }

		// Parsing response failed.
		else if (error.code == 4) { ... }

		// Image too text-large.
		else if (error.code == 5) { ... }

		// Invalid image type.
		else if (error.code == 6) { ... }

		// Image can be uploaded only to same domain in IE 8 and IE 9.
		else if (error.code == 7) { ... }*/

		// Response contains the original server response to the request if available.
	
	})
  });
</script>
@endsection
