@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('page.create',[Null,'page_id'=>$page_id]) }}" style="float: right;margin: 10px 0px;">Add</a>
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
				<h3 class="box-title">Pages</h3>
				</div>
				<div class="box-body">
					{!! Form::open(['route' => ['page.order', Null,  'page_id' => $page_id], 'method' => "POST",]) !!}

						<table class="table table-bordered">
							<thead>
								<tr>
									<th style="width: 50px;">Short NO</th>
									@if ($page_id==0)
									<th>Home Page</th>
									@endif
									<th>Page Name</th>
									<th style="width: 10px;">Status</th>
									<th>Action</th>
								</tr>
							</thead>

							@if ($pages)
								<tbody>
									@foreach ($pages as $page)
										<tr>
											<td >{{ Form::dText2('order['.$page->id.']','',null,$page->order,['style'=> 'width: 50px;text-align: center;']) }}</td>
											@if ($page_id==0)
										<td style="width: 100px; text-align:center;"><a href="{{ route('page.home', ['id'=>$page->id]) }}">@php
											if($page->homepage==1){ $color="yellow"; }else{ $color="#c5c5c5"; }
										@endphp<i class="fa fa-star" style="color:{{ $color }};font-size: 22px;"></i></a></td>
											@endif

											<td><a href="{{ route('page.index', [null,'page_id'=>$page->id]) }}">{{$page->title}}</a></td>
											<td style="text-align: center;">
												<a href="{{ route('page.status', ['page_id'=>$page->id] ) }}">@if ($page->status==1) <i class="fa fa-eye" style="color:#356635;    font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
											</td>
											<td>
												@if ($page->page_type->path=='team')
													<a href="{{ route('team.index', ['page_id'=>$page->id] ) }}" class="btn btn-primary action"><i class="fa fa-list"></i></a>
												@endif
												@if ($page->page_type->path == "sponsor")
													<a href="{{ route('sponsor.index', ['page_id'=>$page->id] ) }}" class="btn btn-primary action"><i class="fa fa-list"></i></a>
												@endif
												@if ($page->page_type->path == "tourgroup")
													<a href="{{ route('tourgroup.index', ['page_id'=>$page->id] ) }}" class="btn btn-primary action"><i class="fa fa-list"></i></a>
												@endif
												@if ($page->page_type->path == "pagecategory")
													<a href="{{ route('pagecategory.index', ['page_id'=>$page->id] ) }}" class="btn btn-primary action"><i class="fa fa-list"></i></a>
												@endif
												@if ($page->page_type->path == "pagecategorydetail")
													<a href="{{ route('pagecategorydetail.index', ['page_id'=>$page->id] ) }}" class="btn btn-primary action"><i class="fa fa-list"></i></a>
												@endif
												@if ($page->page_type->path == "pageblog")
													<a href="{{ route('pageblog.index', ['page_id'=>$page->id] ) }}" class="btn btn-primary action"><i class="fa fa-list"></i></a>
												@endif

												<a href="{{ route('pagecontent.index', [null,'type' => $page->page_type_id,'oid'=>$page->id]) }}" class="btn btn-primary action"><i class="fa fa-file"></i></a>
												<a href="{{ route('page.edit', ['id'=>$page->id]) }}" class="btn btn-primary action">Edit</a>
												<a href="{{ route('page.destroy', ['id'=>$page->id]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
											</td>
										</tr>
									@endforeach
									<tr>
										<td>{{ Form::dSubmit('save','Short Save') }}</td>
										@if ($page_id==0)
										<td colspan="4"></td>
										@else
										<td colspan="3"></td>
										@endif
									</tr>
								</tbody>
							@endif
						</table>
						{!! Form::close() !!}

					</div>
			</div>
		</div>
	</div>
</div>
@endsection
