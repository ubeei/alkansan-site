@extends('admin.layouts.app')

@section('content')
<div class="content">
	<section id="widget-grid" class="">
		<article class="col-sm-12 col-md-12 col-lg-6">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Page Category Edit</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
					
						{!! Form::model($pagecategory, ['route' => ['pagecategory.update' ,$pagecategory->id,  'page_id' => $page_id ], 'method' => "put", "class" =>"smart-form", 'files' => true]) !!}
							<fieldset>
							{{ Form::dText('title','Title') }}
							{{ Form::dText('meta_desc','Meta Desc') }}
							{{ Form::dText('meta_keyword','Meta Keyword') }}
							{{ Form::dTextarea('description','Açıklama') }}

							{{ Form::dFile2('image','Image') }}
							{{ Form::dFile2('pdf','PDF') }}
							
							{{ Form::hidden('page_id', $page_id) }}	
							<br/>
							</fieldset>	
							{{ Form::dSubmit('save','Update') }}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</article>
	</section>
</div>

@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
<style>
.input-file {
    position: relative;
    overflow: hidden;
	display: inline-block;
	
	padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
	border-radius: 4px;
	color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.input-file input {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    opacity: 0;
    -ms-filter: 'alpha(opacity=0)';
    font-size: 200px !important;
    direction: ltr;
    cursor: pointer;
}
</style>
@endsection
@section('js')
<script src="{{ asset('js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/upload/jquery.fileupload.js') }}"></script>
	<script >
		
			$(function () {
				'use strict';
				
				imageupload();
				$('#image_image').on('click',function() {
					$('#upload_image').trigger('click');
				});
				var i = $('#image').val();
				if (i!="") {
					$('#image_image').attr('src',i);
				}

				pdfupload();
				$('#image_pdf').on('click',function() {
					$('#upload_pdf').trigger('click');
				});
				var i = $('#pdf').val();
				if (i!="") {
					$('#image_pdf').attr('src',i);
				}
			});
			function imageupload () {
				var url = "{{ route('pagecategory.image') }}";
				$('#upload_image').fileupload({
					url: url,
					dataType: 'json',
					method: "POST",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					done: function (e, data) {
						if (data.result.error==0) {
							var file = data.result;
							//alert(file.id);
							$('#image_image').attr('src',file.url);
							$('#image').val(file.url);
						}else{
							alert(data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('#progress .progress-bar').css(
                			'width',
                			progress + '%'
            			);
											
					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}

			
			$(function () {
				$('.removeimage').on('click',function() {
					var imgpath = $('#image').val();
					
					$.ajax({
						method: "POST",
						url: "{{ route('pagecategory.imageremove') }}",
						data: { img: imgpath },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {
						$('#image_image').attr('src',data.url);
						$('#image').val('');
					});
				});
			});

			function pdfupload () {
				var url = "{{ route('pagecategory.pdf') }}";
				$('#upload_pdf').fileupload({
					url: url,
					dataType: 'json',
					method: "POST",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					done: function (e, data) {
						if (data.result.error==0) {
							var file = data.result;
							//alert(file.id);
							$('#image_pdf').attr('src',file.url);
							$('#pdf').val(file.url);
						}else{
							alert(data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('#progress .progress-bar').css(
                			'width',
                			progress + '%'
            			);
											
					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}

			$(function () {
				$('.removepdf').on('click',function() {
					var imgpath = $('#pdf').val();
					
					$.ajax({
						method: "POST",
						url: "{{ route('pagecategory.pdfremove') }}",
						data: { img: imgpath },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {
						$('#image_pdf').attr('src',data.url);
						$('#pdf').val('');
					});
				});
			});
			
	</script>
		<link rel="stylesheet" href="{{ asset('admin/css/bootstrap-multiselect.css') }}" type="text/css">
		<script type="text/javascript" src="{{ asset('admin/js/bootstrap-multiselect.js') }}"></script>
		<script>
		$(document).ready(function() {
			$('#lists').multiselect({
				buttonClass: 'form-control',
				  buttonWidth: '100%',
				  buttonText: function(options) {
					if (options.length == 0) {
						return 'None selected';
					}
					else if (options.length > 999) {
						return options.length + ' selected ';
					}
					else {
						var selected = '';
						options.each(function() {
							selected += $(this).text() + ', ';
						});
						return selected.substr(0, selected.length -2) + '';
					}
				},
			});
		});
		</script>


<!-- Include CSS for icons. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
 
<!-- Include Editor style. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />
 
<!-- Create a tag that we will use as the editable area. -->
<!-- You can use a div tag as well. -->
 

 
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('admin/js/froala/froala_editor.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/froala/plugins/image_manager.min.js') }}"></script>
<!-- Initialize the editor. -->
<script>
	
  $(function() {
	$.FroalaEditor.DefineIcon('btn-rows', {NAME: 'plus'});
	$.FroalaEditor.RegisterCommand('btn-rows', {
		title: 'Advanced options',
		type: 'dropdown',
		focus: true,
		  undo: true,
		  refreshAfterCallback: true,

	});
	$('#description').froalaEditor({
		heightMin: 500,
		toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'embedly', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo','|','btn-rows'],
		// Set the image upload parameter.
		imageUploadParam: 'image_param',

		// Set the image upload URL.
		imageUploadURL: "{{ route('contetupload.image') }}",

		// Additional upload params.
		imageUploadParams: {id: 'my_editor'},
		requestHeaders: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('description')
		},

		// Set request type.
		imageUploadMethod: 'POST',

		// Set max image size to 5MB.
		imageMaxSize: 5 * 1024 * 1024,

		// Allow to upload PNG and JPG.
		imageAllowedTypes: ['jpeg', 'jpg', 'png']
		})
		.on('froalaEditor.image.beforeUpload', function (e, editor, images) {
		// Return false if you want to stop the image upload.
		})
		.on('froalaEditor.image.uploaded', function (e, editor, response) {
		// Image was uploaded to the server.
		})
		.on('froalaEditor.image.inserted', function (e, editor, $img, response) {
		// Image was inserted in the editor.
		})
		.on('froalaEditor.image.replaced', function (e, editor, $img, response) {
		// Image was replaced in the editor.
		})
		.on('froalaEditor.image.error', function (e, editor, error, response) {
		// Bad link.
		/*if (error.code == 1) { ... }

		// No link in upload response.
		else if (error.code == 2) { ... }

		// Error during image upload.
		else if (error.code == 3) { ... }

		// Parsing response failed.
		else if (error.code == 4) { ... }

		// Image too text-large.
		else if (error.code == 5) { ... }

		// Invalid image type.
		else if (error.code == 6) { ... }

		// Image can be uploaded only to same domain in IE 8 and IE 9.
		else if (error.code == 7) { ... }*/

		// Response contains the original server response to the request if available.
	
	})
  });
</script>
@endsection
