@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('pagecategory.create',[Null,'page_id'=>$page_id]) }}" style="float: right;margin: 10px 0px;">Add</a>
					
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
				<h3 class="box-title">Category</h3>
			</div>
			<div class="box-body">
						{!! Form::open(['route' => ['pagecategory.order', Null,  'page_id' => $page_id], 'method' => "POST"]) !!}

					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Short NO</th>
                        		<th>Title</th>
                        		<th style="width: 10px;">Status</th>
                        		<th>Action</th>
                        	</tr>
                      	</thead>

						@if ($categories)
							<tbody>
								@foreach ($categories as $category)
									<tr>
										<td style="width: 50px;">{{ Form::dText2('order['.$category->id.']','',null,$category->order,['style'=> 'width: 50px;text-align: center;']) }}</td>
									
										<td><a href="{{ route('pagecategory.index', ['page_id'=>$page->id,'pagecategory_id'=>$category->id] ) }}" >{{ $category->title }}</a></td>
										<td style="text-align: center;">
											<a href="{{ route('pagecategory.status', ['id'=>$category->id] ) }}">@if ($category->status==1) <i class="fa fa-eye" style="color:#356635;    font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>
										<td>
											<a href="{{ route('pagecategory.index', ['page_id'=>$page->id,'pagecategory_id'=>$category->id] ) }}" class="btn btn-primary action"><i class="fa fa-list"></i></a>
											<a href="{{ route('pagecontent.index', [null,'type' => $page->page_type_id,'oid'=>$category->id]) }}" class="btn btn-primary action"><i class="fa fa-file"></i></a>
											<a href="{{ route('pagecategory.edit', ['id'=>$category->id, 'page_id' => $page_id]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('pagecategory.destroy', ['id'=>$category->id, 'page_id' => $page_id ]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
										</td>
									</tr>
								@endforeach
								<tr>
									<td>{{ Form::dSubmit('save','Short Save') }}</td>
									<td colspan="4"></td>
								</tr>
							</tbody>
						@endif
					</table>
					{!! Form::close() !!}

			
				</div>
			</div>
		</div>
	</div>	
</div>
@endsection