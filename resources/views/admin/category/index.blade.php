@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('category.create',[Null]) }}" style="float: right;margin: 10px 0px;">Add</a>
					
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
				<h3 class="box-title">Category</h3>
			</div>
			<div class="box-body">
						{!! Form::open(['route' => ['category.order', Null], 'method' => "POST"]) !!}

					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Short NO</th>
                        		<th>Kategori Adı</th>
                        		<th style="width: 10px;">Status</th>
                        		<th>Action</th>
                        	</tr>
                      	</thead>

						@if ($categories)
							<tbody>
								@foreach ($categories as $category)
									<tr>
										<td style="width: 50px;">{{ Form::dText2('order['.$category->id.']','',null,$category->order,['style'=> 'width: 50px;text-align: center;']) }}</td>
									
										<td>{{ $category->title }}</td>
										<td style="text-align: center;">
											<a href="{{ route('category.status', ['id'=>$category->id] ) }}">@if ($category->status==1) <i class="fa fa-eye" style="color:#356635;    font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>
										<td>
											<a href="{{ route('category.edit', ['id'=>$category->id]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('category.destroy', ['id'=>$category->id ]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
										</td>
									</tr>
								@endforeach
								<tr>
									<td>{{ Form::dSubmit('save','Short Save') }}</td>
									<td colspan="4"></td>
								</tr>
							</tbody>
						@endif
					</table>
					{!! Form::close() !!}

			
				</div>
			</div>
		</div>
	</div>	
</div>
@endsection