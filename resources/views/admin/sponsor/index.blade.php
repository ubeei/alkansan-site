@extends('layouts.app')

@section('content')
<div class="content">
<section id="widget-grid" class="">
    
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<a class="btn btn-primary" href="{{ route('sponsor.create',[Null,'page_id'=>$page_id]) }}" style="float: right;margin: 10px 0px;">Add</a>
		</article>
    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Sponsors</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox">
				</div>
				
				<div class="widget-body">
					
					<div class="table-responsive">
						{!! Form::open(['route' => ['sponsor.order', Null,  'page_id' => $page_id], 'method' => "POST"]) !!}
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Short NO</th>
                        		<th>Name</th>
								<th>Url</th>
                        		<th style="width: 10px;">Status</th>							
                        		<th>Action</th>
                        	</tr>
                      	</thead>

						@if ($sponsors)
							<tbody>
								@foreach ($sponsors as $sponsor)
									<tr>
										<td style="width: 50px;">{{ Form::dText2('order['.$sponsor->id.']','',null,$sponsor->order,['style'=> 'width: 50px;text-align: center;']) }}</td>
										<td>{{ $sponsor->name }}</td>
										<td>{{ $sponsor->url }}</td>
										<td style="text-align: center;">
											<a href="{{ route('sponsor.status', ['id'=>$sponsor->id] ) }}">@if ($sponsor->status==1) <i class="fa fa-eye" style="color:#356635;    font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>
										<td>
											<a href="{{ route('sponsor.edit', ['id'=>$sponsor->id, 'page_id' => $page_id]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('sponsor.destroy', ['id'=>$sponsor->id, 'page_id' => $page_id ]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
										</td>
									</tr>
								@endforeach
								<tr>
									<td>{{ Form::dSubmit('save','Short Save') }}</td>
									<td colspan="4"></td>
								</tr>
							</tbody>
						@endif
					</table>
					{!! Form::close() !!}

					</div>
				</div>
			</div>
		</div>
	</article>
	
</div>
@endsection