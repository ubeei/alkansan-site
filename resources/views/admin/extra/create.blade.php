@extends('admin.layouts.app')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
	 	<h3 class="box-title">Ekstra Ekle</h3>
	</div>
	{!! Form::open(['route' => ['extra.store'], 'method' => "POST", "class" =>"", 'files' => true]) !!}
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
		<div class="box-body">
			{{ Form::dText('name','Ekstra hizmet Adı') }}
			{{ Form::dTextnumber('price','Ücret',null,null,array('min'=>'0.00', 'max'=>'10000.00', 'step'=>'0.1')) }}
			{{ Form::dText('extra_time','Ek Zaman') }}
			{{ Form::dText('group','Group ID') }}
		</div>
		<div class="box-footer">
			{{ Form::dSubmit('save','Insert') }}
		</div>
	{!! Form::close() !!}

</div>

@endsection