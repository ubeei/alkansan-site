@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('extra.create') }}" style="float: right;margin: 10px 0px;">Add</a>
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Ekstra</h3>
				</div>
				<div class="box-body">
					

					<table class="table table-bordered">
						<thead>
							<tr>
								
                        		<th>Name</th>
                        		<th>Ücret</th>
								<th style="width: 10px;">Group</th>
								<th>Status</th>
                        		<th>Action</th>
                        	</tr>
                      	</thead>

						@if ($extras)
							<tbody>
								@foreach ($extras as $extra)
									<tr>
										<td>{{ $extra->name }}</td>
										<td>{{ number_format($extra->price,2) }}</td>

										<td style="text-align: center;">
											{{ $extra->group }}
										</td>
										<td>
											<a href="{{ route('extra.status', ['id'=>$extra->id] ) }}">@if ($extra->status==1) <i class="fa fa-eye" style="color:#356635;    font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>
										<td>
											<a href="{{ route('extra.edit', ['id'=>$extra->id]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('extra.destroy', ['id'=>$extra->id ]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						@endif
					</table>

					</div>
				</div>
			</div>
		</div>
	</article>
	
</div>
@endsection