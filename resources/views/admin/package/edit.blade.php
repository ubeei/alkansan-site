@extends('admin.layouts.app')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		 <h3 class="box-title">Package Edit</h3>
	</div>
	{!! Form::model($package, ['route' => ['admin.package.update' ,$package->id ], 'method' => "put", "class" =>"smart-form", 'files' => true]) !!}

		<div class="box-body">
			{{ Form::dText('name','Package Name') }}
			{{ Form::dTextarea('content','Content') }}
			{{ Form::dTextnumber('real_price','Price',null,null,array('min'=>'0.00', 'max'=>'10000.00', 'step'=>'0.1')) }}
			{{ Form::dTextnumber('discount_price','Discount Price',null,null,array('min'=>'0.00', 'max'=>'10000.00', 'step'=>'0.1')) }}
			{{ Form::dTextnumber('total_use','Total use',null,null,array('min'=>'1', 'max'=>'10000', 'step'=>'1')) }}
		</div>
		<div class="box-footer">
			{{ Form::dSubmit('save','Update') }}
		</div>
	{!! Form::close() !!}

</div>

@endsection