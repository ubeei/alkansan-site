@extends('admin.layouts.app')

@section('content')
<div class="content">
		<section id="widget-grid" class="">
			<article class="col-sm-12 col-md-12 col-lg-6">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Team Edit</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
						{!! Form::model($team, ['route' => ['team.update' ,$team->id,  'page_id' => $page_id ], 'method' => "put", "class" =>"smart-form", 'files' => true]) !!}

						<fieldset>
								{{ Form::dFile2('image','Image') }}
								{{ Form::dText('first_name','First Name') }}
								{{ Form::dText('last_name','Last Name') }}
								{{ Form::dText('department','Department') }}
								{{ Form::dText('email','E-mail') }}
								{{ Form::dText('phone','Phone') }}
								{{ Form::dText('linkedin','Linkedin') }}
								{{ Form::dText('googleplus','Google Plus') }}
								{{ Form::dText('skype','Skype') }}
								{{ Form::hidden('page_id', $page_id) }}
						<br/>

						</fieldset>	

							{{ Form::dSubmit('save','Update') }}
						{!! Form::close() !!}

				</div>
			</div>
		</div>
	</article>

	</section>
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
@endsection
@section('js')
<script src="{{ asset('js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/upload/jquery.fileupload.js') }}"></script>
	<script >
		
			$(function () {
				'use strict';
				
				imageupload();
				$('#image_image').on('click',function() {
					$('#upload_image').trigger('click');
				});
				var i = $('#image').val();
				if (i!="") {
					$('#image_image').attr('src',i);
				}
			});
			function imageupload () {
				var url = "{{ route('team.image') }}";
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				$('#upload_image').fileupload({
					url: url,
					dataType: 'json',
					type: 'POST',
					
					headers: {
						'X-CSRF-TOKEN': CSRF_TOKEN,
					},
					done: function (e, data) {
						if (data.result.error==0) {
							var file = data.result;
							//alert(file.id);
							$('#image_image').attr('src',file.url);
							$('#image').val(file.url);
						}else{
							alert(data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('#progress .progress-bar').css(
                			'width',
                			progress + '%'
            			);
											
					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}
			$(function () {
				$('.removeimage').on('click',function() {
					var imgpath = $('#image').val();
					
					$.ajax({
						method: "POST",
						url: "{{ route('team.imageremove') }}",
						data: { img: imgpath },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {
						$('#image_image').attr('src',data.url);
						$('#image').val('');
					});
				});
			});
			
	</script>
@endsection
