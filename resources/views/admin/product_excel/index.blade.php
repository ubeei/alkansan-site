@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('product.create',[Null]) }}" style="float: right;margin: 10px 0px;">Add</a>
					
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
				<h3 class="box-title">Ürünler</h3>
			</div>
			<div class="box-body">
					{!! Form::open(['route' => ['productexcel.store' ,null], 'method' => "POST", "class" =>"smart-form", 'files' => true]) !!}

						<div class="form-group ">
							<label for="meta_desc" class="label">Bağlı Kategori</label>
							<div class="overhidebox">
								<ul class="tree2">
									{{ subcat($categories,old('category_id',array())) }}
								</ul>
							</div>
						</div>
						{{ Form::dFile2('excel','Excel') }}

						{{ Form::dSubmit('save','Insert') }}

					{!! Form::close() !!}

			
				</div>
			</div>
		</div>
	</div>	
</div>
@php
	function subcat($categories,$selectedin=array())
	{
		if ($categories) {
			echo "<ul>";
			foreach ($categories as $category){
				echo '<li><input type="checkbox" class="catlist" name="category_id[]" value="'.$category->id.'"';
				if (in_array($category->id,$selectedin)) {
					echo 'checked="checked"';
				}
				echo ' >'.$category->title.'</li>';
				if ($category->subcategories) {
					subcat($category->subcategories,$selectedin);
				}
			}
			echo "</ul>";
		}
	}
@endphp
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
<style>
	.overhidebox{
		height: 200px;
		overflow: overlay;		
	}
	ul.tree2, ul.tree2 ul {
    list-style: none;
     margin: 0;
     padding: 0;
   } 
   ul.tree2 ul {
     margin-left: 10px;
   }
   ul.tree2 li {
     margin: 0;
     padding: 0 7px;
     line-height: 20px;
     color: #369;
     font-weight: bold;
     border-left:1px solid rgb(100,100,100);

   }
   ul.tree2 li:last-child {
       border-left:none;
   }
   ul.tree2 li:before {
      position:relative;
      top:-0.3em;
      height:1em;
      width:12px;
      color:white;
      border-bottom:1px solid rgb(100,100,100);
      content:"";
      display:inline-block;
      left:-7px;
   }
   ul.tree2 li:last-child:before {
      border-left:1px solid rgb(100,100,100);   
   }
	.input-file {
		position: relative;
		overflow: hidden;
		display: inline-block;
		
		padding: 6px 12px;
		margin-bottom: 0;
		font-size: 14px;
		font-weight: 400;
		line-height: 1.42857143;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		-ms-touch-action: manipulation;
		touch-action: manipulation;
		cursor: pointer;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		background-image: none;
		border: 1px solid transparent;
		border-radius: 4px;
		color: #fff;
		background-color: #5cb85c;
		border-color: #4cae4c;
	}
	.input-file input {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		opacity: 0;
		-ms-filter: 'alpha(opacity=0)';
		font-size: 200px !important;
		direction: ltr;
		cursor: pointer;
	}
</style>
@endsection
@section('js')
<script src="{{ asset('js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/upload/jquery.fileupload.js') }}"></script>
<script>
	$(function () {
		'use strict';
		excelupload();
		$('#image_excel').on('click',function() {
			$('#upload_excel').trigger('click');
		});
		var i = $('#excel').val();
		if (i!="") {
			$('#image_excel').attr('src',i);
		}
	});

	function excelupload () {
		var url = "{{ route('product.excel') }}";
		$('#upload_excel').fileupload({
			url: url,
			dataType: 'json',
			method: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			done: function (e, data) {
				if (data.result.error==0) {
					var file = data.result;
					$('#image_excel').attr('src',file.url);
					$('#excel').val(file.url);
				}else{
					alert(data.result.error);
				}
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
            	$('#progress .progress-bar').css(
            		'width',
                	progress + '%'
            	);
			}
		}).prop('disabled', !$.support.fileInput)
        	.parent().addClass($.support.fileInput ? undefined : 'disabled');
	}
	$(function () {
		$('.removeexcel').on('click',function() {
			var imgpath = $('#excel').val();
			$.ajax({
				method: "POST",
				url: "{{ route('product.excelremove') }}",
				data: { img: imgpath },
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			})
			.done(function( data ) {
				$('#image_excel').attr('src',data.url);
				$('#excel').val('');
			});
		});
	});
</script>
@endsection
