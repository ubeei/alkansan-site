@extends('admin.layouts.app')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
	 	<h3 class="box-title">Kupon Ekle</h3>
	</div>
	{!! Form::open(['route' => ['admin.coupon.store'], 'method' => "POST", "class" =>"", 'files' => true]) !!}
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
		<div class="box-body">
			{{ Form::dText('code','Kupon') }}
			{{ Form::dSelect('coupon_type_id','Kupon Tipi',$coupon_types) }}
			{{ Form::dTextnumber('value','Değer',null,null,array('min'=>'0.00', 'max'=>'10000.00', 'step'=>'0.1')) }}
			{{ Form::dTextnumber('number_of_use','Kullanım Adeti',null,null,array('min'=>'0', 'max'=>'10000', 'step'=>'1')) }}
			{{ Form::dDateTime('startdate','Baş. Tarihi') }}
			{{ Form::dDateTime('finishdate','Bit. Tarihi') }}
		</div>
		<div class="box-footer">
			{{ Form::dSubmit('save','Insert') }}
		</div>
	{!! Form::close() !!}

</div>

@endsection
@push('scripts')

<link href="{{ asset('admin/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

<script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript">
	$(function () {
		$('#startdate').datetimepicker({
			locale: 'tr'
		});
		$('#finishdate').datetimepicker({
			locale: 'tr'
		});
	});
</script>
@endpush