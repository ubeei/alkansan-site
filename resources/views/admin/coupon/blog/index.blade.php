@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('blog.create') }}" style="float: right;margin: 10px 0px;">Add</a>
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Blog</h3>
				</div>
				<div class="box-body">
		<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width: 50px;">Short NO</th>
								<th>Title</th>
                        		<th style="width: 10px;">Status</th>								

														<th style="width:20px;">Action</th>
                        	</tr>
                      	</thead>

						@if ($blogs)
							<tbody>
								@foreach ($blogs as $blog)
									<tr>
											<td>{{$blog->id}}</td>

										<td>{{$blog->title}}</td>
										<td style="text-align: center;">
											<a href="{{ route('blog.status', ['id'=>$blog->id]) }}">@if ($blog->status==1) <i class="fa fa-eye" style="color:#356635; font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>

										<td>
											<a href="{{ route('blog.edit', ['id'=>$blog->id]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('blog.destroy', ['id'=>$blog->id]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?" class="btn btn-danger action">Delete</a>
										</td>
									</tr>
								@endforeach

							</tbody>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
