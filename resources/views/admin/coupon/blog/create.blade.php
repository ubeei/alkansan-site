@extends('admin.layouts.app')

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">Blog Add</h3>
		</div>
		{!! Form::open(['url' => 'admin/blog', 'method' => "POST", "class" =>"smart-form", 'files' => true]) !!}

			<div class="box-body">
				{{ Form::dText('title','Title') }}
				{{ Form::dText('text','Content') }}
				<div class="image_uploader">
				<label>Image(s)</label>
				<img class="blogImg" src="https://dummyimage.com/150x150/b3b1b3/000000.png&text=Image"/>
				<div class="progress">
						<div class="progress-bar progress-bar-success"></div>
					</div>
				<input class="image_path" type="text"/>
				<input type="hidden" name="count" value="1"/>
				<input type="file" name="image[]" class="blogImgInput"/>
			</div>


			</div>
			<div class="box-footer">
				{{ Form::dSubmit('save','Save') }}
			</div>
		{!! Form::close() !!}
	</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('admin/css/upload/jquery.fileupload.css') }}">
@endsection
@section('js')

<script type="text/javascript">
	$( document ).ready(function() {
		   console.log( "ready!" );
			 var image_count = 0;

	   });
</script>

<script src="{{ asset('admin/js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('admin/js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('admin/js/upload/jquery.fileupload.js') }}"></script>
	<script >

			$(function () {
				'use strict';

				imageupload();
				$('.blogImg').on('click',function() {
					$('.blogImgInput').trigger('click');
				});
				$(".image_path").each(function(index){
					if($( this ).val())
					$(".blogImg").eq(index).attr('src', $( this ).val());
				});
				// var i = $('#image').val();
				// if (i!="") {
				// 	$('#image_image').attr('src',i);
				// }
			});
			function imageupload () {
				var url = "{{ route('blog.image') }}";
				console.log("uploading..");
				$('.blogImgInput').fileupload({
					url: url,
					dataType: 'json',
					method: "POST",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					done: function (e, data) {
						image_count++;
						console.log('image uploaded');
						$(".image_uploader").eq(image_count-1).after(`<div class="image_uploader">
						<label>Image(s)</label>
						<img class="blogImg" src="https://dummyimage.com/150x150/b3b1b3/000000.png&text=Image"/>
						<div class="progress">
								<div class="progress-bar progress-bar-success"></div>
							</div>
						<input class="image_path" type="text"/>
						<input type="hidden" name="count" value="`+image_count+`"/>
						<input type="file" name="image[]" class="blogImgInput"/>
					</div>`);

						if (data.result.error==0) {
							var file = data.result;

							$('.blogImg').eq(image_count-1).attr('src',file.url);
							$('.blogImgInput').eq(image_count-1).val(file.url);

						}else if(data.result.errors!=null){
							alert('1 => '+data.result.message);
						}else{
							alert('2 => '+data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('.progress > .progress-bar').css(
                			'width',
                			progress + '%'
            			);

					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}

			// $(function () {
			// 	$('.removeimage').on('click',function() {
			// 		var imgpath = $('#image').val();
			//
			// 		$.ajax({
			// 			method: "POST",
			// 			url: "{{ route('sponsor.imageremove') }}",
			// 			data: { img: imgpath },
			// 			headers: {
			// 				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			// 			}
			// 		})
			// 		.done(function( data ) {
			// 			$('#image_image').attr('src',data.url);
			// 			$('#image').val('');
			// 		});
			// 	});
			// });

	</script>
@endsection
