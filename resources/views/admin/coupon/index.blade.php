@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('admin.coupon.create') }}" style="float: right;margin: 10px 0px;">Add</a>
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Kupon</h3>
				</div>
				<div class="box-body">
					

					<table class="table table-bordered">
						<thead>
							<tr>
								
                        		<th>Kupon</th>
                        		<th>Baş. Tarihi</th>
                        		<th>Bit. Tarihi</th>
								<th>Status</th>
                        		<th>Action</th>
                        	</tr>
                      	</thead>

						@if ($coupons)
							<tbody>
								@foreach ($coupons as $coupon)
									<tr>
										<td>{{ $coupon->code }}</td>
										<td>
											{{ $coupon->startdate }}
										</td>
										<td>
											{{ $coupon->finishdate }}
										</td>
										<td>
											<a href="{{ route('admin.coupon.status', ['id'=>$coupon->id] ) }}">@if ($coupon->status==1) <i class="fa fa-eye" style="color:#356635;    font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>
										<td>
											<a href="{{ route('admin.coupon.edit', ['id'=>$coupon->id]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('admin.coupon.destroy', ['id'=>$coupon->id ]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						@endif
					</table>

					</div>
				</div>
			</div>
		</div>
	</article>
	
</div>
@endsection