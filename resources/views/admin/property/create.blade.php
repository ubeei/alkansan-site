@extends('admin.layouts.app')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
	  <h3 class="box-title">Özellik Ekle</h3>
	</div>
		{!! Form::open(['route' => ['property.store'], 'method' => "POST", "class" =>"smart-form", 'files' => true]) !!}

		<fieldset>
			{{ Form::dText('name','Başlık') }}
			<div class="form-group ">
				<label for="meta_desc" class="label">Bağlı Kategori</label>
				<ul class="tree2">
					{{ subcat($categories) }}
				</ul>
			</div>
		<br/>

		</fieldset>	
		{{ Form::dSubmit('save','Insert') }}
		<br/>
	{!! Form::close() !!}


</div>
@php
	function subcat($categories)
	{
		if ($categories) {
			echo "<ul>";
			foreach ($categories as $category){
				echo '<li><input type="checkbox" name="category_id[]" value="'.$category->id.'">'.$category->title.'</li>';
				if ($category->subcategories) {
					subcat($category->subcategories);
				}
			}
			echo "</ul>";
		}
	}
@endphp
@endsection
@section('css')
	<style>
	ul.tree2, ul.tree2 ul {
    list-style: none;
     margin: 0;
     padding: 0;
   } 
   ul.tree2 ul {
     margin-left: 10px;
   }
   ul.tree2 li {
     margin: 0;
     padding: 0 7px;
     line-height: 20px;
     color: #369;
     font-weight: bold;
     border-left:1px solid rgb(100,100,100);

   }
   ul.tree2 li:last-child {
       border-left:none;
   }
   ul.tree2 li:before {
      position:relative;
      top:-0.3em;
      height:1em;
      width:12px;
      color:white;
      border-bottom:1px solid rgb(100,100,100);
      content:"";
      display:inline-block;
      left:-7px;
   }
   ul.tree2 li:last-child:before {
      border-left:1px solid rgb(100,100,100);   
   }
	</style>
@endsection