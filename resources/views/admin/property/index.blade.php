@extends('admin.layouts.app')

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<a class="btn btn-primary" href="{{ route('property.create') }}" style="float: right;margin: 10px 0px;">Add</a>
					
				</div>
			</div>
			<div class="box">
				<div class="box-header with-border">
				<h3 class="box-title">Özellikler</h3>
			</div>
			<div class="box-body">
						{!! Form::open(['route' => ['property.order'], 'method' => "POST"]) !!}

					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Short NO</th>
                        		<th>Özellik Adı</th>
                        		<th style="width: 10px;">Status</th>
                        		<th>Action</th>
                        	</tr>
                      	</thead>

						@if ($properties)
							<tbody>
								@foreach ($properties as $property)
									<tr>
										<td style="width: 50px;">{{ Form::dText2('order['.$property->id.']','',null,$property->order,['style'=> 'width: 50px;text-align: center;']) }}</td>
										
										<td>{{ $property->name }}</td>
										<td style="text-align: center;">
											<a href="{{ route('property.status', ['id'=>$property->id] ) }}">@if ($property->status==1) <i class="fa fa-eye" style="color:#356635;    font-size: 19px;"></i> @else <i class="fa fa-eye-slash" style="color:red;font-size: 19px;"></i> @endif</a>
										</td>
										<td>
											<a href="{{ route('property.edit', ['id'=>$property->id]) }}" class="btn btn-primary action">Edit</a>
											<a href="{{ route('property.destroy', ['id'=>$property->id ]) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure it will be deleted?" class="btn btn-danger action">Delete</a>
										</td>
									</tr>
								@endforeach
								<tr>
									<td>{{ Form::dSubmit('save','Short Save') }}</td>
									<td colspan="4"></td>
								</tr>
							</tbody>
						@endif
					</table>
					{!! Form::close() !!}

			
				</div>
			</div>
		</div>
	</div>	
</div>
@endsection