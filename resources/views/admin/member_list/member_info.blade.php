@extends('layouts.app')

@section('content')

<div class="content">
    <section id="widget-grid" class="">
            <article class="col-sm-12 col-md-12 col-lg-6">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
                    <h2> Member Information </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body no-padding">
                        {{ Form::open(['url' => 'admin/member-info-update', 'method' => "POST", "class" =>"smart-form", 'files' => true]) }}
                            <fieldset>
                                <input type="hidden" name="member_id" value="{{ $user->id }}">
                                {{ Form::dText('name', 'Ad Soyad :', null, $user->name) }}
                                {{ Form::dText('email', 'E-Mail :', null, $user->email) }}
                                {{ Form::dText('password', 'Şifre :') }}
                                @if ($user_info)
                                    
                                
                                {{ Form::dText('mobile', 'Telefon :', null, $user_info->mobile) }}
                                    @if ($user->roles[0]->name=="agency")
                                        {{ Form::dText('mobile_office', 'Ofis Telefonu :', null, $user_info->office_phone) }}
                                        {{ Form::dText('site', 'Web Sitesi :', null, $user_info->site) }}
                                        {{ Form::dText('job_title', 'Pozisyon :', null, $user_info->job_title) }}
                                        {{ Form::dText('agency_name', 'Acenta Adı :', null, $user_info->agency_name) }}
                                        {{ Form::dText('trade_association', 'Ticaret Birliği :', null, $user_info->trade_association) }}
                                        {{ Form::dText('trade_association_no', 'Ticaret Birliği Üye No :', null, $user_info->trade_association_no) }}
                                        {{ Form::dText('discount_rate', 'İndirim Oranı :', null, $user_info->discount_rate  ) }}
                                    @endif
                                    {{ Form::dText('postal_code', 'Posta Kodu :', null, $user_info->postal_code) }}
                                    {{ Form::dText('address', 'Adres :', null, $user_info->address) }}
                                    <?php $country_s = App\Models\Country::find($user_info->country_id) ?>
                                    {{ Form::dSelect('country_id', 'Ülke :', $countries, null, $user_info->country_id) }}
                                    <?php $states_s = App\Models\State::where('country_id', $user_info->country_id)->pluck('name', 'id'); ?>
                                    {{ Form::dSelect('state_id', 'Bölge :', $states_s, null, $user_info->state_id) }}
                                    <?php $city_s = App\Models\City::where('state_id', $user_info->state_id)->pluck('name', 'id'); ?>
                                    {{ Form::dSelect('city_id', 'Şehir :', $city_s, null, $user_info->city_id) }}
                                @endif
                                
                            </fieldset> 
                            {{ Form::dSubmit('update','Güncelle') }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <br><br><br>
        </article>
    </section>
</div>

@endsection

@section('js')
    <script src="{{ asset('js/intl-tel-input/build/js/intlTelInput-jquery.js') }}"></script>
    <script>

    $( document ).ready(function() {
        $('#country_id').on('change',function() {
            var country_id = $('#country_id').val();
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.state') }}",
                data:'country_id='+country_id,
                success: function(data){
                    $('#state_id').empty();
                    $('#state_id').append($('<option>').text('{{__('form.register_state_select')}}').attr('value', ""));
                    $.each(data, function(i, value) {
                        $('#state_id').append($('<option>').text(value).attr('value', i));
                    });
                }
            });
        });
        $('#state_id').on('change',function() {
            var state_id = $('#state_id').val();
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.city') }}",
                data:'state_id='+state_id,
                success: function(data){
                    $('#city_id').empty();
                    $('#city_id').append($('<option>').text('{{__('form.register_city_select')}}').attr('value', ''));
                    $.each(data, function(i, value) {
                        $('#city_id').append($('<option>').text(value).attr('value', i));
                    });
                }
            });
        });
    });
    </script>
@endsection