@extends('layouts.app')

@section('content')

<div class="content">
    <section id="widget-grid" class="">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ul"></i> </span>
                    <h2> Member List </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <div class="row">
                            <div class="col-md-12">
                                <select name="role_check_name" id="role_check_id">
                                    <option value="all"> All </option>
                                    <option value="admin"> Admin </option>
                                    <option value="agency"> Agency </option>
                                    <option value="user"> User </option>
                                </select>
                                <table class="table table-bordered" id="myTable">
                                    <thead>
                                        <tr>
                                            <th> Üye Adı </th>
                                            <th> E-Mail </th>
                                            <th> Rol </th>
                                            <th> Status </th>
                                            <th> Aktivasyon İşlemi </th>
                                            <th> İşlem </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <td><a href="{{ url('admin/member-info', ['id'=>$user->id]) }}"> {{ $user->name }} </a></td>
                                                <td> {{ $user->email }} </td>
                                                <td> @foreach ($user->roles as $key => $value)
                                                        {{$value->name}} ,
                                                @endforeach
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="status" class="durum" data-id="{{$user->id}}", data-url="status_change_user" {{$user->status ? "checked" : null}}>
                                                </td>
                                                <td>
                                                    @if ($user->activation_code)
                                                        <label for="" class="no_active_label"> Yapılmamış </label>
                                                    @else
                                                        <label for="" class="active_label"> Yapılmış </label>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('admin/member-info', ['id'=>$user->id]) }}" class="btn btn-primary action">Edit</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th> Üye Adı </th>
                                            <th> E-Mail </th>
                                            <th> Rol </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
</div>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap-switch.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
@endsection

@section('js')
    <script src="{{ asset('admin/js/bootstrap-switch.min.js') }}"></script>
    <script>        
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            var durum = parseInt($("body").data("status"));
            $('[data-toggle="tooltip"]').tooltip();
            $(".durum").bootstrapSwitch();
            $(".durum").on('switchChange.bootstrapSwitch', function(event, state) {
                $.ajax({
                    data: {"durum": state,"id":$(this).data("id") },
                    type: "POST",
                    url: $(this).data("url"),
                    success: function(url) {
                        //alert('Success');
                    }
                });
            });
        })
    </script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'pageLength', 'copy', 'csv', 'excel', 'pdf', 'print'
                ],





                initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }








            });
        });
    </script>
@endsection