@extends('admin.layouts.app')

@section('content')
<div class="content">
		<section id="widget-grid" class="">
			<article class="col-sm-12 col-md-12 col-lg-6">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Blog Edit</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
					<div class="widget-body no-padding">
<form method="PUT" action="{{route('blog.update',['id' => $id])}}" >
	@CSRF
									<div class="box-body">
						        <label> Id: {{$id}}</label>
						        <input type="hidden" value="{{$id}}" id="blogId" name="blog_id"/>
<br><br>

										<label>Title</label><br>
										<input style="width:60%;" name="title" type="text" value="{{$blog->title}}"><br>
										<br><br>
										<label>Content</label><br>
										<textarea style="width:60%;" name="content" >{{$blog->content}}</textarea>

						        <div>
											@foreach($blogImages as $key => $image)
										<div class="image_uploader">
										<label>Image(s)</label><br>
										<img class="blogImg" src="{{$image->path_thumbnail}}"/>
						        <button type="button" class="imgRemove">Remove Image</button>
						        <input type="hidden" value="$image->id" class="imgId">


										<input style="display:none;" type="file" name="image[]" class="blogImgInput"/>
						        <input style="display:none;" class="image_path" type="text"/>
						        <input type="hidden" name="image_count" value="{{$key}}"/>
						        <!-- <div class="progress">
						            <div class="progress-bar progress-bar-success"></div>
						          </div> -->

									</div>
									@endforeach
									<div class="image_uploader">
									<label>Image(s)</label><br>
									<img class="blogImg" src="https://dummyimage.com/150x150/b3b1b3/000000.png&text=Image"/>
									<button type="button" class="imgRemove">Remove Image</button>
									<input type="hidden" value="" class="imgId">


									<input style="display:none;" type="file" name="image[]" class="blogImgInput"/>
									<input style="display:none;" class="image_path" type="text"/>
									<input type="hidden" name="image_count" value="0"/>
									<!-- <div class="progress">
											<div class="progress-bar progress-bar-success"></div>
										</div> -->

								</div>

						    </div>


									</div>
									<div class="box-footer">
										<button type="submit" class="button submit">Save</button>

									</div>

							</form>

				</div>
			</div>
		</div>
	</article>

	</section>
</div>

@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('admin/css/upload/jquery.fileupload.css') }}">

<style>
.image_uploader{width:32%;display:inline-block;text-align: center;}
.blogImg{max-width: 150px;}

</style>
@endsection
@section('js')

<script type="text/javascript">
	$( document ).ready(function() {
		   console.log( "ready!" );
			 var image_count = $("input[name='image_count']").val();
       console.log(image_count);

	   });
</script>

<script src="{{ asset('admin/js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('admin/js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('admin/js/upload/jquery.fileupload.js') }}"></script>
	<script >

			$(function () {
				'use strict';

				imageupload();
				$('.blogImg').each(function(){

          $( this ).on('click',function() {

  					$( this ).next().next().next().trigger('click');
  				});
        });
				$(".image_path").each(function(index){
					if($( this ).val())
					$(".blogImg").eq(index).attr('src', $( this ).val());
				});
				// var i = $('#image').val();
				// if (i!="") {
				// 	$('#image_image').attr('src',i);
				// }
			});
			function imageupload () {
				var url = "{{ route('blog.image') }}";
				console.log("uploading..");
				$('.blogImgInput').fileupload({
          formData: {blog_id: $("#blogId").val()},
					url: url,
					dataType: 'json',
          // data: + "blog_id" + $("#blogId").val(),
					method: "POST",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					done: function (e, data) {





var image_count = $("input[name='image_count']").val();

						if (data.result.error==0) {
							var file = data.result;



$('.blogImg').eq(image_count).attr('src',file.url);
$('.imgId').eq(image_count).val(file.id);

						$(".image_uploader").last().after(`<div class="image_uploader">
    				<img class="blogImg" src="https://dummyimage.com/150x150/b3b1b3/000000.png&text=Image"/>
            <button type="button" class="imgRemove">Remove Image</button>
            <input type="hidden" value="" class="imgId">


    				<input style="display:none;" type="file" name="image[]" class="blogImgInput"/>
            <input style="display:none;" class="image_path" type="text"/>
            <input type="hidden" name="image_count" value="0"/>
					</div>`);
          image_count++;
          $("input[name='image_count']").val(image_count);

          $('.blogImg').each(function(){

            $( this ).on('click',function() {

              $( this ).next().next().next().trigger('click');
            });
          });

          imageupload();
							// $('.blogImgInput').eq(image_count-1).val(file.url);

						}else if(data.result.errors!=null){
							alert('1 => '+data.result.message);
						}else{
							alert('2 => '+data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('.progress > .progress-bar').css(
                			'width',
                			progress + '%'
            			);

					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}

			$(function () {
				$('.imgRemove').on('click',function() {
          console.log("remove");

					var imgId = $( this ).next().val();

					$.ajax({
						method: "POST",
						url: "{{ route('blog.image.remove') }}",
						data: { id: imgId },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {

var image_count = $("input[name='image_count']").val();
image_count--;
$("input[name='image_count']").val(image_count);
$(".image_uploader").last().remove();
						// $('.blogImg').attr('src',data.url);
						// $('.imgId').val('');
					});
          $( this ).prev().attr('src', "https://dummyimage.com/150x150/b3b1b3/000000.png&text=Image");
          $( this ).next().val("");
				});
			});

	</script>
@endsection
