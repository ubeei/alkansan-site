@extends('admin.layouts.app')

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">Slider Add</h3>
		</div>
		{!! Form::open(['url' => 'admin/slider', 'method' => "POST", "class" =>"smart-form", 'files' => true]) !!}

			<div class="box-body">
				{{ Form::dText('title_desc','Title Desc.') }}
				{{ Form::dText('title','Title') }}
				{{ Form::dText('text','Text') }}
				{{ Form::dFile2('image','Image') }}

				<div class="col-md-6">
					{{ Form::dText('button_name[]','Button Name',Null) }}
				</div>
				<div class="col-md-6">
					{{ Form::dText('button_url[]','Button Url',Null) }}
				</div>
			</div>
			<div class="box-footer">
				{{ Form::dSubmit('save','Insert') }}
			</div>
		{!! Form::close() !!}
	</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('admin/css/upload/jquery.fileupload.css') }}">
@endsection
@section('js')

<script type="text/javascript">
	$( document ).ready(function() {
		   console.log( "ready!" );
	   });
</script>

<script src="{{ asset('admin/js/upload/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('admin/js/upload/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('admin/js/upload/jquery.fileupload.js') }}"></script>
	<script >

			$(function () {
				'use strict';

				imageupload();
				$('#image_image').on('click',function() {
					$('#upload_image').trigger('click');
				});
				var i = $('#image').val();
				if (i!="") {
					$('#image_image').attr('src',i);
				}
			});
			function imageupload () {
				var url = "{{ route('slider.image') }}";
				$('#upload_image').fileupload({
					url: url,
					dataType: 'json',
					method: "POST",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					done: function (e, data) {
						if (data.result.error==0) {
							var file = data.result;

							$('#image_image').attr('src',file.url);
							$('#image').val(file.url);

						}else if(data.result.errors!=null){
							alert('1 => '+data.result.message);
						}else{
							alert('2 => '+data.result.error);
						}
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
            			$('#progress .progress-bar').css(
                			'width',
                			progress + '%'
            			);

					}
				}).prop('disabled', !$.support.fileInput)
        			.parent().addClass($.support.fileInput ? undefined : 'disabled');
			}
			$(function () {
				$('.removeimage').on('click',function() {
					var imgpath = $('#image').val();

					$.ajax({
						method: "POST",
						url: "{{ route('slider.imageremove') }}",
						data: { img: imgpath },
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					})
					.done(function( data ) {
						console.log("image removed");
						$('#image_image').attr('src',data.url);
						$('#image').val('');
					});
				});
			});

	</script>
@endsection
