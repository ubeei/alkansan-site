@extends('admin.layouts.app')
@section('css')
    <link href="{{ asset('css/adminbsb-materialdesign/plugins/morrisjs/morris.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Randevular</h3>
				</div>
				<div class="box-body">
					
					<div class="row">
						<div class="col-md-12 text-right">
							<input type="text" id="capaingdate" class="form-control form-control-sm" value="{{ $startdate->format('m/d/Y') }} - {{ $finishdate->format('m/d/Y') }}" style="width: 200px;display: inline-block;margin-bottom:15px;">
						</div>
					</div>
					<table class="table table-bordered">
						<thead>
							<tr>
								
                        		<th>Code</th>
                        		<th>Tarih Saat</th>
                        		<th>Ad Soyad</th>
                        		<th>T.C.</th>
                        		<th>E-mail</th>
								<th>Telefon</th>
								<th>Uygulama</th>
								
								<th>İşlemler</th>
                        	</tr>
                      	</thead>

						@if ($appointments)
							<tbody>
								@foreach ($appointments as $appointment)
									<tr>
										<td>{{ $appointment->code }}</td>
										<td>{{ $appointment->appointment_time->format('d-m-Y H:i') }}</td>
										<td>{{ $appointment->user->name }}</td>
										<td>{{ $appointment->user->identity_number }}</td>
										<td>{{ $appointment->user->email }}</td>
										<td>{{ $appointment->user->phone }}</td>
										<td>@foreach (json_decode($appointment->items) as $key => $item)
											@if ($key>0)
												,
											@endif
											{{$item->name}}
										@endforeach</td>
										
										<td>
											@if ($appointment->check)
												<a href="{{ route('admin.appointment.check', ['id'=>$appointment->id,'check'=>$appointment->check]) }}" class="btn btn-success action">
													Geldi
												</a>
											@else
												<a href="{{ route('admin.appointment.check', ['id'=>$appointment->id,'check'=>$appointment->check]) }}" class="btn btn-danger action">
													Gelmedi
												</a>
											@endif
											
										</td>
									</tr>
								@endforeach
							</tbody>
						@endif
					</table>

					</div>
				</div>
			</div>
		</div>
	</article>
	
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
        $(function() {
            $('#capaingdate').daterangepicker({
                opens: 'left'
            }, function(start, end, label) {
                window.location.href = "{{ route('admin.appointment.index') }}?startdate="+start.format('YYYY-MM-DD')+"&finishdate="+end.format('YYYY-MM-DD');
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
    
	</script>
@endsection
@push('scripts')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
        $(function() {
            $('#capaingdate').daterangepicker({
                opens: 'left'
            }, function(start, end, label) {
                window.location.href = "{{ route('admin.appointment.index') }}?startdate="+start.format('YYYY-MM-DD')+"&finishdate="+end.format('YYYY-MM-DD');
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
    
	</script>
@endpush
	