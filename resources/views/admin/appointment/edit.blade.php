@extends('admin.layouts.app')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		 <h3 class="box-title">Paket Güncelle</h3>
	</div>
	{!! Form::model($extra, ['route' => ['extra.update' ,$extra->id ], 'method' => "put", "class" =>"smart-form", 'files' => true]) !!}

		<div class="box-body">
			{{ Form::dText('name','Ekstra hizmet Adı') }}
			{{ Form::dTextnumber('price','Ücret',null,null,array('min'=>'0.00', 'max'=>'10000.00', 'step'=>'0.1')) }}
			{{ Form::dText('extra_time','Ek Zaman') }}
			{{ Form::dText('group','Group ID') }}
		</div>
		<div class="box-footer">
			{{ Form::dSubmit('save','Update') }}
		</div>
	{!! Form::close() !!}

</div>

@endsection