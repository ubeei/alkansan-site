@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu2'))
    <div class="container">
        <div class="row justify-content-center">
                <div class="col-md-12">
                        <h1 class="slidertitle">{{ trans('site.profil_duzenle') }}</h1>
                        <div class="row">  
                            <div class="col-md-12">
                            {!! Form::open(['url' => 'user-update','class' => 'appointmnent']) !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    {!! Form::dText2("name", null , Lang::get('site.profil_update_name'), $user->name) !!}
                                    {!! Form::dText2("email",null , Lang::get('site.profil_update_email'), $user->email) !!}
                                    {!! Form::dPassword2("password",null , Lang::get('site.profil_update_password')) !!}
                                    {!! Form::dPassword2("password_confirmation", null , Lang::get('site.profil_update_password_confirmation')) !!}
                                    {!! Form::dText2("phone",null , Lang::get('site.profil_update_mobile'), $user->phone) !!}
                                </div>
                                <div class="col-sm-6">
                                        {{ Form::dText2('identity_number',null , trans('form.register_identity_number'), $user->identity_number) }}
                                    {!! Form::dSelect2("country_id", null , $countries, Lang::get('site.profil_update_country'), $user->country_id) !!}
                                        <?php 
                                            $states = App\Models\State::where('country_id', $user->country_id)->pluck('name', 'id');
                                        ?>
                                    {!! Form::dSelect2("state_id", null ,$states, Lang::get('site.profil_update_state'), $user->state_id) !!}
                                    {!! Form::dText2("address", null , Lang::get('site.profil_update_address'), $user->address) !!}
                                    {!! Form::dText2("postal_code", null , Lang::get('site.profil_update_postal_code'), $user->postal_code) !!}
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary" onclick="$('#office_phone').val($('#office_phone').intlTelInput('getNumber'));$('#mobile').val($('#mobile').intlTelInput('getNumber'));">
                                        {{ Lang::get('site.profil_update_button') }}
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
        </div>
            
    </div>
</div>
    
@endsection

@section('js')

    <script>
       
        


        $( document ).ready(function() {
            $('#country_id').on('change',function() {
                var country_id = $('#country_id').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.state') }}",
                    data:'country_id='+country_id,
                    success: function(data){
                        $('#state_id').empty();
                        $('#state_id').append($('<option>').text('{{__('form.register_state_select')}}').attr('value', ""));
                        $.each(data, function(i, value) {
                            $('#state_id').append($('<option>').text(value).attr('value', i));
                        });
                    }
                });
            });
           
        });
        
  
        $(document).ready(function(){
            $('#phone').inputmask("0(999) 999 99 99");  //static mask
            $('#identity_number').inputmask("99999999999");  //static mask
        });
    </script>
@endsection