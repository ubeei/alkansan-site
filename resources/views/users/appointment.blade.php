@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu2'))
    <div class="container">
        <div class="row justify-content-center">
                <div class="col-md-12">
                    <h1 class="slidertitle">{{ trans('site.appointmenttitle') }}</h1>
                    <table  class="table">
                        <tr>
                            <th>Randevu Kodu</th>
                            <th>Tarih Saat</th>
                            <th>İşlemler</th>
                        </tr>
                        @foreach ($appointments as $appointment)
                            <tr>
                                <td>{{ $appointment->code }}</td>
                                <td>{{ $appointment->appointment_time->format('d/m/Y H:i') }}</td>
                                
                                <td>
                                    @foreach (json_decode($appointment->items) as $key => $item)
                                    @if ($key>0)
                                        ,
                                    @endif
                                    {{$item->name}}
                                @endforeach</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
        </div>
            
    </div>
</div>
    
@endsection

