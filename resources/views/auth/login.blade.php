@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu2'))
        <div class="container">
            <div class="row">
               
                <div class="col-md-6 offset-md-3">
                <h1 class="slidertitle">{{ trans('form.login') }}</h1>

                    <form method="POST" action="{{ route('login') }}" class="appointmnent">
                <div class="row">

                        @csrf
                        <div class="col-md-12">
                            {{ Form::dText2('email',null , trans('form.register_email') ) }}
                        </div>
                        

                        <div class="col-md-12">
                            <div class="input ">
                                <input id="password" placeholder="{{ trans('form.register_password') }}" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary" style="width: 100%;">
                                        {{ trans('form.login') }}
                                    </button>
                                </div>
                                <div class="col-md-6 text-right">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link forgot_password" href="{{ route('password.request') }}">
                                            {{ trans('form.forgot_password') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a class="btn btn-primary" style="width: 100%;" href="{{ route('register') }}">@lang('form.register_btn_register')</a>
                        </div>
                </div>

                    </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
