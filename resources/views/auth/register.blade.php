@extends($extend_uzanti)

@section('content')
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<div id="body-wrap">
    @include(template_path_check('/layouts/topmenu2'))
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-12">
                    <h1 class="slidertitle">{{ trans('form.register_btn_register') }}</h1>
                    <div class="row">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <form method="POST" action="{{ route('register') }}" class="appointmnent">
                        <div class="row">
                        <div class="col-md-6">    
                            @csrf
                        
                            <div class="row">
                                <div class="col-md-12">
                                    {{ Form::dText2('name',null , trans('form.register_name') ) }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dText2('email',null , trans('form.register_email')) }}
                                </div>
                                <div class="col-md-12">
                                    <div class="input ">
                                        <input id="password" placeholder="{{ trans('form.register_password') }}" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input ">
                                        <input id="password-confirm" placeholder="{{ trans('form.register_confirm_password') }}" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation" required>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dText2('phone',null , trans('form.register_phone')) }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dText2('identity_number',null , trans('form.register_identity_number')) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                
                                <div class="col-md-12">
                                    {{ Form::dSelect2('country_id', null, $countries ,trans('form.register_country_select'),223) }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dSelect2('state_id', null, array() ,trans('form.register_state_select')) }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dTextarea2('address',null , trans('form.register_address')) }}
                                </div>
                                <div class="col-md-12">
                                    {{ Form::dText2('zipcode',null , trans('form.register_postal_code')) }}
                                </div>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('form.register_btn_register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
    </div>
</div>
</div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $('#phone').inputmask("0(999) 999 99 99");  //static mask
            $('#identity_number').inputmask("99999999999");  //static mask
        });
        $( document ).ready(function() {
            $('#country_id').on('change',function() {
                var country_id = $('#country_id').val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.state') }}",
                    data:'country_id='+country_id,
                    success: function(data){
                        $('#state_id').empty();
                        $('#state_id').append($('<option>').text('{{__('form.register_state_select')}}').attr('value', ""));
                        $.each(data, function(i, value) {
                            $('#state_id').append($('<option>').text(value).attr('value', i));
                        });
                    }
                });
            });
            $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.state') }}",
                    data:'country_id='+223,
                    success: function(data){
                        $('#state_id').empty();
                        $('#state_id').append($('<option>').text('{{__('form.register_state_select')}}').attr('value', ""));
                        $.each(data, function(i, value) {
                            $('#state_id').append($('<option>').text(value).attr('value', i));
                        });
                    }
                });
        });
    </script>
@endpush