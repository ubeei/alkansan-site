<?php

return [
    'account_logout'    => 'Çıkış Yap',

    'about_head'  => 'Hakkımızda',
    'about_first_par' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    'about_second_par' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',

    'home'  => 'Ana Sayfa',
    'blog_continue'  => 'Devamını oku',
    'blog_other'  => 'Diğer Blog Yazılar',

    'categories' => 'Kategoriler',

    'contact_head'  => 'İletişim',
    'contact_sub_head'  => 'Sormak istediğiniz bir şey var mı? Lütfen bizimle iletişime geçmekten çekinmeyin. Ekibimiz size geri dönecek.',
    'contact_form_name'  => 'Ad Soyad',
    'contact_form_email'  => 'Email',
        'contact_form_subject'  => 'Konu',
    'contact_form_message'  => 'Mesajınız',
    'contact_form_submit'  => 'Gönder',
    'contact_address'  => 'Perpa Ticaret Merkezi A Blok Kat:1 No:9/0044 Okmeydanı Şişli/Istanbul',


    'product_code' =>  'Ürün Kodu',
    'product_prodtecs' =>  'Ürün Teknik Bilgileri (PDF)',
    'product_cattecs' =>  'Ürün Teknik Bilgileri (PDF)',
    'product_company' =>  'Marka',
    'product_power' =>  'Ampül Gücü',
    'product_volt' =>  'Voltaj (V)',
    'product_hour' =>  'Ömür (Saat)',
    'product_socket' =>  'Duy',
    'product_complen' =>  'Toplam Uzunlug',
    'product_lenght' =>  'Yanma Uzunlugu',
    'product_ansicode' =>  'Ansi Kod',
    'product_fiber_code' =>  'Lif Kodu',
    'product_light_color' =>  'İşik Rengi',
    'product_light_stream' =>  'İşık Akısı (Lumen)',
    'product_reflector' =>  'Reflectör',
    'product_use_case' =>  'Kullanım Alanları',
      'product_detail' => 'Ürün Detay',


    'footer_follow' => 'Takip Et',
    'footer_instagram'  => 'Instagram',
    'footer_contact'    => 'İletişim Bilgileri',

    'contact_namesurname'   => 'Ad Soyad',
    'contact_phone'         => 'Telefon',
    'contact_email'         => 'E-mail',
    'contact_massage'       => 'Mesaj',
    'g_recaptcha_respons'   => 'Doğrulama Alanı',
    'contact_sendmassage'   => 'Gönder',

    'form_success'      => 'Mesajınız başarılı şekilde gönderilmiştir. En kısa sürede sizinle iletişime geçilecektir.',
    'form_error'        => 'Mesajınız gönderilirken hata oluştu. Lütfen daha sonra tekrar deneyiniz',

    'user_title'        => 'Üye İşlemleri',
    'user_description'  => 'Üyelik Düzenleme',
    'user_tags'         => 'Üyelik Düzenleme',
    'profil_duzenle'    => 'Profil Düzenleme',

    'profil_update_name'                  => 'İsim',
    'profil_update_fullname'              => 'İsim',
    'profil_update_email'                 => 'E-Mail Adresi',
    'profil_update_password'              => 'Şifre',
    'profil_update_password_confirmation' => 'Şifreyi Yenile',
    'profil_update_mobile'                => 'Mobil No',
    'profil_update_country'               => 'Ülke',
    'profil_update_state'                 => 'Bölge',
    'profil_update_city'                  => 'Şehir',
    'profil_update_postal_code'           => 'Posta Kodu',
    'profil_update_address'               => 'Adres',

    'profile_update_agency_name'        => 'Acenta Adı',
    'profile_update_job_title'          => 'Ünvan',
    'profile_update_office_phone'       => 'Ofis telefonu',
    'profile_update_site'               => 'Web Site',
    'profile_update_trade_association'  => 'Ticaret Birliği',
    'profile_update_trade_association_no' => 'Ticaret Birliği Üye No',

    'profil_update_choose_country'        => 'Lütfen bir ülke seçiniz...',
    'profil_update_choose_state'          => 'Lütfen bir bölge seçiniz...',
    'profil_update_choose_city'           => 'Lütfen bir şehir seçiniz...',


    'profil_update_button'  => 'Güncelle',

    /** Enquiry Form Dil Özellikleri*/
    'enquiry_name'          =>  'Lütfen adınızı yazınız.',
    'enquiry_last_name'     =>  'Lütfen soyadınızı yazınız.',
    'enquiry_mobile'        =>  'Lütfen telefon numaranızı yazınız.',
    'enquiry_email'         =>  'Lütfen emailinizi yazınız.',
    'enquiry_email_format'  =>  'Lütfen email biçimini doğru yazınız.',
    'enquiry_city'          =>  'Lütfen ilçe bilgisini yazınız.',
    'enquiry_state'         =>  'Lütfen şehir bilgisini yazınız.',
    'enquiry_country_id'    =>  'Lütfen ülke seçiniz.',
    'enquiry_group_size'    =>  'Lütfen grup sayısını yazınız.',
    'enquiry_group_size_format'  =>  'Lütfen grup alanına sayı yazınız.',
    'enquiry_message'       =>  'Lütfen mesajınızı yazınız.',

    /** Search */
    'tour_name_like'        => 'Tur Adı',
    'continent'             => 'Kıta',
    'country'               => 'Ülke',
    'style_id'              => 'Tur Çeşiti',
    'search_tour_city'      => 'Destinasyon',
    'duration'              => 'Süre',
    'budget'                => 'Fiyat Aralığı',
    'search'                => 'Ara',

    'touritem_pricetext'    => 'Price starts from',
    'touritem_from_from'    => 'From',
    'touritem_from_to'      => 'To',
    'touritem_from_duration'=> 'Süre',
    'touritem_from_days'    => 'Gün',

    /** Booking Form Validation Dil Özellikleri */
    'booking_v_cntfirstname'      => 'Lütfen adınızı yazınız.',
    'booking_v_cntlastname'       => 'Lütfen soyadınızı yazınız.',
    'booking_v_cntmail'           => 'Lütfen emailinizi yazınız.',
    'booking_v_cntmail_email'     => 'Lütfen email biçimini doğru yazınız.',
    'booking_v_cntphone'          => 'Telefon alanı gereklidir.',
    'booking_v_cntphone_numeric'  => 'Lütfen sadece rakam giriniz.',
    'booking_v_cntcountry'        => 'Lütfen ülke seçiniz.',
    'booking_v_cntbirth'          => 'Lütfen yıl seçiniz.',
    'booking_v_travellersnames[]' => 'Lütfen adınızı yazınız.',
    'booking_v_country[]'         => 'Lütfen ülke seçiniz.',
    'booking_v_DOB[]'             => 'Lütfen yıl seçiniz.',
    'booking_v_flight_arrival_datetime'   => 'Lütfen varış tarih ve saatinizi yazınız.',
    'booking_v_flight_arrival_number'     => 'Lütfen uçuş numaranızı yazınız.',
    'booking_v_flight_to_airport'         => 'Lütfen havaalanı bilgisini yazınız.',
    'booking_v_flight_departure_datetime' => 'Lütfen kalkış tarih ve saatini yazınız.',
    'booking_v_flight_departure_number'   => 'Lütfen uçuş numaranızı yazınız.',
    'booking_v_flight_departure_from'     => 'Lütfen kalkış noktanızı yazınız.',
    'booking_v_ccname'            => 'Lütfen kart sahibinin adını yazınız.',
    'booking_v_ccnumber'          => 'Lütfen kart numaranızı yazınız.',
    'booking_v_ccexpm'            => 'Lütfen kartınızın son kullanma tarihini giriniz.',
    'booking_v_ccexpy'            => 'Lütfen kartınızın son kullanma tarihini giriniz.',
    'booking_v_cvv'               => 'Lütfen kartınızın CVV bilgisini giriniz.',

    /** Booking Form Dil Özellikleri */
    'booking_page'  => 'Ödeme Sayfası',

    'booking_contact_person'=> 'İlgili Kişi',
        'booking_name'          => 'İsim *',
        'booking_surname'       => 'Soyisim *',
        'booking_email'         => 'E-Mail *',
        'booking_contact_phone' => 'İletişim Telefonu *',
        'booking_country'       => 'Ülke *',
        'booking_date_of_birth' => 'Doğum Tarihi *',

    'booking_traveller_detail'  => 'Yolcu Detayları *',
        'booking_name_surname'  => 'Ad Soyad *',

    'booking_children_detail'   => 'Çocuk Detayları',

    'booking_flight_detail'     => 'Uçuş Detayları',
        'booking_arrival_detail'    => 'Varış uçuş detaylarınız (eğer şimdi biliniyorsa)',
        'booking_arrival'           => 'Varış Tarihi ve Saati',
        'booking_flight_number'     => 'Uçuş Numarası',
        'booking_flight_airport'    => 'Havaalanına Uçuş',
        'booking_small_arrival'     => 'Kalkış uçuş detaylarınız (şimdi biliniyorsa)',
        'booking_departure'         => 'Kalkış Tarihi ve Saati',
        'booking_departure_from'    => 'Kalkış Yeri',

    'booking_payment_detail'    => 'Ödeme Detayları',
        'booking_credit_card'   => 'Kredi Kartı',
        'booking_bank_transfer' => 'Banka Transferi',
        'booking_deposit'       => 'MEVDUAT - Başka bir yüzde seçilmediği sürece her siparişte% 25 ilk depozito yapılır.',
        'booking_paying_now'    => '(şimdi ödeme, tur başlangıcında ödemek için son bakiye)',
        'booking_card_detail'   => 'Kredi Kartı Bilgileriniz',
        'booking_card_holder'   => 'Kart Sahibinin Adı',
        'booking_card_number'   => 'Kredi Kartı Numarası',
        'booking_card_number_2' => 'Kart numarası 13-17 hane arasında olmalıdır.',
        'booking_expiry_date'   => 'Son Kullanma Tarihi',
        'booking_cvv'           => 'Güvenlik Numarası(CVV)',
        'booking_cvv_2'         => 'Güvenlik Numarası 3-4 hane arasında olmalıdır.',
        'booking_confirm'       => 'Ödemeyi Onayla',

    'booking_adults'        => 'Yetişkin',
    'booking_person'        => 'Kişi',
    'booking_children'      => 'Çocuklar',
    'booking_child'         => 'Çocuk',
    'booking_hotel_star'    => 'Otel Yıldızı',
    'booking_single_room'   => 'Tek Kişilik Oda',
    'booking_double_room'   => 'Çift Kişilik Oda',
    'booking_triple_room'   => 'Triple Oda',
    'booking_pax_total'     => 'Pax Toplam Maliyet',
    'booking_child_cost'    => 'Toplam Çocuk Maliyeti',
    'booking_total'         => 'Genel Toplam',
    'booking_discount'      => 'İndirim',
    'booking_net_payable'   => 'Ödenecek Net',
    'booking_piece'         => 'Parça',

    /** Agency Language */
    'agency_enquiry_message'    => "Bilgi Mesajları Sayfası",
    'agency_booking_page'       => "Ödeme Sayfası",
    'agency_account_update'     => "Acenta Hesap Ayarları",
    'agency_enquiry_page'       => "Enquiry Sayfası",
    'agency_enquiry_message'    => "Mesaj İçeriği",
    'agency_enquiry_status'     => "Mesaj Durumu",
    'agency_enquiry_date'       => 'Mesaj Tarihi',

    'enquiry_message_client'    => "Müşteri",
    'enquiry_message'           => "Mesajınızı Buraya Yazınız.",
    'enquiry_message_send'      => "Cevabı Gönder",

    'agency_booking_title'      => "Acente Sipariş Sayfası",
    'agency_booking_order_number' => 'Order Number',
    'agency_booking_name'       => 'Ad Soyad',
    'agency_booking_status'     => 'Durum',
    'agency_booking_date'       => 'Tarih',
    'agency_booking_order_total'=> 'Toplam',
    'agency_booking_view'       => 'Detay',

    /** User Language */
    'user_account_update'    => "Kullanıcı Hesap Ayarları",
    'user_enquiry_page'      => "Enquiry Sayfası",
    'user_enquiry_message'   => "Enquiry Mesaj Sayfası",
    'user_booking_page'      => "Ödeme Sayfası",

    /** Enquiry Form Dil */
    'enquiry_form_name'     => 'İsim',
    'enquiry_form_lastname' => 'Soyisim',
    'enquiry_form_mobile'   => 'Telefon',
    'enquiry_form_email'    => 'E-Mail Adresi',
    'enquiry_form_country'  => 'Ülke Seçiniz',
    'enquiry_form_state'    => 'Şehir Seçiniz',
    'enquiry_form_city'     => 'Semt Seçiniz',
    'enquiry_form_group_size' => 'Grup Sayısı',
    'enquiry_form_message'    => 'Mesaj',
    'enquiry_form_submit'     => 'Gönder',
    'enquiry_form_success'     => 'Mesajınız gönderilmiştir. En kısa zamanda sizinle iletişime geçeceğiz.',

    'membership_approval'   => 'Üyeliğiniz yönetici tarafından onaylandıktan sonra aktif edilecektir.',

    'pagecontinents'         => 'Kıtalar',

    'search_style_selected' => 'Bir ülke seçin',

    'booking_confirmation'  => 'Rezervasyon işleminiz gerçekleşmiştir. Sipariş kodunuz <strong> :ordercode </strong>',

    'search_continent'  => 'Kıta Seçiniz',
    'search_country'    => 'Ülke Seçiniz',
    'search_tour_name'  => 'Tur Adı',

    'footer_iletisim'   => 'İletişim Bilgileri',
    'footer_information'   => 'Bilgi',

    'slogan_best_price' => 'Our expert will map out your entire trip, with insider recommendations about where to go, what to do, how to get around and where to stay.',
    'slogan_traveller_love' => 'Yüksek müşteri tekrar oranına sahibiz.',
    'slogan_best_travel'    => 'Our in-house specialists will seamlessly help you book the hotels or activities on your itinerary at the best available rates.',
    'slogan_support'    => '7/24 destekle yurtiçi veya yurtdışı farketmeksizin asla uzakta hissetmeyeceksiniz.',

    'title_best_price' => 'Get a Customized Plan',
    'title_traveller_love'  => 'Müşterilerimiz Bizi Seviyor',
    'title_best_travel' => 'Book Your Trip',
    'title_support' => 'Özel Desteklerimiz',

    /*booking detail page*/
    'booking_order_detail'  => 'Sipariş Detayları',
    'booking_tour_name'     => 'Tur Adı',
    'booking_tur_tarihi'    => 'Depar Tarihi',
    'booking_order_code'    => 'Sipariş Kodu',
    'booking_pax_count'     => 'Kişi Sayısı',
    'booking_child_count'   => 'Çocuk Sayısı',
    'booking_room_count'    => 'Oda Sayısı',
    'booking_total_pay'     => 'Toplam Ödeme',


    'sliderbtn'         => 'RANDEVU AL',
    'menudownload'      => 'MENU',

    'appointmnent_date'     => 'Rezervasyon Tarihi',
    'appointmnent_time'     => 'Saat',
    'appointmnent_name'     => 'Ad Soyad',
    'appointmnent_email'    => 'E-mail',
    'appointmnent_phone'    => 'Telefon',
    'appointmnent_note'     => 'Note',
    'appointmnent_btn'      => 'Gönder',

    'appointmnent_pymnt_btn' => 'Ödeme Yap',
    'appointmenttitle'      => 'Randevular',
    'profile_update'        => 'Profili güncelle',
    'profile'               => 'Profil',

    'package_buy'           => 'SATIN AL',

];
