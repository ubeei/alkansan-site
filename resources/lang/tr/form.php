<?php

return [
    'form_input_browse' => 'Bul',

    'register_register'         => 'Üye Ol',
    'register_name'             => 'Ad Soyad',
    'register_email'            => 'E-Mail Adresi',
    'register_password'         => 'Şifre',
    'register_confirm_password' => 'Şifre Tekrar',
    'register_identity_number'  => 'TC Numarası',
    'register_phone'            => 'Telefon',
    'register_address'          => 'Adres',
    'register_country'          => 'Ülke',
    'register_country_select'   => 'Ülke Seçiniz',
    'register_state'            => 'Şehir',
    'register_state_select'     => 'Şehir Seçiniz',
    'register_city'             => 'Semt',
    'register_city_select'      => 'Semt Seçiniz',
    'register_postal_code'      => 'Posta Kodu',
    'register_btn_register'             => 'Kayıt Ol',

    'register_agency'   => 'Acenta Üyeliği',
    'register_user'   => 'Kullanıcı Üyeliği',
    'login' => 'Giriş Yap',
    'remember_me'   => 'Beni Hatırla',
    'forgot_password'   => 'Şifremi Unuttum',
    

];