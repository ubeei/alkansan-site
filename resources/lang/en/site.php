<?php

return [
    'account_logout'    => 'Logout',

    'about_head'  => 'About us',
    'about_first_par' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    'about_second_par' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',



    'home'  => 'Home Page',
    'blog_continue'  => 'Continue reading..',
    'blog_other'  => 'More Blog Posts',

    'categories' => 'Categories',

    'contact_head'  => 'Contact us',
    'contact_sub_head'  => 'Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
        a matter of hours to help you.',
    'contact_form_name'  => 'Your Name',
    'contact_form_email'  => 'Your Email',
        'contact_form_subject'  => 'Subject',
    'contact_form_message'  => 'Your Message',
    'contact_form_submit'  => 'Send',
    'contact_address'  => 'Perpa Trade Center A Block Floor:1 No:9/0044 Okmeydanı Şişli/Istanbul',

    'product_code' =>  'Code',
    'product_prodtecs' =>  'Product Technical Information (PDF)',
    'product_cattecs' =>  'Product Technical Information (PDF)',
    'product_company' =>  'Brand',
    'product_power' =>  'Bulb Power',
    'product_volt' =>  'Voltage (V)',
    'product_hour' =>  'Life Time (Hour)',
    'product_socket' =>  'Socket',
    'product_complen' =>  'Combustion Length',
    'product_lenght' =>  'Lenght',
    'product_ansicode' =>  'Ansi Code',
    'product_fiber_code' =>  'Fiber Code',
    'product_light_color' =>  'Light Color',
    'product_light_stream' =>  'Light Stream (Lumen)',
    'product_reflector' =>  'Reflector',
    'product_use_case' =>  'Use Cases',
    'product_detail' => 'Detail',








    'footer_follow' => 'Follow Us',
    'footer_instagram'  => 'Instagram',
    'footer_contact'    => 'Contact info',

    'contact_namesurname'   => 'Name Surname',
    'contact_phone'         => 'Phone',
    'contact_email'         => 'E-mail',
    'contact_massage'       => 'Message',
    'g_recaptcha_respons'   => 'I\'m not a robot',
    'contact_sendmassage'   => 'Send Mail',

    'form_success'      => 'Your message has been sent successfully. You will be contacted as soon as possible.',
    'form_error'        => 'There was an error sending your message. Please try again later.',

    'user_title'        => 'Member Operations',
    'user_description'  => 'Edit Member',
    'user_tags'         => 'Edit Member',
    'profil_duzenle'    => 'Profile Update',

    'profil_update_name'                  => 'Name',
    'profil_update_email'                 => 'E-Mail Adress',
    'profil_update_password'              => 'Password',
    'profil_update_password_confirmation' => 'Confirm Password',
    'profil_update_mobile'                => 'Mobile',
    'profil_update_country'               => 'Country',
    'profil_update_state'                 => 'State',
    'profil_update_city'                  => 'City',
    'profil_update_postal_code'           => 'Postal Code',
    'profil_update_address'               => 'Address',

    'profile_update_agency_name'        => 'Agency Name',
    'profile_update_job_title'          => 'Your Job Title/Position',
    'profile_update_office_phone'       => 'Office Phone',
    'profile_update_site'               => 'Web Site',
    'profile_update_trade_association'  => 'Trade Association',
    'profile_update_trade_association_no' => 'Trade Assoc. Members. No',

    'profil_update_choose_country'        => 'Please select a country...',
    'profil_update_choose_state'          => 'Please select a state...',
    'profil_update_choose_city'           => 'Please select a city...',

    'profil_update_button'  => 'Update',

    /** Enquiry Form Validation Dil Özellikleri*/
    'enquiry_name'          =>  'Please enter your name.',
    'enquiry_last_name'     =>  'Please enter your last name.',
    'enquiry_mobile'        =>  'Please enter your phone number.',
    'enquiry_email'         =>  'Please write your email.',
    'enquiry_email_format'  =>  'Please write your email format correctly.',
    'enquiry_city'          =>  'Please provide the city information.',
    'enquiry_state'         =>  'Please provide the state information.',
    'enquiry_country_id'    =>  'Please select country.',
    'enquiry_group_size'    =>  'Please enter the number of groups.',
    'enquiry_group_size_format'  =>  'Please enter a number in the group field.',
    'enquiry_message'       =>  'Please write your message.',

    /** Search */
    'tour_name_like'        => 'Tour Name',
    'continent'             => 'Continent',
    'country'               => 'Country',
    'style_id'              => 'Tour Style',
    'search_tour_city'      => 'Destination',
    'duration'              => 'Duration',
    'budget'                => 'Budget',
    'search'                => 'Search',

    'touritem_pricetext'    => 'Price starts from',
    'touritem_from_from'    => 'From',
    'touritem_from_to'      => 'To',
    'touritem_from_duration'=> 'Duration',
    'touritem_from_days'    => 'Days',

    /** Booking Form Validation Dil Özellikleri */
    'booking_v_cntfirstname'      => 'Please enter your name.',
    'booking_v_cntlastname'       => 'Please enter your last name.',
    'booking_v_cntmail'           => 'Please write your email.',
    'booking_v_cntmail_email'     => 'Please write your email format correctly.',
    'booking_v_cntphone'          => 'Please enter your phone number.',
    'booking_v_cntphone_numeric'  => 'Please enter numbers only.',
    'booking_v_cntcountry'        => 'Please select country.',
    'booking_v_cntbirth'          => 'Please select date.',
    'booking_v_travellersnames[]' => 'Please enter your name.',
    'booking_v_country[]'         => 'Please select country.',
    'booking_v_DOB[]'             => 'Please select date.',
    'booking_v_flight_arrival_datetime'   => 'Please enter the date and time of arrival.',
    'booking_v_flight_arrival_number'     => 'Please enter your flight number.',
    'booking_v_flight_to_airport'         => 'Please enter the airport information.',
    'booking_v_flight_departure_datetime' => 'Please enter the departure date and time.',
    'booking_v_flight_departure_number'   => 'Please enter your flight number.',
    'booking_v_flight_departure_from'     => 'Please enter departure from. ',
    'booking_v_ccname'            => 'Please enter the name of the card holder.',
    'booking_v_ccnumber'          => 'Please enter the card number.',
    'booking_v_ccexpm'            => 'Please select month.',
    'booking_v_ccexpy'            => 'Please select date.',
    'booking_v_cvv'               => 'Please enter cvv info.',

    /** Booking Form Dil Özellikleri */
    'booking_page'  => 'Tour Booking',

    'booking_contact_person'    => 'Contact Person',
        'booking_name'          => 'Name *',
        'booking_surname'       => 'Surname *',
        'booking_email'         => 'E-Mail *',
        'booking_contact_phone' => 'Contact Phone *',
        'booking_country'       => 'Country *',
        'booking_date_of_birth' => 'Date of Birth *',

    'booking_traveller_detail'  => 'Traveller Details',
        'booking_name_surname'  => 'Name Surname *',

    'booking_children_detail'   => 'Children Details',

    'booking_flight_detail'     => 'Flight Details',
        'booking_arrival_detail'    => 'Your arrival flight details(if known now)',
        'booking_arrival'           => 'Arrival Date & Time',
        'booking_flight_number'     => 'Flight Number',
        'booking_flight_airport'    => 'Flight to Airport',
        'booking_small_arrival'     => 'Your departure flight details (if known now)',
        'booking_departure'         => 'Departure Date & Time',
        'booking_departure_from'    => 'Departure From',

    'booking_payment_detail'    => 'Payment Details',
        'booking_credit_card'   => 'Credit Card',
        'booking_bank_transfer' => 'Bank Transfer',
        'booking_deposit'       => 'DEPOSIT - There is 25% percent initial deposit made with every order unless another percentage is selected',
        'booking_paying_now'    => '(paying now, final balance to pay at tour start)',
        'booking_card_detail'   => 'Your Credit Card Details',
        'booking_card_holder'   => "Card Holder's Name",
        'booking_card_number'   => 'Credit Card Number',
        'booking_card_number_2' => 'Card Number should be between 13-17 digits',
        'booking_expiry_date'   => 'Expiry Date',
        'booking_cvv'           => 'Security Number (CVV)',
        'booking_cvv_2'         => 'Security Number should be between 3-4 digits',
        'booking_confirm'       => 'Confirm Booking',

    'booking_adults'        => 'Adults',
    'booking_person'        => 'Person',
    'booking_children'      => 'Children',
    'booking_child'         => 'Children',
    'booking_hotel_star'    => 'Hotel Category',
    'booking_single_room'   => 'Single Room',
    'booking_double_room'   => 'Double Room',
    'booking_triple_room'   => 'Triple Room',
    'booking_pax_total'     => 'Pax Total Cost',
    'booking_child_cost'    => 'Child Total Cost',
    'booking_total'         => 'Total',
    'booking_discount'      => 'Discount',
    'booking_net_payable'   => 'Net Payable',
    'booking_piece'         => 'Piece',

    /** Agency Language */
    'agency_enquiry_message'    => "Enquiry Message's Page",
    'agency_booking_page'       => "Booking Page",
    'agency_account_update'     => "Agency Account Update",
    'agency_enquiry_page'       => "Enquiry Page",
    'agency_enquiry_message'    => "Message Content",
    'agency_enquiry_status'     => "Message Status",
    'agency_enquiry_date'       => 'Message Date',

    'enquiry_message_client'    => "Client",
    'enquiry_message'           => "Write your message here.",
    'enquiry_message_send'      => "Send Reply",

    'agency_booking_title'      => "Agency Booking Page",
    'agency_booking_order_number' => 'Order Number',
    'agency_booking_name'       => 'Name Surname',
    'agency_booking_status'     => 'Status',
    'agency_booking_date'       => 'Date',
    'agency_booking_order_total'=> 'Total',
    'agency_booking_view'       => 'View',


    /** User Language */
    'user_account_update'    => "User Account Update",
    'user_enquiry_page'      => "Enquiry Page",
    'user_enquiry_message'   => "Enquiry Message's Page",
    'user_booking_page'      => "Booking Page",

    /** Enquiry Form Dil */
    'enquiry_form_name'     => 'Name',
    'enquiry_form_lastname' => 'Last Name',
    'enquiry_form_mobile'   => 'Mobile Phone',
    'enquiry_form_email'    => 'E-Mail Address',
    'enquiry_form_country'  => 'Select Country',
    'enquiry_form_state'    => 'Select State',
    'enquiry_form_city'     => 'Select City',
    'enquiry_form_group_size' => 'Group Size',
    'enquiry_form_message'    => 'Messsage',
    'enquiry_form_submit'     => 'Send',
    'enquiry_form_success'     => 'Your message has been sent. We will contact you as soon as possible.',

    'b2b_see_tours'         => 'See Tours',
    'b2b_download'          => 'Download PDF',

    'membership_approval'   => 'Your membership will be activated upon confirmation by administration.',

    'pagecontinents'         => 'Continents',

    'search_style_selected' => 'Please select a country',

    'booking_confirmation'  => 'Your booking has been processed. Your order code is <strong> :ordercode </strong>',

    'search_continent'  => 'Which Continent?',
    'search_country'    => 'Which Country?',
    'search_tour_name'  => 'Tour Name',

    'footer_iletisim'   => 'Have a Questions?',
    'footer_information'   => 'Information',

    'slogan_best_price' => 'Our expert will map out your entire trip, with insider recommendations about where to go, what to do, how to get around and where to stay.',
    'slogan_traveller_love' => 'Most "repeat" client rate.',
    'slogan_best_travel'    => 'Our in-house specialists will seamlessly help you book the hotels or activities on your itinerary at the best available rates.',
    'slogan_support'    => "With 24/7 support, you won't feel 'away' abroad.",

    'title_best_price' => 'Get a Customized Plan',
    'title_traveller_love'  => 'Travellers Love Us',
    'title_best_travel' => 'Book Your Trip',
    'title_support' => 'Our Dedicated Support',

    /*booking detail page*/
    'booking_order_detail'  => 'Order Details',
    'booking_tour_name'     => 'Tour Name',
    'booking_tur_tarihi'    => 'Departure Date',
    'booking_order_code'    => 'Order Code',
    'booking_pax_count'     => 'Pax Count',
    'booking_child_count'   => 'Child Count',
    'booking_room_count'    => 'Room Count',
    'booking_total_pay'     => 'Total Payment',

    'sliderbtn'         => 'YERİNİ AYIRT',

    'appointmnent_date'     => 'Rezervasyon Tarihi',
    'appointmnent_time'     => 'Saat',
    'appointmnent_name'     => 'Ad Soyad',
    'appointmnent_email'    => 'E-mail',
    'appointmnent_phone'    => 'Telefon',
    'appointmnent_note'     => 'Note',
    'appointmnent_btn'      => 'Gönder',

];
