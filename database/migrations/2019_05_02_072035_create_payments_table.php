<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('package_id');
            $table->integer('payment_status_id');
            $table->string('code');
            $table->float('price');
            $table->float('paid_price');
            $table->float('discount_price');
            $table->integer('total_use');
            $table->integer('installment');
            $table->string('card_type');
            $table->string('card_association');
            $table->string('card_family');
            $table->string('last_four_digits');
            $table->longText('raw_result');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
