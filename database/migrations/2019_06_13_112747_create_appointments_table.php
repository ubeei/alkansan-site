<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->dateTime('appointment_time');
            $table->string('code');
            $table->text('items');
            $table->text('note');
            $table->float('price');
            $table->float('paid_price');
            $table->integer('installment');
            $table->string('card_type');
            $table->string('card_association');
            $table->string('card_family');
            $table->string('last_four_digits');
            $table->longText('raw_result');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
