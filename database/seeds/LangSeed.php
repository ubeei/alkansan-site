<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;
class LangSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langs = array(
            array('id' => 1, 'name' => 'Turkish', 'short'=> 'tr', 'flag'=> 'flag-tr','status' => 1),
            array('id' => 2, 'name' => 'English', 'short'=> 'en', 'flag'=> 'flag-us' ,'status' => 1),
        );
		Lang::truncate();
        Lang::insert($langs);
    }
}
