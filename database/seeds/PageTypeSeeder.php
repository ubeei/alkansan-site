<?php

use Illuminate\Database\Seeder;
use App\Models\PageType;

class PageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pagetype = array(
            array('id' => 1, 'name' => 'Standart', 'status' => 1,'standart'),
            array('id' => 2, 'name' => 'Referans', 'status' => 0,'sponsor'),
            array('id' => 3, 'name' => 'Ekibimiz', 'status' => 0,'team'),
        );

		PageType::truncate();
        PageType::insert($pagetype);
    }
}
