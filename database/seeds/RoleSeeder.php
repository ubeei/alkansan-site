<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = array(
            array('name' => 'admin'),
            array('name' => 'agency'),
            array('name' => 'user'),
        );

        Role::truncate();
        Role::insert($roles);

    }
}
