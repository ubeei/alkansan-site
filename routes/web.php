<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', "PageController@home")->name("index");

// Route::get('category',"PageController@page_category_dfp_detail")->name("category");
//

Route::get('{id}-{name}.html',"PageController@page")->name('a.page')->where('id', '[0-9]+');
Route::get('{id}-{name}/{id2}-{name2}.html',"PageController@pagedetail")->name('a.pagedetail')->where('id', '[0-9]+')->where('id2', '[0-9]+');
Route::post('{id}-{name}.html',"PageController@pagepost")->where('id', '[0-9]+');


// Route::get('contact','PageController@page')->name('contact.page');


Route::resource('appointment','AppointmentController');
Route::post('extracheck','AppointmentController@extracheck')->name('extracheck');
Route::resource('payment','PaymentController');
Route::post('paymentresult','PaymentController@success')->name('payment.success');
Route::get('paymentsuccess','PaymentController@successok')->name('payment.successok');
Route::get('paymenttest','PaymentController@test')->name('payment.test');

Route::post('packageresult','PackageController@result')->name('package.result');
Route::get('packagesuccess','PackageController@success')->name('package.success');

Route::get('package/{package}','PackageController@index')->name('site.package.index');
Route::post('package/result/{package}','PackageController@result')->name('site.package.result');

Route::get('blog','BrandController@blog')->name('site.blog.index');
Route::get('blog/{id}','BrandController@blogContent')->name('blog.content');
Route::get('contact','BrandController@contact')->name('contact');
Route::post('sendMessage', 'BrandController@sendMessage')->name('brand.sendMessage');
Route::get('about','BrandController@about')->name('about');


Auth::routes();

Route::post('ajax/hour', 'AjaxController@hour')->name('hour');
Route::get('ajax/state',"AjaxController@state")->name('ajax.state');
Route::post('appoint/detail','AppointmentController@detail')->name('appointment.detail');

Route::post('ajax/coupon', 'CouponController@index')->name('ajax.coupon');
Route::get('ajax/coupon/remove', 'CouponController@destroy')->name('ajax.coupon.remove');

Route::group(["middleware" => ["rolecheck:user","auth"]], function () {
    Route::group(['namespace' => 'User'], function () {

        Route::get('/user-update', 'UserController@edit')->name('user.update');
        Route::post('/user-update', 'UserController@update');
        Route::get('user/appointment', 'UserController@appointment')->name('user.appointment');
    });
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(["middleware" => ["rolecheck:admin","auth"]], function () {

    Route::group(['namespace' => 'Admin'], function () {

      Route::get('admin/blog', 'BlogController@index')->name('admin.blog.index');
      Route::post('admin/blog/create', 'BlogController@create')->name('blog.create');
      Route::get('admin/blog/create', 'BlogController@create')->name('blog.create');

  Route::get('admin/blog/edit/{id}', 'BlogController@edit')->name('blog.edit');
      Route::post('admin/blog/store', 'BlogController@store')->name('admin.blog.store');
      Route::put('admin/blog/update/{id}', 'BlogController@update')->name('blog.update');
        Route::get('admin/blog/update/{id}', 'BlogController@update')->name('blog.update');
      Route::delete('admin/blog/destroy/{id}', 'BlogController@destroy')->name('blog.destroy');
      Route::post('admin/blog/status/{id}', 'BlogController@status')->name('blog.status');

      Route::post('admin/blog/image', 'BlogImageController@image_upload')->name('blog.image');
      Route::post('admin/blog/image/remove', 'BlogImageController@image_remove')->name('blog.image.remove');



        Route::resource('admin/page', 'PageController');
        Route::post('admin/page/order', 'PageController@order')->name('page.order');
        Route::get('admin/page/status/{page}', 'PageController@status')->name('page.status');

        Route::get('admin/page/{page}/home', 'PageController@home')->name('page.home');

        Route::get('admin/lang/{id}', 'Lang@index')->name('admin.lang');
        Route::resource('admin/pagecontent', 'PageContentController');
        Route::post('admin/pagecontent/order', 'PageContentController@order')->name('pagecontent.order');
        Route::get('admin/pagecontent/status/{pagecontent}', 'PageContentController@status')->name('pagecontent.status');
        Route::post('admin/pagecontetupload', 'PageContentController@contetupload')->name('contetupload.image');

        Route::resource('admin/team', 'TeamController');
        Route::post('image/team', 'TeamController@image_upload')->name('team.image');
        Route::put('image/team', 'TeamController@image_upload')->name('team.image');
        Route::post('imageremove/team', 'TeamController@image_remove')->name('team.imageremove');
        Route::post('admin/team/order', 'TeamController@order')->name('team.order');
        Route::get('admin/team/status/{team}', 'TeamController@status')->name('team.status');


        /*Route::resource('admin/sponsor', 'sponsorController');
        Route::post('image/sponsor', 'sponsorController@image_upload')->name('sponsor.image');
        Route::put('image/sponsor', 'sponsorController@image_upload')->name('sponsor.image');
        Route::post('imageremove/sponsor', 'sponsorController@image_remove')->name('sponsor.imageremove');
        Route::post('admin/sponsor/order', 'sponsorController@order')->name('sponsor.order');
        Route::get('admin/sponsor/status/{sponsor}', 'sponsorController@status')->name('sponsor.status');*/

        Route::resource('admin/setting', 'SettingController');
        Route::resource('admin/slider', 'SliderController');
        Route::post('admin/slider/order', 'SliderController@order')->name('slider.order');
        Route::get('admin/slider/status/{slider}', 'SliderController@status')->name('slider.status');

        Route::post('image/slider', 'SliderController@image_upload')->name('slider.image');
        Route::post('image/slider/remove', 'SliderController@image_remove')->name('slider.imageremove');




        Route::resource('admin/contentfile', 'ContentFileController');

        Route::post('image/contentfile', 'ContentFileController@image_upload')->name('contentfile.image');
        Route::put('image/contentfile', 'ContentFileController@image_upload')->name('contentfile.image');
        Route::post('imageremove/contentfile', 'ContentFileController@image_remove')->name('contentfile.imageremove');

        Route::resource('admin/package', 'PackageController', [
            'as' => 'admin'
        ]);
        Route::post('admin/package/order', 'PackageController@order')->name('admin.package.order');
        Route::get('admin/package/status/{package}', 'PackageController@status')->name('admin.package.status');

        Route::resource('admin/extra', 'ExtraController');
        Route::get('admin/extra/status/{extra}', 'ExtraController@status')->name('extra.status');

        Route::resource('admin/time', 'TimeController');

        Route::resource('admin/appointment', 'AppointmentController', [
            'as' => 'admin'
        ]);
        Route::get('admin/appointment/check/{appointment}', 'AppointmentController@check')->name('admin.appointment.check');

        Route::get('admin/accounting', 'AccountingController@index')->name('admin.accounting.index');
        Route::get('admin/appointmentshow/{appointment}', 'AccountingController@appointmentshow')->name('admin.accounting.appointmentshow');

        Route::get('admin/payment', 'AccountingController@paymentindex')->name('admin.accounting.paymentindex');
        Route::get('admin/paymentshow/{appointment}', 'AccountingController@paymentshow')->name('admin.accounting.paymentshow');

        Route::resource('admin/coupon', 'CouponController', [
            'as' => 'admin'
        ]);
        Route::get('admin/coupon/status/{coupon}', 'CouponController@status')->name('admin.coupon.status');

        Route::resource('admin/pagecategory', 'PageCategoryController');
        Route::post('image/pagecategory', 'PageCategoryController@image_upload')->name('pagecategory.image');
        Route::put('image/pagecategory', 'PageCategoryController@image_upload')->name('pagecategory.image');
        Route::post('imageremove/pagecategory', 'PageCategoryController@image_remove')->name('pagecategory.imageremove');
        Route::post('pdf/pagecategory', 'PageCategoryController@pdf_upload')->name('pagecategory.pdf');
        Route::put('pdf/pagecategory', 'PageCategoryController@pdf_upload')->name('pagecategory.pdf');
        Route::post('pdfremove/pagecategory', 'PageCategoryController@pdf_remove')->name('pagecategory.pdfremove');
        Route::post('admin/pagecategory/order', 'PageCategoryController@order')->name('pagecategory.order');
        Route::get('admin/pagecategory/status/{pagecategory}', 'PageCategoryController@status')->name('pagecategory.status');
          Route::get('admin/pagecategory/status/{pagecategory}', 'PageCategoryController@status')->name('pagecategorydetail.index');



        Route::resource('admin/property', 'PropertyController');
        Route::post('admin/property/order', 'PropertyController@order')->name('property.order');
        Route::get('admin/property/status/{property}', 'PropertyController@status')->name('property.status');


        Route::resource('admin/category', 'CategoryController');
        Route::post('image/category', 'CategoryController@image_upload')->name('category.image');
        Route::put('image/category', 'CategoryController@image_upload')->name('category.image');
        Route::post('imageremove/category', 'CategoryController@image_remove')->name('category.imageremove');
        Route::post('pdf/category', 'CategoryController@pdf_upload')->name('category.pdf');
        Route::put('pdf/category', 'CategoryController@pdf_upload')->name('category.pdf');
        Route::post('pdfremove/category', 'CategoryController@pdf_remove')->name('category.pdfremove');
        Route::post('admin/category/order', 'CategoryController@order')->name('category.order');
        Route::get('admin/category/status/{category}', 'CategoryController@status')->name('category.status');


        Route::resource('admin/product', 'ProductController');
        Route::post('image/product', 'ProductController@image_upload')->name('product.image');
        Route::put('image/product', 'ProductController@image_upload')->name('product.image');
        Route::post('imageremove/product', 'ProductController@image_remove')->name('product.imageremove');
        Route::post('pdf/product', 'ProductController@pdf_upload')->name('product.pdf');
        Route::put('pdf/product', 'ProductController@pdf_upload')->name('product.pdf');
        Route::post('familypdf/product', 'ProductController@familypdf_upload')->name('product.familypdf');
        Route::put('familypdf/product', 'ProductController@familypdf_upload')->name('product.familypdf');
        Route::post('pdfremove/product', 'ProductController@pdf_remove')->name('product.pdfremove');
        Route::post('admin/product/order', 'ProductController@order')->name('product.order');
        Route::get('admin/product/status/{product}', 'ProductController@status')->name('product.status');
        Route::post('admin/product/property', 'ProductController@property')->name('admin.product.property');



    });
});

Route::get('categories.html',"ProductController@categories")->name('categories');
//
Route::get('category/{slug}',"ProductController@category")->name('category');

Route::get('{slug}/{id2}-{slug2}',"ProductController@product")->name('product');


Route::get('lang/{id}', 'Lang@index')->name('lang');
