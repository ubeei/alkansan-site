<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Models\Category;
use App\Models\Property;
use App\Models\ProductImage;
use App\Models\ProductProperty;

class ProductImport implements WithHeadingRow, ToModel
{

    public $categories = array();
    public $lang = "";

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //dd($row);
        $insert = array(
            'lang_id' => $this->lang->id,
            'name'  => $row['urun_adi_tr'],
            'meta_desc' => $row['kisa_aciklama_250_karakter_tr'],
            'meta_keyword' => $row['urun_adi_varyasyon_tr'],
            'code' => $row['urun_kodu'],
            'brand' => $row['marka'],
            'short_description' => $row['kisa_aciklama_250_karakter_tr'],
            'pdf' => '/storage/product_pdf/'.$row['urun_teknik_pdf'],
            'family_pdf' => '/storage/product_pdf/'.$row['urun_teknik_aile_pdf'],
            'description' => $row['kullanim_alanlari_uzun_not_tr'],
            'order' => 100,
            'status' => 1);
        $product = new Product;
        $insertproduct = $product->create($insert);

        $category_id = $this->categories;

        $category = Category::find($category_id);
        //dd($insertproduct);

        $insertproduct->categories()->attach($category);

        $insert = array('product_id' => $insertproduct->id, 'path' => '/storage/product_image/'.$row['image']);
        $productImage = new ProductImage;
        $productImage->create($insert);
        if ($category) {
            foreach ($category as $catitem) {
                if ($catitem->properties) {
                    foreach ($catitem->properties as $property) {
                        $slug = str_replace('-','_',str_slug($property->name));
                        $value = $row[$slug];
                        if ($value!="NA") {
                            $insert = array(
                                'product_id' => $insertproduct->id, 
                                'property_id' => $property->id, 
                                'value' => $value
                            );
                            $productProperty = new ProductProperty;
                            $productProperty->create($insert);
                        }
                    }
                }
            }
        }

        return $insertproduct;

    }



    public function headingRow(): int
    {
        return 2;
    }
}
