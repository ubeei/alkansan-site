<?php

use App\Models\Template;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Storage;

if (!function_exists('template_check')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function template_check()
    {
        $template = Template::where('status', 1)->first();
        return $template;
    }
}

if (!function_exists('template_path_check')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function template_path_check($page_path)
    {
        $template = Template::where('status', 1)->first();

        $files = File::exists(base_path().'/resources/views/'.$template->template_path.$page_path.'.blade.php');
        
        if($files)  // seçili temada böyle bir dosya var mı?
        {
            $layouts_extend = $template->template_path.$page_path;
        }
        else
        {
            $layouts_extend = $page_path;
        }
        
        return $layouts_extend;
    }
}
if (!function_exists('image_resize')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function image_resize($imagepath, $w, $h,$crop=0)
    {
        if ($imagepath!="") {
            
        
            $replaceimagepath = str_replace('storage','storage/public',$imagepath);
            
            if (file_exists(public_path($imagepath))) {
                $expath = explode('/',$replaceimagepath);
                $expath[count($expath)-2] = str_replace($expath[count($expath)-2],$w.'x'.$h.'/'.$expath[count($expath)-2],$expath[count($expath)-2]);
                $newpath = implode($expath,'/');
                $p=str_replace('/public','',$newpath);
                $p = public_path($p);
                
                if (!file_exists($p)) {
                    $manager = new ImageManager();
                    $resize = $manager->make(public_path($imagepath));
                    $resize->resize($w, $h, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
              
                    $canvas = $manager->canvas($w, $h);
                    $canvas->insert($resize, 'center');
                    
                    $path = str_replace('/'.$expath[count($expath)-1],'',$newpath);
                   
                    File::makeDirectory(storage_path('app/'.str_replace('/storage/','',$path)), $mode = 0777, true, true);
                    
                    $canvas->save(storage_path('app/'.str_replace('/storage/','',$newpath)));
                    $newpath = str_replace('/storage/public','/storage',$newpath);
                    return $newpath;
                }else{
                    $newpath = str_replace('/storage/public','/storage',$newpath);
                    return $newpath;
                }
            }
        }
    }
}