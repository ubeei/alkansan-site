<?php
use App\Models\Page;
use App\Models\PageContent;
use App\Models\TourGroup;
use App\Models\Tour;
use App\Models\Package;


use App\Models\Country;
use Illuminate\Support\Facades\View;

if (!function_exists('string_search')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function string_search($string)
    {
        if(stripos($string,'{team=') || stripos($string,'{sponsor=') || stripos($string,'{content=') || stripos($string,'{form=') || stripos($string,'{tourgroup=') || stripos($string,'{packets=')){
            
            preg_match('/{(?<type>\w+)=(?<value>\w+)}/', $string, $mapped);
            $rep = "";
            //dd($mapped);
            if ($mapped['type'] == "team" and is_integer((int)$mapped['value'])) {
                $page = Page::find($mapped['value']);
               
                $teams = $page->teams;
                $rep = view('pages.pageteam.list', compact('teams'));
                
            }else if ($mapped['type'] == "sponsor"){
                $page = Page::find($mapped['value']);
                $sponsors = $page->sponsors;
                $rep = view('pages.pagesponsor.list', compact('sponsors'));

            }else if ($mapped['type'] == "content"){
                $contentitem = PageContent::find($mapped['value']);
                $rep = view('pages.contentview.standart', compact('contentitem'));

            }else if($mapped['type'] == "form"){
                if ($mapped['value'] == "contact") {
                    $rep = view('pages.formview.contact')->render();
                }
                if($mapped['value'] == "enquiry"){
                    $rep = view('pages.formview.enquiry');

                    /** <(0_0)> */
                    $countries = Country::get()->pluck('name', 'id');
                    View::share('countries', $countries);
                    /** <(0_0)> */
                }
            }else if ($mapped['type'] == "tourgroup"){
                
                $page = Page::find($mapped['value']);
                $pageitem = Page::where('page_type_id',6)->first();
                $tourgroups = TourGroup::find($mapped['value']);
                $tourgroupsitem= $tourgroups;
                if ($tourgroups) {
                    $tourapi = json_decode($tourgroups->lists);
                
                    
                    
                    $tourgroups = $tourapi;
                    for($i=0; $i<count($tourgroups); $i++)
                    {
                       /* $params['tour_id'] = $tourgroups[$i];
                        if (Cache::has('tourgroup'.$mapped['value'].'_'.$tourgroups[$i])){
                            $tour_details[$i] = Cache::get('tourgroup'.$mapped['value'].'_'.$tourgroups[$i]);
                        } else {*/
                            $tour_details[$i] = Tour::find($tourgroups[$i]); 
                            //req_get('detail',$params);
                            /*Cache::put('tourgroup'.$mapped['value'].'_'.$tourgroups[$i], $tour_details[$i], 100);
                        }*/
                        
                    }
                    $tour_detail = array();
                    
                    if (count($tour_details)>0) {
                        
                        for($i=0; $i<count($tour_details); $i++)
                        {
                            //d($tour_details);
                            try {
                                $tour_detail[$i]['ID']                  = $tour_details[$i]->id;
                                $tour_detail[$i]['Name']                = $tour_details[$i]->name;
                                $tour_detail[$i]['Discount_Flat']       = $tour_details[$i]->discount_flat;
                                $tour_detail[$i]['Duration']            = $tour_details[$i]->duration;
                                $tour_detail[$i]['Highlights']          = $tour_details[$i]->highlights;
                                $tour_detail[$i]['Price_Default']       = $tour_details[$i]->price_default;
                                $tour_detail[$i]['Tour_Route']          = $tour_details[$i]->tour_route;
                                $tour_detail[$i]['Tour_Start_City']     = $tour_details[$i]->tour_start_city;
                                $tour_detail[$i]['Tour_End_City']       = $tour_details[$i]->tour_end_city;
                                $tour_detail[$i]['Image_Long']          = $tour_details[$i]->image_long;
                                $tour_detail[$i]['Invoice_Currency']    = $tour_details[$i]->invoice_currency;
                                //$tour_detail[$i]['_config_data_']       = $tour_details[$i]->_config_data_;
                            } catch (\Throwable $th) {
                               //d($th);
                            }
                               
                            
                        }
                    }
                }else{
                    $tour_detail = array(); 
                }
                //d($tour_detail);
                $tours = $tour_detail;
                $rep = view(template_path_check('/pages/pagetourgroup/list'), compact('tour_detail','pageitem','tourgroupsitem'))->render();
            }else if($mapped['type'] == "packets"){
                $packets = explode(',',$mapped['value']);
                $packages = Package::where('status',1)->get();
                $rep = view(template_path_check('/pages/pagepackage/list'), compact('packages'));
            }
            
            $string = str_replace('{'.$mapped['type'].'='.$mapped['value'].'}',$rep,$string);
            $string = string_search($string);
        }
        return $string;
    }
    if (!function_exists('d')) {
        function d()
        {
            $args = func_get_args();
            call_user_func_array('dump', $args);
        }
    }
}
