<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Appointment;


class AppointmentNotification extends Notification
{
    use Queueable;
    protected $appointment;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $appointment = $this->appointment;

        //dd($appointment->discount);
        
        return (new MailMessage)
        ->subject(env('APP_NAME')." - ".$appointment->user->name." Randevu")
        ->line('Ad Soyad :'.$appointment->user->name)
        ->line('Telefon:'.$appointment->user->phone)
        ->line('E-Mail Address :'.$appointment->user->email)
        ->line('Randevu Saati :'.$appointment->appointment_time)
        ->line('Takip Kodu :'.$appointment->code)
        ->line('Not :'.$appointment->note)
        ->line(($appointment->coupon) ? 'Kupon :'.$appointment->coupon->code : 'Kupon : -' )
        ->line('İndirim :'.number_format($appointment->discount,2). 'TL')
        ->line('Ödenen Fiyat :'.number_format($appointment->paid_price,2). 'TL')
        ->line('Taksit :'.$appointment->installment)
        ->line('Kart Tipi :'.$appointment->card_type)
        ->line('Kart Tipi :'.$appointment->card_association)
        ->line('Banka :'.$appointment->card_family)
        ->line('Kart Numarası : ************'.$appointment->last_four_digits);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
