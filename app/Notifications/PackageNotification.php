<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Payment;

class PackageNotification extends Notification
{
    use Queueable;
    protected $payment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $payment = $this->payment;

        return (new MailMessage)
        ->subject(env('APP_NAME')." - ".$payment->user->name." Paket - ".$payment->code)
        ->line('Ad Soyad :'.$payment->user->name)
        ->line('Telefon:'.$payment->user->phone)
        ->line('E-Mail Address :'.$payment->user->email)
        ->line('Paket :'.$payment->package->name)
        ->line('Takip Kodu :'.$payment->code)
        ->line('Ödenen Fiyat :'.number_format($payment->paid_price,2). 'TL')
        ->line('Taksit :'.$payment->installment)
        ->line('Kart Tipi :'.$payment->card_type)
        ->line('Kart Tipi :'.$payment->card_association)
        ->line('Banka :'.$payment->card_family)
        ->line('Kart Numarası : ************'.$payment->last_four_digits);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
