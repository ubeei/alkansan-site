<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Lang;
use App\Models\Setting;
use App\Models\Page;
use View;
use Mailgun\Mailgun;
use Illuminate\Support\Facades\Mail;



class ProductController extends Controller
{
  protected $activemenu = array();
  protected $menucategories;


  public function __construct()
  {
      /* $this->middleware('auth'); */

      $this->lang = Lang::where('short',\App::getLocale())->first();
      $this->langs = Lang::where('status',1)->get();
      $this->title = env('APP_NAME');
      $this->meta_description = env('META_DESCRIPTION');
      $this->meta_tags = env('META_TAGS');
      $this->homepage = Page::where('homepage',1)->where('lang_id',$this->lang->id)->first();

      $this->topmenus = Page::where('topmenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();
      $this->bottommenus = Page::where('submenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();

      $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();
      $this->menucategories = Category::where('lang_id',$this->lang->id)->where('status',1)->where('parent_id',0)->orderBy('order')->get();

      View::share('activemenu', $this->activemenu);
      View::share('menucategories', $this->menucategories);
}



public function categories(){




  $page = Page::find(7);


  View::share('title', $page->title.' - '.$this->title);
  View::share('meta_description', $page->meta_description);
  View::share('meta_tags', $page->meta_tags);



  $page_path = '/pages/pagecategory/list';          // kontrol edilecek dosya uzantısı
  $files = template_path_check($page_path);           // helpera kontrol için giden dosya


  $categories = Category::where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();
  $products = Product::where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();
  //$images = $products->product_images;
  
  return view($files, compact('categories','products'));

}
public function category($slug){

  $page = Page::find(7);


  View::share('title', $page->title.' - '.$this->title);
  View::share('meta_description', $page->meta_description);
  View::share('meta_tags', $page->meta_tags);




  $page_path = '/pages/pagecategory/category';          // kontrol edilecek dosya uzantısı
  $files = template_path_check($page_path);

  $categories = Category::where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();
  $category = Category::where('lang_id',$this->lang->id)->where('slug',$slug)->where('status',1)->orderBy('order')->first();
  if($category)
  $cproducts = $category->products;
  //Product::where('lang_id',$this->lang->id)->where('cat_id',$category->id)->where('status',1)->orderBy('order')->get();
  else
  $cproducts = array();
// $prod = $category->products;
//   dd($prod);
//   $product[0]->categories[0];
           // helpera kontrol için giden dosya



  // $images = array();
  // foreach($products as $product){
  //   $images[] = ProductImage::where('product_id',$product->id)->first();
  // }

  /* $images = $category->images; */

  return view($files, compact('categories','cproducts','category'));



}
public function product($slug,$id2,$slug2){
	
	$page = Page::find(8);
	  
  	View::share('title', $page->title.' - '.$this->title);
  	View::share('meta_description', $page->meta_description);
  	View::share('meta_tags', $page->meta_tags);

 	$page_path = '/pages/pageproduct/index';          // kontrol edilecek dosya uzantısı
  	$files = template_path_check($page_path);


  	$product = Product::where('id',$id2)->first();

	if($product)
		$images = $product->product_images;
		else
		$images = array();

		return view($files, compact('product','images'));
	}

}

?>
