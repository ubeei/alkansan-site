<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Models\Lang;
use App\Models\Page;
use App\Models\Setting;
use View;
use Session;
use Auth;
use App\Models\Extra;
use App\Models\Appointment;
use App\Notifications\AppointmentNotification;
use Carbon\Carbon;


class PaymentController extends Controller
{

    protected $langs;
    protected $lang;
    protected $breadcrumb;
    protected $pages;
    protected $title;
    protected $meta_description;
    protected $meta_tags;
    protected $activemenu = array();
    protected $settings;
    protected $currencies;
    protected $homepage;
    

    public function __construct() 
    {
        $this->middleware('auth');
        if(!Session::get('appointment')){
            return redirect()->route('appointment.index');
        }
       
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->langs = Lang::where('status',1)->get();
        $this->title = env('APP_NAME');
        $this->meta_description = env('META_DESCRIPTION');
        $this->meta_tags = env('META_TAGS');
        $this->homepage = Page::where('homepage',1)->where('lang_id',$this->lang->id)->first();
        
        $this->topmenus = Page::where('topmenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();
        $this->bottommenus = Page::where('submenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();

        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();

        

        View::share('topmenus', $this->topmenus);
        View::share('bottommenus', $this->bottommenus);
        View::share('homepage', $this->homepage);
        View::share('setting', $this->setting);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointment = Session::get('appointment');
        if(!Session::get('appointment')){
            return redirect()->route('appointment.index');
        }
        $totalprice = 0;
        $discount = 0;
        $couponid=0;

        $total_use = Auth::user()->payments->sum('total_use');
        $total_appointment = Auth::user()->appointments()->where('status',1)->count();
            
        if (($total_use-$total_appointment)>0) {
            $package = true;
        }else{
            $package = false;
        }
            
        if ($appointment) {
            $extras = Extra::whereIn('id',$appointment['extra'])->orderBy('group')->get();

            foreach ($extras as $extra) {
                if ($extra->id==99) {
                    if ($package) {
                        $totalprice+=0;
                    }else{
                        $totalprice+=$extra->price;
                    }
                    
                }else{
                    $totalprice+=$extra->price;
                }
            }
        }
        
        if (Session::has('coupon')){
        
            $coupon = Session::get('coupon');
            $couponid = $coupon->id;
            if ($coupon->coupon_type_id==1) {
                $discount = $coupon->value;
            } else if($coupon->coupon_type_id==2) {
                $discount = $totalprice * ($coupon->value/100);                    
            }
        }

        $totalprice = $totalprice-$discount;

        $packages = Package::where('status',1)->get();
        $page_path = '/payment/index';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya
        $code = strtoupper(substr(bin2hex(openssl_random_pseudo_bytes(20)), 0, 10));
        //dd(Session::all());
        $options = new \Iyzipay\Options();
        $options->setApiKey("1wI7YwTr0zn9cesTdzznvXkRUT6Hp7dw");
        $options->setSecretKey("s5e6BPMeHgpZUi9IWmukLDM7Vjt07M4r");
        $options->setBaseUrl("https://api.iyzipay.com");
        /* $options->setApiKey("sandbox-afXhZPW0MQlE4dCUUlHcEopnMBgXnAZI");
        $options->setSecretKey("sandbox-wbwpzKIiplZxI3hh5ALI4FJyAcZKL6kq");
        $options->setBaseUrl("https://sandbox-api.iyzipay.com");  */
                
        $request = new \Iyzipay\Request\CreateCheckoutFormInitializeRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId("1");
        $request->setPrice($totalprice);
        $request->setPaidPrice($totalprice);
        $request->setCurrency(\Iyzipay\Model\Currency::TL);
        $request->setBasketId($code);
        $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::LISTING);
        $request->setCallbackUrl(route('payment.success'));
        $request->setEnabledInstallments(array(2, 3));

        $buyer = new \Iyzipay\Model\Buyer();

        $buyer->setId($code);
        $name = explode(' ',Auth::user()->name);
        $lastname = "";
        foreach ($name as $key => $value) {
            if($key>0){
                $lastname.=$value.' ';
            }
        }
        $buyer->setName($name[0]);
        $buyer->setSurname($lastname);
        $buyer->setGsmNumber(Auth::user()->phone);
        $buyer->setEmail(Auth::user()->email);
        $buyer->setIdentityNumber(Auth::user()->identity_number);
        /* $buyer->setLastLoginDate("2015-10-05 12:43:35");
        $buyer->setRegistrationDate("2013-04-21 15:12:09"); */
        $buyer->setRegistrationAddress(Auth::user()->address);
        /* $buyer->setIp("85.34.78.112"); */
        $buyer->setCity(Auth::user()->state->name);
        $buyer->setCountry(Auth::user()->country->name);
        /* $buyer->setZipCode("34732"); */

        $request->setBuyer($buyer);
        $shippingAddress = new \Iyzipay\Model\Address();
        $shippingAddress->setContactName("Fatura");
        $shippingAddress->setCity(Auth::user()->state->name);
        $shippingAddress->setCountry(Auth::user()->country->name);
        $shippingAddress->setAddress(Auth::user()->address);
        $shippingAddress->setZipCode(Auth::user()->zipcode);
        $request->setShippingAddress($shippingAddress);

        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName("Fatura");
        $billingAddress->setCity(Auth::user()->state->name);
        $billingAddress->setCountry(Auth::user()->country->name);
        $billingAddress->setAddress(Auth::user()->address);
        $billingAddress->setZipCode(Auth::user()->zipcode);
        $request->setBillingAddress($billingAddress);
        
        $basketItems = array();
        $firstBasketItem = new \Iyzipay\Model\BasketItem();
        $firstBasketItem->setId("1");
        $firstBasketItem->setName("Hizmet");
        $firstBasketItem->setCategory1("Kategori");
        $firstBasketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
        $firstBasketItem->setPrice($totalprice);
        $basketItems[0] = $firstBasketItem;


        $request->setBasketItems($basketItems); 

        $checkoutFormInitialize = \Iyzipay\Model\CheckoutFormInitialize::create($request, $options);

        
        //dd($checkoutFormInitialize);

      
        return view($files, compact('page','packages','checkoutFormInitialize'));
    }
    public function successok(Request $request)
    {
        $r = $request->all();
        $code = $r['code'];

        $page_path = '/payment/success';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path); 

        return view($files, compact('code'));
    }
    public function success(Request $request)
    {
        /* $options = new \Iyzipay\Options();
        $options->setApiKey("sandbox-afXhZPW0MQlE4dCUUlHcEopnMBgXnAZI");
        $options->setSecretKey("sandbox-wbwpzKIiplZxI3hh5ALI4FJyAcZKL6kq");
        $options->setBaseUrl("https://sandbox-api.iyzipay.com"); */

        $options->setApiKey("1wI7YwTr0zn9cesTdzznvXkRUT6Hp7dw");
        $options->setSecretKey("s5e6BPMeHgpZUi9IWmukLDM7Vjt07M4r");
        $options->setBaseUrl("https://api.iyzipay.com");

        $r = $request->all();
        $requestiyzico = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
        $requestiyzico->setLocale(\Iyzipay\Model\Locale::TR);
        $requestiyzico->setToken($r['token']);

        $checkoutForm = \Iyzipay\Model\CheckoutForm::retrieve($requestiyzico, $options);

        //dd($checkoutForm);
        
        if($checkoutForm->getpaymentStatus() == 'SUCCESS'){
            
            $appointmentsession = Session::get('appointment');
            $extras = Extra::whereIn('id',$appointmentsession['extra'])->orderBy('group')->get();
            $items = array();
            $totalprice = 0;
            $total_use = Auth::user()->payments->sum('total_use');
            $total_appointment = Auth::user()->appointments()->where('status',1)->count();
            //dd($total_appointment);
            if (($total_use-$total_appointment)>0) {
                $package = true;
            }else{
                $package = false;
            }
            foreach ($extras as $extra) {
                if ($extra->id==99) {
                    if ($package) {
                        $items[]=array(
                            'name' => $extra->name,
                            'price' => 0);
                        $totalprice += 0;
                    } else {
                        $items[]=array(
                            'name' => $extra->name,
                            'price' => $extra->price);
                        $totalprice += $extra->price;
                    }
                    
                } else {
                    $items[]=array(
                        'name' => $extra->name,
                        'price' => $extra->price);
                    $totalprice += $extra->price;
                }
                
            }
            $discount = 0;
            $coupon = false;
            if (Session::has('coupon')){
        
                $coupon = Session::get('coupon');
                $couponid = $coupon->id;
                if ($coupon->coupon_type_id==1) {
                    $discount = $coupon->value;
                } else if($coupon->coupon_type_id==2) {
                    $discount = $totalprice * (100/$coupon->value);                    
                }
            }
            $dt = Carbon::createFromFormat('d/m/Y H:i', $appointmentsession['date'].' '.$appointmentsession['time']);
            //dd($dt);
            $insert = array(
                'appointment_time'  => $dt,
                'user_id'           => Auth::user()->id,
                'code'              => $checkoutForm->getbasketId(),
                'items'             => json_encode($items),
                'note'              => ($appointmentsession['note']) ? $appointmentsession['note'] : '-' ,
                'price'             => $checkoutForm->getprice(),
                'paid_price'        => $checkoutForm->getpaidPrice(),
                'installment'       => $checkoutForm->getinstallment(),
                'card_type'         => $checkoutForm->getcardType(),
                'card_association'  => $checkoutForm->getcardAssociation(),
                'card_family'       => $checkoutForm->getcardFamily(),
                'last_four_digits'  => $checkoutForm->getlastFourDigits(),
                'raw_result'        => $checkoutForm->getrawResult(),
                'status'            => 1,
            );
            if ($coupon) {
                $insert['coupon_id'] = $coupon->id;
                $insert['discount'] = $discount;
            }else{
                $insert['coupon_id'] = 0;
                $insert['discount'] = 0;
            }
            $appointment = Appointment::create($insert);

            $appointment->user = Auth::user();
            
            $appointment->email = env('SYSTEM_MAIL');
            //$appointment->email = "cihan@targetdigital.com.tr";

            $appointment->notify(new AppointmentNotification($appointment));

            Session::pull('coupon');
            Session::save();

            return redirect()->route('payment.successok',['code' => $checkoutForm->getbasketId()]);
            
        }else if($checkoutForm->getpaymentStatus() == 'FAILURE'){
            return redirect()->route('payment.index')
                        ->withErrors($checkoutForm->geterrorMessage())
                        ->withInput();
        }else{
            return redirect()->route('payment.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
    public function test()
    {
        $dt = Carbon::createFromFormat('d/m/Y H:i', '16/07/2019 07:00');
        $insert = array(
            'appointment_time'  => $dt,
            'user_id'           => Auth::user()->id,
            'code'              => '0000',
            'items'             => '-',
            'note'              => '-' ,
            'price'             => '0.0',
            'paid_price'        => '0.0',
            'installment'       => 1,
            'card_type'         => '-',
            'card_association'  => '-',
            'card_family'       => '-',
            'last_four_digits'  => '-',
            'raw_result'        => '-',
            'status'            => 1,
        );
        $appointment = Appointment::create($insert);

        $appointment->user = Auth::user();
        
        //$appointment->email = env('SYSTEM_MAIL');
        $appointment->email = 'cihan@targetdigital.com.tr';

        $appointment->notify(new AppointmentNotification($appointment));
        //return redirect()->route('payment.successok',['code' => $checkoutForm->getbasketId()]);
        dd($appointment);
    }
}
