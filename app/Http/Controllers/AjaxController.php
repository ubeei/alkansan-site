<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Time;
use Carbon\Carbon;
use App\Models\State;
use DB;

class AjaxController extends Controller
{
    //
    public function hour(Request $request)
    {
        DB::enableQueryLog();

        # your laravel query builder goes here

        
        $r = $request->all();
        $c = Carbon::createFromFormat('d/m/Y H:i', $r['date'].' 00:00');
        $cx = Carbon::createFromFormat('d/m/Y H:i', $r['date'].' 00:00')->add(1, 'day');
       
        $dates = Time::whereBetween('datetime',[$c, $cx])->get();
        $times = array();
        foreach ($dates as $date) {
            //d($date);
            if($date->qty > $date->appointments()->where('status',1)->count()){
                $times[] = date('H:i',strtotime($date->datetime));
            }
        }
        return json_encode($times);
    }
    public function state(Request $request)
    {
        $req = $request->all();
        $states = array();
        if ($req['country_id']!="") {
            $states = State::where('country_id',$req['country_id'])->pluck('name','id');
        }
        return $states;
    }
}
