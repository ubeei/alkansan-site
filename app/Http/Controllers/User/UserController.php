<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Lang;
use App\User;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Country;
use App\Models\State;
use App\Models\City;

use App\Models\Appointment;
use View;
use Auth;
use DB;

class UserController extends Controller
{
    public function __construct() 
    {
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->title = env('APP_NAME');
        $this->meta_description = env('META_DESCRIPTION');
        $this->meta_tags = env('META_TAGS');
        $this->homepage = Page::where('homepage',1)->where('lang_id',$this->lang->id)->first();

        $this->topmenus = Page::where('topmenu',1)->where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();
        $this->bottommenus = Page::where('submenu',1)->where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();

        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();
        
        View::share('topmenus', $this->topmenus);
        View::share('bottommenus', $this->bottommenus);
        View::share('homepage', $this->homepage);
        View::share('setting', $this->setting);
        
    }
    public function edit()
    {
        View::share('title', \Lang::get('site.user_title'));
        View::share('meta_description', \Lang::get('site.user_description'));
        View::share('meta_tags', \Lang::get('site.user_tags'));


        $user = User::find(Auth::user()->id);

        $countries = Country::get()->pluck('name', 'id');
        $state = State::find(Auth::user()->state_id);

        View::share('countries', $countries);
        View::share('state', $state);
        
        $page_path = '/users/user_update';   
        $files = template_path_check($page_path);  
        return view($files, compact('user'));
    }

    public function update(Request $request)
    {
        if($request->password)
        {
            $this->validate($request, ['password' => 'required|string|min:6|confirmed']);
            $input_user['password'] = md5($request->password);
        }

        $this->validate($request, [
            'name'          => 'required',
            'email'         => 'required',
            'phone'        => 'required',
            'country_id'    => 'required',
            'state_id'      => 'required',
            'postal_code'   => 'required',
            'address'       => 'required',
            'identity_number' => 'required',
        ]);

        $input_user['name'] = $request->name;
        $input_user['email'] = $request->email;
        $input_user['phone'] = $request->phone;
        $input_user['state_id'] = $request->state_id;
        $input_user['postal_code'] = $request->postal_code;
        $input_user['address'] = $request->address;
        $input_user['identity_number'] = $request->identity_number;

        $user = User::find(Auth::user()->id);
        $user->update($input_user);
        
        
        return redirect()->route('user.update');
    }
    public function appointment()
    {
        $appointments = Auth::user()->appointments;
        
        $page_path = '/users/appointment';   
        $files = template_path_check($page_path);  
        return view($files, compact('appointments'));
    }
}
