<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\BlogImages;
use App\Models\Lang;
use App\Models\Setting;
use App\Models\Page;
use View;
use Mailgun\Mailgun;
use Illuminate\Support\Facades\Mail;



class BrandController extends Controller
{

  protected $blog;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->langs = Lang::where('status',1)->get();
        $this->title = env('APP_NAME');
        $this->meta_description = env('META_DESCRIPTION');
        $this->meta_tags = env('META_TAGS');
        $this->homepage = Page::where('homepage',1)->where('lang_id',$this->lang->id)->first();

        $this->topmenus = Page::where('topmenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();
        $this->bottommenus = Page::where('submenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();

        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();

        //
        //
        // View::share('topmenus', $this->topmenus);
        // View::share('bottommenus', $this->bottommenus);
        // View::share('homepage', $this->homepage);
        // View::share('setting', $this->setting);



    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function blog()
    {

      $page = Page::where('lang_id',$this->lang->id)->where('page_type_id',12)->first();

// dd($page);


      $meta_tags = $page->meta_keyword;
      $meta_description = $page->meta_desc;
      $title = $this->title. ' - ' .$page->title;


      // View::share('title', env('APP_NAME',$page->title.' - '.$this->title));
      // View::share('meta_description', env('META_DESCRIPTION',$page->meta_description));
      // View::share('meta_tags', env('META_TAGS',$page->meta_tags));
      // View::share('activemenu', $this->activemenu);
      //



      // $this->lang = Lang::where('short',\App::getLocale())->first();
      // $this->langs = Lang::where('status',1)->get();

       $blogs = Blog::where('lang_id', $this->lang->id)->get();
       // dd($blogs);
       $page_path = '/pages/pageblog/list';             // kontrol edilecek dosya uzantısı
       $blogp = template_path_check($page_path);           // helpera kontrol için giden dosya

        return view($blogp,compact('blogs','meta_tags','meta_description','title'));
    }

    public function blogContent($id){
      // $this->lang = Lang::where('short',\App::getLocale())->first();
      // $this->langs = Lang::where('status',1)->get();

       $blog = Blog::where('status',1)->where('id',$id)->where('lang_id',$this->lang->id)->first();

       $blogImages = BlogImages::where("blog_id",$id)->get();
       $blogs = Blog::where('status',1)->orderBy('created_at', 'desc')->take(4)->get();

       $page = Page::where('lang_id',$this->lang->id)->where('page_type_id',12)->first();

       $meta_tags = $page->meta_keyword;
       $meta_description = $page->meta_desc;
       $title = $this->title. ' - BLOG POST';



       $page_path = '/pages/pageblog/content';             // kontrol edilecek dosya uzantısı
       $blogp = template_path_check($page_path);           // helpera kontrol için giden dosya

        return view($blogp,compact('blog','blogImages','blogs'));

    }

    public function contact(){


      $page = Page::where('lang_id',$this->lang->id)->where('page_type_id',4)->first();

      $meta_tags = $page->meta_keyword;
      $meta_description = $page->meta_desc;
      $title = $this->title. ' - ' .$page->title;



      $page_path = '/pages/pagecontact/index';
      $page = template_path_check($page_path);

      return view($page);
    }

    public function about(){

      $page = Page::where('lang_id',$this->lang->id)->where('page_type_id',13)->first();

      $meta_tags = $page->meta_keyword;
      $meta_description = $page->meta_desc;
      $title = $this->title. ' - ' .$page->title;




      $page_path = '/pages/pageabout/index';
      $page = template_path_check($page_path);

      return view($page);
    }

    public function sendMessage(Request $request){
       try{
           $r = $request->all();

           $page_path = '/pages/pagecontact/message';
           $page = template_path_check($page_path);


           $data = array('name'=>$r['name'], 'email'=>$r['email'],'subject'=>$r['subject'], 'message'=>$r['body']);
           Mail::send("$page", ["data"=>$data], function($message) use ($data){
              $message->to('javid10@mail.ru', 'Alkansan')->subject('Alkansan Contact Form Message');
           });

           return back();
       }
       catch (Exception $e){}
   }
}
