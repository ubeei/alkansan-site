<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Role;
use App\Models\Country;
use App\Models\State;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        $validator = Validator::make($data, [
            'name'              => ['required', 'string', 'max:255'],
            'email'             => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'          => ['required', 'string', 'min:8', 'confirmed'],
            'phone'             => ['required', 'max:25'],
            'identity_number'   => ['required', 'min:11'],
            'country_id'        => ['required'],
            'state_id'          => ['required'],
            'address'           => ['required', 'min:5'],
            'zipcode'           => ['min:5'],
            
        ]);
        $validator->setAttributeNames([
            'name'                  => trans('form.register_name'),
            'email'                 => trans('form.register_email'),
            'password'              => trans('form.register_password'),
            'phone'                 => trans('form.register_phone'),
            'identity_number'       => trans('form.register_identity_number'),
            'country_id'            => trans('form.register_country'),
            'state_id'              => trans('form.register_state'),
            'zipcode'               => trans('form.register_postal_code'),
            'address'               => trans('form.register_address')
        ]);
        
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $user = User::create([
            'name'              => $data['name'],
            'email'             => $data['email'],
            'password'          => Hash::make($data['password']),
            'phone'             => $data['phone'],
            'identity_number'   => $data['identity_number'],
            'country_id'        => $data['country_id'],
            'state_id'          => $data['state_id'],
            'zipcode'           => $data['zipcode'],
            'address'           => $data['address'],
        ]);
        
        $user->roles()->attach(2);
        return $user;
    }

    public function showRegistrationForm()
    {
        $countries=Country::pluck('name','id');
        $states = State::where('country_id',223)->pluck('name','id');
        
        $page_path = '/auth/register';
        $files = template_path_check($page_path); 
        return view($files, compact('countries'));
    }
}
