<?php

namespace App\Http\Controllers;

//use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Http\Requests\AppointmentPost;
use Session;
use App\Models\Lang;
use App\Models\Page;
use App\Models\Setting;
use View;
use Carbon\Carbon;
use App\Models\Time;
use App\Models\Extra;
use Illuminate\Support\Facades\Validator;
use Sewssion;
use Auth;
use App\Models\Category;

class AppointmentController extends Controller
{
    protected $langs;
    protected $lang;
    protected $breadcrumb;
    protected $pages;
    protected $title;
    protected $meta_description;
    protected $meta_tags;
    protected $activemenu = array();
    protected $settings;
    protected $currencies;

    public function __construct() 
    {
        //dd(Session::all());
        $this->middleware('auth');


        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->langs = Lang::where('status',1)->get();
        $this->title = env('APP_NAME');
        $this->meta_description = env('META_DESCRIPTION');
        $this->meta_tags = env('META_TAGS');
        $this->homepage = Page::where('homepage',1)->where('lang_id',$this->lang->id)->first();

        $this->topmenus = Page::where('topmenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();
        $this->bottommenus = Page::where('submenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();

        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();

        View::share('topmenus', $this->topmenus);
        View::share('bottommenus', $this->bottommenus);
        View::share('homepage', $this->homepage);
        View::share('setting', $this->setting);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extras = Extra::where('group','>',0)->where('status',1)->orderBy('group')->get();
        $nowdate = Carbon::now();
        $times = Time::where('datetime','>',$nowdate)->orderBy('datetime')->get();
        $timelist = array();

        
        foreach ($times as $time ) {
            if ($time->qty > $time->appointments()->where('status',1)->count()) {
                $timelist[] = $time;
            }
        }
        
        if ($timelist) {
            $firstday = $times[0]->datetime;
            $endday = $times[count($times)-1]->datetime;
        }else{
            $firstday   = false;
            $endday     = false;
        }
        $datelist = array();
        foreach ($timelist as $timeitem) {
            $x = date('jn',strtotime($timeitem->datetime));
            if (!in_array($x,$datelist)) {
                $datelist[] = $x;
            }
        }
        $disableList = array();
        if ($firstday) {
            $date = $firstday;
            while (strtotime($date) <= strtotime($endday)) {
                    if (!in_array(date('jn',strtotime($date)),$datelist )) {
                        $disableList[] = date ("d/m/Y",strtotime($date));
                    }
                    $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
            }
        }
        $disableList = json_encode($disableList);
        $datelist = json_encode($datelist);

        $total_use = Auth::user()->payments->sum('total_use');
        $total_appointment = Auth::user()->appointments()->where('status',1)->count();
        //dd($total_appointment);
        if (($total_use-$total_appointment)>0) {
            $package = true;
        }else{
            $package = false;
        }

       
        //dd($endday);
        $page_path = '/pages/appointment/index';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya

        return view($files,compact('times','timelist','datelist','firstday','endday','disableList','extras','package'));
    }
    public function detail(Request $request)
    {
        $r = $request->all();
        $extras = Extra::whereIn('id',$r['extra'])->where('status',1)->orderBy('group')->get();

        $page_path = '/pages/appointment/detail';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya

        $total_use = Auth::user()->payments->sum('total_use');
        $total_appointment = Auth::user()->appointments()->where('status',1)->count();
        //dd($total_appointment);
        if (($total_use-$total_appointment)>0) {
            $package = true;
        }else{
            $package = false;
        }

        return view($files,compact('extras','package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppointmentPost $request)
    {
        $r = $request->all();

        $appointment['appointment'] = array(
            'date'  => $r['date'], 
            'time'  => $r['time'],
            'extra' => $r['extra'],
            'note'  => $r['note'],
        );
        Session::put($appointment);
        
        return redirect()->route('payment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
    public function extracheck(Request $request)
    {
        $r = $request->all();
        $d = explode('/',$r['date']);
        $y = explode(' ',$d[2]);
        
        $datetime = $y[0].'-'.$d[1].'-'.$d[0].' '.$y[1];
        //dd($datetime);
        $times = Time::where('datetime',$datetime)->get();
        $grouplist=array();
        foreach ($times as $time) {
            $grouplist[] = $time->group_id;
        }
        return $grouplist;
    }
}
