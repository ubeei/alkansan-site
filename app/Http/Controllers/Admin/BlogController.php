<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\BlogImageController;
use App\Models\Lang;
use View;
use App\Http\Requests\BlogPost;
use App\Http\Requests\BlogImagePost;
use App\Models\BlogImages;
use Session;
use App\Models\Setting;
use Intervention\Image\ImageManager;
use Storage;
use File;


class BlogController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct()
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();

        // $this->breadcrumb[] =array('page' => "Home",'Link' => 'dashboard');
        //
        // View::share('breadcrumb', $this->breadcrumb);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $blogs = Blog::get();

        return view('admin.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
$last = Blog::all()->sortBy('id')->last();

if($last)
      $id = $last->id+1;
      else {
        $id = 0;
      }


      $falseImages = BlogImages::where('blog_id',$id)->get();


      // dd($falseImages);

      foreach($falseImages as $images){

        $images->delete();

      }


        return view('admin.blog.create',compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogPost $request)
    {

        $result = $request->all();



        $result['lang_id'] = $this->lang->id;
        $result['status']  = 1;
        // $result['path']   = $request['path'];

        $blog = new Blog;

        $blog->create($result);

        Session::flash('status', 1);

        return redirect()->route('admin.blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BLog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


$blog = Blog::where('id',$id)->first();

      $blogImages = BlogImages::where('blog_id',$id)->get();
      // dd($blogImages);
      $this->id = $id;

        return view('admin.blog.edit',compact('blog','blogImages','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $result = $request->all();
        $blog = Blog::where("id",$id)->first();

        $blog->update($result);

        Session::flash('status', 1);

        return redirect()->route('admin.blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $blog = Blog::where('id',$id);
        $blog->delete();
        Session::flash('status', 1);

        return redirect()->route('admin.blog.index');
    }

    public function status(Blog $blog)
    {
        if ($blog->status==1) {
            $blog->status=0;
        } else {
            $blog->status=1;
        }
        $blog->update();
        Session::flash('status', 1);

        return redirect()->route('admin.blog.index');

    }



}
