<?php
use App\Models\Page;

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Models\PageType;
use App\Models\Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PagePost;
use App\Models\Setting;
use Storage;

use Session;
use View;
use Intervention\Image\ImageManager;


class PageController extends Controller
{
    protected $langs;
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct()
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->langs = Lang::all();
        $this->lang = Lang::where('short',\App::getLocale())->first();

        $this->breadcrumb[] =array('page' => "Home",'Link' => route('home'));
        $this->breadcrumb[] =array('page' => "Page",'Link' => route('page.index'));

        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();


        View::share('langs', $this->langs);
        View::share('lang', $this->lang);
        View::share('breadcrumb', $this->breadcrumb);


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd(Session::get('locale'));
        //dd($request);
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;
        $pages = Page::where('page_id',$page_id)->where('lang_id',$this->lang->id)->orderBy('order')->get();
        //dd($page);,

        return view('admin.page.index',compact('pages','page_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;
        $pagetypes = PageType::pluck('name','id')->all();
        $pagetypes[""] = "";
        ksort($pagetypes);

        return view('admin.page.create',compact('pagetypes','page_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);

        $page = new Page;
        $r = $request->all();
        $l = Lang::where('short',app()->getLocale())->first();

        if ($request->file()) {

            $path = $request->file()['page_img'];

            $manager = new ImageManager(array('driver' => 'gd'));
            $coe =$path->getClientOriginalExtension();

            $resize = $manager->make($path);
            if ($this->setting['page_image']) {
                $ex = explode('x',$this->setting['page_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            $hash = md5($resize->__toString());
            $path_url = "images/page_img/{$hash}.".$coe;
            $resize->save(public_path($path_url));
            $url = "/" . $path_url;

            $r['page_img'] = $url;
        }


        $r['homepage'] = 0;
        $r['lang_id'] = $l->id;
        $r['topmenu'] = (isset($r['topmenu'])) ? 1 : 0 ;
        $r['submenu'] = (isset($r['submenu'])) ? 1 : 0 ;
        $r['status'] = 1;
        $r['order'] = 100;
        //dd($r);
        $page->create($r);
        Session::flash('status', 1);


        return redirect()->route('page.index',[null,'page_id'=>$r['page_id']]);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function home(Page $page)
    {
        //dd($page);
        if ($page->homepage == 1) {
            $page->homepage = 0;
        } else {
            $page->homepage = 1;
        }
        $page->update();
        return redirect()->route('page.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
        $pagetypes = PageType::pluck('name','id')->all();
        $pagetypes[""] = "";
        ksort($pagetypes);

        return view('admin.page.edit',compact('pagetypes','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(PagePost $request, Page $page)
    {
        //
        $r=$request->all();
        $r['topmenu'] = (isset($r['topmenu'])) ? 1 : 0 ;
        $r['submenu'] = (isset($r['submenu'])) ? 1 : 0 ;
        if ($request->file()) {
            $path = $request->file()['page_img'];
            $coe =$path->getClientOriginalExtension();

            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);

            if ($this->setting['page_image']) {
                $ex = explode('x',$this->setting['page_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            $hash = md5($resize->__toString().time());
            $path_url = "images/page_img/{$hash}.".$coe;
            $resize->save(public_path($path_url));
            $url = "/" . $path_url;


            $r['page_img'] = $url;
        }else{
            $r['page_img'] = $page->page_img;
        }
        //dd($r);
        $page->update($r);
        Session::flash('status', 1);

        return redirect()->route('page.index',[null,'page_id'=>$page->page_id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
        $page_id = $page->page_id;
        $page->delete();

        return redirect()->route('page.index',[null,'page_id'=>$page_id]);
    }
    public function order(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;

        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $page = Page::find($key);
                $page->order = $value;
                $page->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('page.index',[null,'page_id'=>$page_id]);
    }
    public function status(Page $page)
    {
        if ($page->status==1) {
            $page->status=0;
        } else {
            $page->status=1;
        }
        $page_id = $page->page_id;
        $page->update();
        Session::flash('status', 1);

        return redirect()->route('page.index',[null,'page_id'=>$page_id]);

    }
}
