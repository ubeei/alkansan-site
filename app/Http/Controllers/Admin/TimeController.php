<?php

namespace App\Http\Controllers\Admin;

use App\Models\Time;
use Illuminate\Http\Request;
use App\Http\Requests\TimePost;
use App\Http\Controllers\Controller;
use App\Models\Group;
use Session;


class TimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        $dates = array();
        $qtys = array();

        foreach ($groups as $group_item) {
            $times[$group_item->id] = Time::where('datetime','>',date('Y-m-d'))->where('group_id',$group_item->id)->get();
      
            foreach ($times[$group_item->id] as $time) {
                $dates[$group_item->id][] = strtotime($time->datetime);
                $qtys[$group_item->id][strtotime($time->datetime)] = $time->qty;
            }  
        }
        
        $startdate = strtotime(date('Y-m-d'));
        $finishdate = $startdate+(10.5*60*60*24);
        $interval = array(7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19);

        foreach ($interval as $key => $value) {
            $interval[$key] = $value*60*60;
        }
        
        return view('admin.time.index',compact('times','startdate','finishdate','interval','dates','qtys','groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Time::truncate();
        $r = $request->all();
        $groups = Group::all();
        foreach ($groups as $group_item) {
            //dd($r['time'][$group_item->id]);
            if (isset($r['time'][$group_item->id])) {
                foreach ($r['time'][$group_item->id] as $key => $value) {
                    //if ($value!="") {          
                        //d($value);  
                        $timedata = arraY(
                            'datetime' => date('Y-m-d H:i',$value),
                            'qty'       => $r['qty'][$group_item->id][$value],
                            'group_id'  => $group_item->id,
                            'status'    => 1,
                        );
                        
                        $time = new Time;
                        $time->create($timedata);
                        //d($timedata);
                    //}
                }
            }
        }
        //exit();
        Session::flash('status', 1);

        return redirect()->route('time.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Time  $time
     * @return \Illuminate\Http\Response
     */
    public function show(Time $time)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Time  $time
     * @return \Illuminate\Http\Response
     */
    public function edit(Time $time)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Time  $time
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Time $time)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Time  $time
     * @return \Illuminate\Http\Response
     */
    public function destroy(Time $time)
    {
        //
    }
}
