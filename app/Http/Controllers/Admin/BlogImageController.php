<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lang;
use View;
use Intervention\Image\ImageManager;
use Session;
use App\Models\Setting;


class BlogImageController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct()
    {
        $this->middleware('auth');

        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();

        $this->breadcrumb[] =array('page' => "Home",'Link' => 'dashboard');

        View::share('breadcrumb', $this->breadcrumb);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in public.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlogImages  $blogImages
     * @return \Illuminate\Http\Response
     */
    public function show(BlogImage $blogImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlogImage  $blogImage
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogImage $blogImage)
    {
        //
    }

    /**
     * Update the specified resource in public.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlogImage  $blogImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogImage $blogImage)
    {
        //
    }

    /**
     * Remove the specified resource from public.
     *
     * @param  \App\Models\BlogImage  $blogImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogImage $blogImage)
    {
        //
    }

    public function image_upload(Request $request)
    {

        if ($request->file()) {
$count = $request->image_count;
            $path = $request->file()['image'][0];

            $coe =$path->getClientOriginalExtension();
            $minetype =$path->getMimeType();
            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            if($this->setting['galery_1_image']!=""){
                $ex = explode('x',$this->setting['galery_1_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            $hash = md5($resize->__toString().time());
            if (!file_exists(public_path("images/blog_images/"))) {
            mkdir(public_path("images/blog_images/", 666, true));
        }
            $path_url = "images/blog_images/{$hash}.".$coe;
            $results = $resize->save(public_path($path_url));


            $url = "/" . $path_url;

            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            if($this->setting['pagecategory_image']!=""){
                $ex = explode('x',$this->setting['pagecategory_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            $hash = md5($resize->__toString().time());
            $path_url = "images/blog_images/t/{$hash}.".$coe;

            $imgresult = $resize->save(public_path($path_url));

            $thumbnail = "/" . $path_url;

            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            if($this->setting['tour_image']!=""){
                $ex = explode('x',$this->setting['tour_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            $hash = md5($resize->__toString().time());
            $path_url = "images/blog_images/mt/{$hash}.".$coe;

            $imgresult = $resize->save(public_path($path_url));

            $mthumbnail = "/" . $path_url;


            $blogImage = new BlogImages;
            $blogImage->blog_id    = $request->blog_id;
            $blogImage->path               = $url;
            $blogImage->path_thumbnail     = $thumbnail;
            $blogImage->path_mthumbnail     = $mthumbnail;
            // dd($blogImage->blog_id);
            $blogImage->save();

            $id = $blogImage->id;


            return array('error'=>0 ,'url'=>$thumbnail,'id'=>$id);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function image_remove(Request $request)
    {
        if ($request->id!="") {
            $blogImage = BlogImages::find($request->id);
            // dd($blogImage);
            $file = public_path().$blogImage['path'];
            $file_thumbnail = public_path().$blogImage['path_thumbnail'];
            \File::delete($file);
            \File::delete($file_thumbnail);
            $blogImage->delete();
            return array('error'=>0);
        }

    }

}
