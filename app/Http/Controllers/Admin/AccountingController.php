<?php

namespace App\Http\Controllers\Admin;

use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Carbon\Carbon;
use App\Models\Payment;

class AccountingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $startdate = new Carbon('tomorrow');
        $finishdate = new Carbon('today');

        $startdate = $startdate->subDays(7);

        $startdate = ($request->startdate) ? new Carbon($request->startdate) : $startdate ;
        $finishdate = ($request->finishdate) ? new Carbon($request->finishdate) : $finishdate ;
        //$startdate = $startdate->subDays(1);
        
        $appointments = Appointment::whereBetween('created_at',[$startdate,$finishdate])->get();
        
        return view('admin.accounting.index',compact('appointments','startdate','finishdate'));
    }
    public function appointmentshow(Appointment $appointment)
    {
        return view('admin.accounting.appointmentshow',compact('appointment'));
    }

    public function paymentindex(Request $request)
    {
        $startdate = new Carbon('tomorrow');
        $finishdate = new Carbon('today');

        $startdate = $startdate->subDays(7);

        $startdate = ($request->startdate) ? new Carbon($request->startdate) : $startdate ;
        $finishdate = ($request->finishdate) ? new Carbon($request->finishdate) : $finishdate ;
        //$startdate = $startdate->subDays(1);
        
        $payments = Payment::whereBetween('created_at',[$startdate,$finishdate])->get();
        
        return view('admin.accounting.paymentindex',compact('payments','startdate','finishdate'));
    }

    public function paymentshow(Payment $payment)
    {
        return view('admin.accounting.paymentshow',compact('payment'));
        
    }

}
