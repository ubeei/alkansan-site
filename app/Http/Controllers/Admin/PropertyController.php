<?php

namespace App\Http\Controllers\Admin;

use App\Models\Property;
use Illuminate\Http\Request;
use App\Http\Requests\PropertyPost;
use App\Http\Controllers\Controller;

use App\Models\Lang;
use App\Models\Setting;
use App\Models\Category;
use Session;
use View;

class PropertyController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct()
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->langs = Lang::all();
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();


        $this->breadcrumb[] =array('page' => "Home",'Link' => route('home'));

        View::share('langs', $this->langs);
        View::share('lang', $this->lang);
        View::share('breadcrumb', $this->breadcrumb);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $properties = Property::where('lang_id',$this->lang->id)->get();

        return view('admin.property.index',compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = Category::where('lang_id',$this->lang->id)->where('parent_id',0)->get();
        return view('admin.property.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PropertyPost $request)
    {
        $result = $request->all();
        $result['lang_id'] = $this->lang->id;
        $result['status'] = 1;
        $result['order'] = 100;

        

        $property = new Property;
        $property->create($result);
        $property = $property->create($result);
        $category_id = $result['category_id'];
        $property->categories->attach($category_id);

        $category = Category::find($category_id);
        $property->categories()->attach($category);

        Session::flash('status', 1);

        return redirect()->route('property.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function edit(Property $property)
    {
        $categories = Category::where('lang_id',$this->lang->id)->where('parent_id',0)->get();
        $categoriesx = $property->categories()->pluck('category_id');
        $selectedcategories = array();
        foreach ($categoriesx as $cat) {
            $selectedcategories[] = $cat;
        }
        //dd($selectedcategories);
        return view('admin.property.edit',compact('property','categories','selectedcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function update(PropertyPost $request, Property $property)
    {
        $result = $request->all();
        $category_id = $result['category_id'];

        $p = $property->update($result);

        $p = Property::find($property->id);

        $p->categories()->attach($category_id);
        
        $property->categories()->detach();
        $category = Category::find($category_id);
        $property->categories()->attach($category);

        Session::flash('status', 1);

        return redirect()->route('property.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property)
    {
        $property->delete();

        return redirect()->route('property.index');
    }

    public function order(Request $request)
    {

        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $property = Property::find($key);
                $property->order = $value;
                $property->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('property.index');
    }
    public function status(Property $property)
    {
        if ($property->status==1) {
            $property->status=0;
        } else {
            $property->status=1;
        }
        $page_id = $property->page_id;
        $property->update();
        Session::flash('status', 1);

        return redirect()->route('property.index');
    }
}
