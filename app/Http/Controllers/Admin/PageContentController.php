<?php

namespace App\Http\Controllers\Admin;

use App\Models\PageContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Team;
use App\Models\Sponsor;
use App\Models\TourGroup;
use App\Models\PageContentType;
use App\Http\Requests\PageContentPost;
use Session;
use View;
use Intervention\Image\ImageManager;


class PageContentController extends Controller
{
    protected $lang;
    protected $breadcrumb;

    public function __construct() 
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->breadcrumb[] =array('page' => "Home",'Link' => route('home'));
        View::share('breadcrumb', $this->breadcrumb);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->type;
        $oid = $request->oid;
        $pagetype = $this->contentlist($type,$oid);
        
        $contentlist = $pagetype->contentsadmin;
        if ($pagetype->toppage) {
            
        
            if ($pagetype->toppages()->count()>0) {
                $this->breadcrumb[] =array('page' => $pagetype->title,'Link' => route('page.index',[null,'page_id'=>$oid]));
            }else{
                $this->breadcrumb[] =array('page' => $pagetype->title,'Link' => route('page.index'));
            }
        }
        View::share('breadcrumb', $this->breadcrumb);
        
        return view('admin.pagecontent.index',compact('contentlist','type','oid'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $type = $request->type;
        $oid = $request->oid;

        $pagecontenttypes = PageContentType::where('status',1)->pluck('name','id')->all();
        $pagecontenttypes[""] = "";
        ksort($pagecontenttypes);
        $page = Page::find($request->oid);
        
        if ($page) {        
            if ($page->toppages()->count()>0) {
                $this->breadcrumb[] =array('page' => $page->title,'Link' => route('page.index',[null,'page_id'=>$oid]));
            }else{
                $this->breadcrumb[] =array('page' => $page->title,'Link' => route('page.index'));
            }
        }
        View::share('breadcrumb', $this->breadcrumb);

        return view('admin.pagecontent.create',compact('pagecontenttypes','type','oid'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageContentPost $request)
    {
        $type = $request->type;
        $oid = $request->oid;
        $content = $this->contentlist($type,$oid);
        
        $insert = array(
            'title' => $request->title,
            'content' => $request->content,
            'html' => $request->html,
            'css' => $request->css,
            'page_content_type_id' => 1,
            'status' => 1,
            'order' => 100,
        );
        //dd($content);
        $content->contents()->create($insert);
       
        Session::flash('status', 1);

        $result['status'] = 200;
        $result['message'] = "";
        return $result;
        //return redirect()->route('pagecontent.index' , [null,'type'=>$type,'oid'=>$oid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageContent  $pageContent
     * @return \Illuminate\Http\Response
     */
    public function show(PageContent $pageContent, Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageContent  $pageContent
     * @return \Illuminate\Http\Response
     */
    public function edit(PageContent $pagecontent, Request $request)
    {
        //
        $type = $request->type;
        $oid = $request->oid;

        $pagecontenttypes = PageContentType::where('status',1)->pluck('name','id')->all(); 
        
        $pagecontenttypes[""] = "";
        ksort($pagecontenttypes);

        $page = Page::find($request->oid);
        if ($page) {        
            if ($page->toppages()->count()>0) {
                $this->breadcrumb[] =array('page' => $page->title,'Link' => route('page.index',[null,'page_id'=>$oid]));
            }else{
                $this->breadcrumb[] =array('page' => $page->title,'Link' => route('page.index'));
            }
        }
        View::share('breadcrumb', $this->breadcrumb);

        return view('admin.pagecontent.edit',compact('pagecontent','pagecontenttypes','type','oid'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageContent  $pageContent
     * @return \Illuminate\Http\Response
     */
    public function update(PageContentPost $request, PageContent $pagecontent)
    {
        //
        $type = $request->type;
        $oid = $request->oid;
        //dd($request);
        $updateobject = array(
            'title' => $request->title,
            'content' => $request->content,
            'html' => $request->html,
            'css' => $request->css,
            'page_content_type_id' => 1,
        );
        
        $pagecontent->update($updateobject);

        Session::flash('status', 1);

        $result['status'] = 200;
        $result['message'] = "";
        return $result;
        //return redirect()->route('pagecontent.index' , [null,'type'=>$type,'oid'=>$oid]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageContent  $pageContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageContent $pagecontent, Request $request)
    {
        $type = $request->type;
        $oid = $request->oid;

        $pagecontent->delete();
        Session::flash('status', 1);
        return redirect()->route('pagecontent.index' , [null,'type'=>$type,'oid'=>$oid]); 
    }
    public function contentlist($type,$id)
    {
        if($type == 1 || $type==4){
            $content = Page::find($id);
        }else if($type == 2){
            $content = Sponsor::find($id);
        }else if($type == 3){
            $content = Team::find($id);
        }else if($type == 5){
            $content = TourGroup::find($id);
        }

        return $content;
    }
    public function order(Request $request)
    {
        $type = $request->type;
        $oid = $request->oid;

        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $pagecontent = PageContent::find($key);
                $pagecontent->order = $value;
                $pagecontent->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('pagecontent.index' , [null,'type'=>$type,'oid'=>$oid]);
    }
    public function status(PageContent $pagecontent, Request $request)
    {
        if ($pagecontent->status==1) {
            $pagecontent->status=0;
        } else {
            $pagecontent->status=1;
        }
        $type = $request->type;
        $oid = $request->oid;

        $pagecontent->update();
        Session::flash('status', 1);

        return redirect()->route('pagecontent.index' , [null,'type'=>$type,'oid'=>$oid]);
    }
    public function contetupload(Request $request)
    {
        if ($request->file()) {
            
            $path = $request->file()['image_param'];
            $coe =$path->getClientOriginalExtension();
            $minetype =$path->getMimeType();
            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            $resize->encode($coe);
            $hash = md5($resize->__toString().time());
            $path_url = "images/content_file_img/{$hash}.".$coe;
            $resize->save(public_path($path_url));
            $url = "/" . $path_url;
            $data['link'] = $url;
           
            return $data;
        }
    }
}
