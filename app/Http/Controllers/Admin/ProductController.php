<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductPost;
use App\Models\Category;
use App\Models\Property;
use App\Models\ProductImage;
use App\Models\ProductProperty;

use App\Http\Requests\ProductImagePost;
use App\Http\Requests\ProductPdfPost;
use App\Http\Requests\ProductPdfFamilyPost;
use Storage;
use Intervention\Image\ImageManager;

use URL;

use App\Models\Lang;
use App\Models\Setting;
use Session;
use View;

class ProductController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;


    public function __construct() 
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->langs = Lang::all();
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();


        $this->breadcrumb[] =array('page' => "Home",'Link' => route('home'));

        View::share('langs', $this->langs);
        View::share('lang', $this->lang);
        View::share('breadcrumb', $this->breadcrumb);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::where('lang_id',$this->lang->id)->get();

        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = Category::where('lang_id',$this->lang->id)->where('parent_id',0)->get();
        return view('admin.product.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductPost $request)
    {
        $result = $request->all();
        $result['lang_id'] = $this->lang->id;
        $result['status'] = 1; 
        $result['order'] = 100;

        $product = new Product;
        $insertproduct = $product->create($result);

        $category_id = $result['category_id'];
        //dd($insertproduct);

        $category = Category::find($category_id);
        $insertproduct->categories()->attach($category);

        if ($request['images']) {
            foreach ($request['images'] as $key => $value) {
                if ($value!="") {
                    $insert = array('product_id' => $insertproduct->id, 'path' => $value);
                    $productImage = new ProductImage;
                    $productImage->create($insert);
                }
            }
        }

        if ($request['property']) {
            foreach ($request['property'] as $key => $value) {
                if ($value!="") {
                    $insert = array(
                        'product_id' => $insertproduct->id, 
                        'property_id' => $key, 
                        'value' => $value
                    );
                    $productProperty = new ProductProperty;
                    $productProperty->create($insert);
                }
            }
        }

        Session::flash('status', 1);

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::where('lang_id',$this->lang->id)->where('parent_id',0)->get();

        return view('admin.product.edit',compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductPost $request, Product $product)
    {
        $result = $request->all();
        $product->update($result);

        Session::flash('status', 1);

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $file= $product->image;
        if($file!=""){
            $filename = public_path().$file;
            \File::delete($filename);
        }
        $product->delete();

        return redirect()->route('product.index');
    }

    public function image_upload(ProductImagePost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_image'];
            $aa = Storage::put('public/product_image', $request->file()['upload_image']);
            $file = Storage::get($aa);
            
            $coe =$path->getClientOriginalExtension();
            $manager = new ImageManager(array('driver' => 'gd'));
            
            $resize = $manager->make($file);
            if($this->setting['product_image']!=""){
                if ($this->setting['product_image']!="0x0") {
                    $ex = explode('x',$this->setting['product_image']);
                    $x = ($ex[0]==0) ? null : $ex[0];
                    $y = ($ex[1]==0) ? null : $ex[1];
                    
                    if ($x==null && $y==mull) {
                        //dd($ex);
                    }else{
                        $resize->resize($x,$y, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    }
                }
            }
            $resize->encode($coe);
            
            $resize->save(storage_path('app/'.$aa));
            $url = Storage::url($aa);
            
            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function image_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $file = str_replace(URL::to('/'),'',$file);
        $file = str_replace('/storage/','public/',$file);
       
        //dd($file);
        if (Storage::delete($file)) {
            return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Image');
        } else {
           
        }
        //$filename = public_path().$file;
        //\File::delete($filename);
    }
    public function order(Request $request)
    {
        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $product = Product::find($key);
                $product->order = $value;
                $product->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('product.index');
    }
    public function status(Product $product)
    {
        if ($product->status==1) {
            $product->status=0;
        } else {
            $product->status=1;
        }
        $parent_id = $product->parent_id;
        $product->update();
        Session::flash('status', 1);

        return redirect()->route('product.index');
    }

    public function pdf_upload(ProductPdfPost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_pdf'];
            $aa = Storage::put('public/product_pdf', $request->file()['upload_pdf']);
            $url = Storage::url($aa);
            
            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function pdf_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $filename = public_path().$file;
        \File::delete($filename);
        return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Image');
    }

    public function property(Request $request)
    {
        $r = $request->all();
        $categories = $r['category_id'];
        
        $categories = Category::whereIn('id',$categories)->with('properties')->where('status',1)->get();
       

        return view('admin.product.property',compact('categories'));
    }
    public function familypdf_upload(ProductPdfFamilyPost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_family_pdf'];
            $aa = Storage::put('public/product_family_pdf', $request->file()['upload_family_pdf']);
            $url = Storage::url($aa);
            
            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    
}
