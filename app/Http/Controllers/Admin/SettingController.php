<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SettingPost;

use App\Models\Lang;

use Session;
use View;

class SettingController extends Controller
{
    public function __construct() 
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->lang = Lang::where('short',\App::getLocale())->first();

        $this->breadcrumb[] =array('page' => "Home",'Link' => 'dashboard');

        View::share('lang', $this->lang);
        View::share('breadcrumb', $this->breadcrumb);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::where('lang_id',$this->lang->id)->get();
        
        return view('dashboard.setting.index',compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.setting.create');   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingPost $request)
    {
        dd($request->all());
        $setting = new Setting;
        $result = $request->all();
        $result['lang_id'] = $this->lang->id;
        $setting->save($result);
        Session::flash('status', 1);

        return redirect()->route('setting.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        return view('dashboard.setting.edit',compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(SettingPost $request, Setting $setting)
    {
        $result = $request->all();

        $setting->update($result);

        Session::flash('status', 1);

        return redirect()->route('setting.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
