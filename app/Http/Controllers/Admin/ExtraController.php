<?php

namespace App\Http\Controllers\Admin;

use App\Models\Extra;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExtraPost;
use Session;

class ExtraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extras = Extra::all();

        return view('admin.extra.index',compact('extras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.extra.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExtraPost $request)
    {
        $r = $request->all();


        $extra = new Extra();

        $r['order']     = 1000;
        $r['status']    = 1;

        $extra->create($r);
        Session::flash('status', 1);

        return redirect()->route('extra.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Extra  $extra
     * @return \Illuminate\Http\Response
     */
    public function show(Extra $extra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Extra  $extra
     * @return \Illuminate\Http\Response
     */
    public function edit(Extra $extra)
    {
        return view('admin.extra.edit',compact('extra'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Extra  $extra
     * @return \Illuminate\Http\Response
     */
    public function update(ExtraPost $request, Extra $extra)
    {
        $r=$request->all();

        $extra->update($r);
        Session::flash('status', 1);

        return redirect()->route('extra.edit',[$extra->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Extra  $extra
     * @return \Illuminate\Http\Response
     */
    public function destroy(Extra $extra)
    {
        $extra->delete();

        return redirect()->route('extra.index');
    }

    public function status(Extra $extra)
    {
        if ($extra->status==1) {
            $extra->status=0;
        } else {
            $extra->status=1;
        }
        $extra->update();
        Session::flash('status', 1);

        return redirect()->route('extra.index');
        
    }
}
