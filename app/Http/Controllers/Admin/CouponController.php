<?php

namespace App\Http\Controllers\Admin;

use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CouponPost;
use Session;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();

        return view('admin.coupon.index',compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $coupon_types= array(
            1 => 'Fiyattan İndirim',
            2 => 'Yüzde İndirim',
        );
        return view('admin.coupon.create',compact('coupon_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponPost $request)
    {
        $r = $request->all();

        $coupon = new Coupon();

        $r['status']    = 1;

        $coupon->create($r);
        Session::flash('status', 1);

        return redirect()->route('admin.coupon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        $coupon_types= array(
            1 => 'Fiyattan İndirim',
            2 => 'Yüzde İndirim',
        );
        return view('admin.coupon.edit',compact('coupon','coupon_types'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(CouponPost $request, Coupon $coupon)
    {
        $r=$request->all();

        $coupon->update($r);
        Session::flash('status', 1);

        return redirect()->route('admin.coupon.edit',[$coupon->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();

        return redirect()->route('admin.coupon.index');
    }

    public function status(Coupon $coupon)
    {
        if ($coupon->status==1) {
            $coupon->status=0;
        } else {
            $coupon->status=1;
        }
        $coupon->update();
        Session::flash('status', 1);

        return redirect()->route('admin.coupon.index');
        
    }
}
