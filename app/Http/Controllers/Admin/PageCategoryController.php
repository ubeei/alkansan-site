<?php

namespace App\Http\Controllers\Admin;

use App\Models\PageCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lang;
use App\Models\Setting;
use Storage;
use App\Models\Page;

use App\Http\Requests\PageCategoryPost;
use App\Http\Requests\PageCategoryImagePost;
use App\Http\Requests\PageCategoryPdfPost;


use Session;
use View;
use Intervention\Image\ImageManager;

class PageCategoryController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct()
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->langs = Lang::all();
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();


        $this->breadcrumb[] =array('page' => "Home",'Link' => route('home'));
        $this->breadcrumb[] =array('page' => "Page",'Link' => route('page.index'));

        View::share('langs', $this->langs);
        View::share('lang', $this->lang);
        View::share('breadcrumb', $this->breadcrumb);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0;
        $page = Page::find($page_id);
        $categories = PageCategory::where('page_id',$page_id)->where('lang_id',$this->lang->id)->get();

        return view('admin.pagecategory.index',compact('categories','page','page_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;

        return view('admin.pagecategory.create',compact('page_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageCategoryPost $request)
    {
        //

        $result = $request->all();
        $result['lang_id'] = $this->lang->id;
        $result['status'] = 1;
        $result['order'] = 100;

        $pagecatgegory = new PageCategory;
        $pagecatgegory->create($result);

        Session::flash('status', 1);

        return redirect()->route('pagecategory.index',[null,'page_id'=>$result['page_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PageCategory $pagecategory,Request $request)
    {
        //
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;
        return view('admin.pagecategory.edit',compact('page_id','pagecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageCategoryPost $request, PageCategory $pagecategory)
    {
        //
        $result = $request->all();

        $pagecategory->update($result);

        Session::flash('status', 1);

        return redirect()->route('pagecategory.index',[null,'page_id'=>$pagecategory->page_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageCategory $pagecategory)
    {
        //
        $page_id = $pagecategory->page_id;
        $file= $pagecategory->image;
        if($file!=""){
            $filename = public_path().$file;
            \File::delete($filename);
        }
        $pagecategory->delete();

        return redirect()->route('pagecategory.index',[null,'page_id'=>$page_id]);
    }
    public function image_upload(PageCategoryImagePost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_image'];
            $aa = Storage::put('public/pagecategory_img', $request->file()['upload_image']);
            $file = Storage::get($aa);

            $coe =$path->getClientOriginalExtension();
            $manager = new ImageManager(array('driver' => 'gd'));

            $resize = $manager->make($file);
            if($this->setting['pagecategory_image']!=""){
                $ex = explode('x',$this->setting['pagecategory_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);


            /* $hash = md5($resize->__toString().time());
            $path_url = "images/pagecategory_img/{$hash}.".$coe; */
            //dd(storage_path($aa));
            $resize->save(storage_path('app/'.$aa));
            $url = Storage::url($aa);

            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function image_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $filename = public_path().$file;
        \File::delete($filename);
        return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Image');
    }
    public function order(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;

        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $pagecategory = PageCategory::find($key);
                $pagecategory->order = $value;
                $pagecategory->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('pagecategory.index',[null,'page_id'=>$page_id]);
    }
    public function status(PageCategory $pagecategory)
    {
        if ($pagecategory->status==1) {
            $pagecategory->status=0;
        } else {
            $pagecategory->status=1;
        }
        $page_id = $pagecategory->page_id;
        $pagecategory->update();
        Session::flash('status', 1);

        return redirect()->route('pagecategory.index',[null,'page_id'=>$page_id]);
    }

    public function pdf_upload(PageCategoryPdfPost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_pdf'];
            $aa = Storage::put('public/pagecategory_pdf', $request->file()['upload_pdf']);
            $url = Storage::url($aa);

            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function pdf_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $filename = public_path().$file;
        \File::delete($filename);
        return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Image');
    }
}
