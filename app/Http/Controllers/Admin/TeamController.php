<?php

namespace App\Http\Controllers\Admin;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lang;
use App\Models\Page;
use View;
use App\Http\Requests\TeamPost;
use App\Http\Requests\TeamImagePost;
use Intervention\Image\ImageManager;
use Session;
use App\Models\Setting;




class TeamController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct() 
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();


        $this->breadcrumb[] =array('page' => "Home",'Link' => 'home');

        View::share('breadcrumb', $this->breadcrumb);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $page_id = (isset($request->page_id)) ? $request->page_id : 0;
        $page = Page::find($page_id);
        $teams = Team::where('page_id',$page_id)->where('lang_id',$this->lang->id)->get();

        return view('admin.team.index',compact('teams','page','page_id'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;
        
        
        return view('admin.team.create',compact('page_id'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamPost $request)
    {
        //
        $result = $request->all();
        $result['lang_id'] = $this->lang->id;
        $result['status'] = 1; 
        $result['order'] = 100;

        $team = new Team;
        $team->create($result);

        Session::flash('status', 1);

        return redirect()->route('team.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team,Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;
        return view('admin.team.edit',compact('page_id','team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(TeamPost $request, Team $team)
    {
        //
        $result = $request->all();

        $team->update($result);

        Session::flash('status', 1);

        return redirect()->route('team.index',[null,'page_id'=>$team->page_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        //
        $page_id = $team->page_id;
        $file= $team->image;
        if($file!=""){
            $filename = public_path().$file;
            \File::delete($filename);
        }
        $team->delete();
        

        return redirect()->route('team.index',[null,'page_id'=>$page_id]);
    }

    public function image_upload(TeamImagePost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_image'];
            $coe =$path->getClientOriginalExtension();
            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            if($this->setting['team_image']!=""){
                $ex = explode('x',$this->setting['team_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y,function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            $hash = md5($resize->__toString().time());
            $path_url = "images/team_img/{$hash}.".$coe;
            $resize->save(public_path($path_url));
            $url = "/" . $path_url;
            
            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function image_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $filename = public_path().$file;
        \File::delete($filename);
        return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Image');
    }
    public function order(Request $request)
    {
        $page_id = (isset($request->page_id)) ? $request->page_id : 0 ;

        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $team = Team::find($key);
                $team->order = $value;
                $team->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('team.index',[null,'page_id'=>$page_id]);
    }
    public function status(Team $team)
    {
        if ($team->status==1) {
            $team->status=0;
        } else {
            $team->status=1;
        }
        $page_id = $team->page_id;
        $team->update();
        Session::flash('status', 1);

        return redirect()->route('team.index',[null,'page_id'=>$page_id]);
        
    }
}
