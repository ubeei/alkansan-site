<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lang;
use App\Models\Setting;
use Storage;

use App\Http\Requests\CategoryPost;
use App\Http\Requests\CategoryImagePost;
use App\Http\Requests\CategoryPdfPost;


use Session;
use View;
use Intervention\Image\ImageManager;

class CategoryController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct() 
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->langs = Lang::all();
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();


        $this->breadcrumb[] =array('page' => "Home",'Link' => route('home'));
        $this->breadcrumb[] =array('page' => "Page",'Link' => route('page.index'));

        View::share('langs', $this->langs);
        View::share('lang', $this->lang);
        View::share('breadcrumb', $this->breadcrumb);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $parent_id = (isset($request->parent_id)) ? $request->parent_id : 0;
        $categories = Category::where('lang_id',$this->lang->id)->get();

        return view('admin.category.index',compact('categories','parent_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $parent_id = (isset($request->parent_id)) ? $request->parent_id : 0 ;
        $categories = Category::where('lang_id',$this->lang->id)->where('parent_id',0)->get();


        return view('admin.category.create',compact('parent_id','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryPost $request)
    {
        //

        $result = $request->all();
        $result['lang_id'] = $this->lang->id;
        $result['status'] = 1; 
        $result['order'] = 100;
        $result['parent_id'] = ($result['parent_id']) ? $result['parent_id'] : 0 ;
        
        $catgegory = new Category;
        $a = $catgegory->create($result);
        Session::flash('status', 1);

        return redirect()->route('category.index',[null,'parent_id'=>$result['parent_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category,Request $request)
    {
        //
        $parent_id = (isset($request->parent_id)) ? $request->parent_id : 0 ;
        $categories = Category::where('lang_id',$this->lang->id)->where('parent_id',0)->get();

        return view('admin.category.edit',compact('parent_id','category','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryPost $request, Category $category)
    {
        //
        $result = $request->all();
        $category->update($result);

        Session::flash('status', 1);

        return redirect()->route('category.index',[null,'parent_id'=>$category->parent_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        $parent_id = $category->parent_id;
        $file= $category->image;
        if($file!=""){
            $filename = public_path().$file;
            \File::delete($filename);
        }
        $category->delete();

        return redirect()->route('category.index',[null,'parent_id'=>$parent_id]);
    }
    public function image_upload(CategoryImagePost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_image'];
            $aa = Storage::put('public/category_img', $request->file()['upload_image']);
            $file = Storage::get($aa);
            
            $coe =$path->getClientOriginalExtension();
            $manager = new ImageManager(array('driver' => 'gd'));
            
            $resize = $manager->make($file);
            if($this->setting['category_image']!=""){
                $ex = explode('x',$this->setting['category_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);
            
            $resize->save(storage_path('app/'.$aa));
            $url = Storage::url($aa);
            
            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function image_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $filename = public_path().$file;
        \File::delete($filename);
        return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Image');
    }
    public function order(Request $request)
    {
        $parent_id = (isset($request->parent_id)) ? $request->parent_id : 0 ;

        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $category = Category::find($key);
                $category->order = $value;
                $category->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('category.index',[null,'parent_id'=>$parent_id]);
    }
    public function status(Category $category)
    {
        if ($category->status==1) {
            $category->status=0;
        } else {
            $category->status=1;
        }
        $parent_id = $category->parent_id;
        $category->update();
        Session::flash('status', 1);

        return redirect()->route('category.index',[null,'parent_id'=>$parent_id]);
    }

    public function pdf_upload(CategoryPdfPost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_pdf'];
            $aa = Storage::put('public/category_pdf', $request->file()['upload_pdf']);
            $url = Storage::url($aa);
            
            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function pdf_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $filename = public_path().$file;
        \File::delete($filename);
        return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Image');
    }
}
