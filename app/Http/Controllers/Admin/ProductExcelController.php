<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Property;
use App\Models\ProductImage;
use App\Models\ProductProperty;

use App\Http\Requests\ProductExcelUploadPost;
use App\Http\Requests\ProductExcelPost;

use App\Models\Lang;
use App\Models\Setting;
use Session;
use View;
use Storage;

use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;


class ProductExcelController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct() 
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->langs = Lang::all();
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();


        $this->breadcrumb[] =array('page' => "Home",'Link' => route('home'));

        View::share('langs', $this->langs);
        View::share('lang', $this->lang);
        View::share('breadcrumb', $this->breadcrumb);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::where('lang_id',$this->lang->id)->where('parent_id',0)->get();

        return view('admin.product_excel.index',compact('categories'));
    }

    public function store(ProductExcelPost $request)
    {
        $result = $request->all();
        $r= $request->all();
        $p = str_replace('storage/','app/public/',$r['excel']);
        $path = storage_path($p);
        $productimport = new ProductImport;
        $productimport->categories = $r['category_id'];
        $productimport->lang = $this->lang;
        Excel::import($productimport, $path);

        Session::flash('status', 1);

        return redirect()->route('productexcel.index');
    }

    public function excel_upload(ProductExcelUploadPost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_excel'];
            $aa = Storage::put('public/product_excel', $request->file()['upload_excel']);
            $url = Storage::url($aa);
            
            $url;
            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
    public function excel_remove(Request $request)
    {
        //dd($request->img);
        $file= $request->img;
        $filename = public_path().$file;
        \File::delete($filename);
        return array('error'=>0 ,'url'=>'https://dummyimage.com/200x200/b3b1b3/000000.png&text=Excel');
    }
}
