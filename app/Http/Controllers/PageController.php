<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Page;
use App\Models\Lang;
use View;
use App\Models\Slider;
use App\Models\Setting;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Http\Requests\ContactFormPost;
use Validator;
use Mail;
use App\Models\Blog;
use Vyuldashev\XmlToArray\XmlToArray;
use Illuminate\Support\Facades\Cache;
use Auth;
use App\Models\PageCategoryDetail;
use Illuminate\Contracts\Routing\ResponseFactory;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ContactMail;
use Illuminate\Support\Facades\Redirect;
use File;
use App\Models\Time;
use Carbon\Carbon;


class PageController extends Controller
{
    //
    protected $langs;
    protected $lang;
    protected $breadcrumb;
    protected $pages;
    protected $title;
    protected $meta_description;
    protected $meta_tags;
    protected $activemenu = array();
    protected $settings;
    protected $currencies;
    protected $menucategories;




    public function __construct()
    {
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->langs = Lang::where('status',1)->get();
        $this->title = env('APP_NAME');
        $this->meta_description = env('META_DESCRIPTION');
        $this->meta_tags = env('META_TAGS');
        $this->homepage = Page::where('homepage',1)->where('lang_id',$this->lang->id)->first();

        $this->topmenus = Page::where('topmenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();
        $this->bottommenus = Page::where('submenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();

        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();
        $this->menucategories = Category::where('lang_id',$this->lang->id)->where('status',1)->where('parent_id',0)->orderBy('order')->get();

        View::share('topmenus', $this->topmenus);
        View::share('bottommenus', $this->bottommenus);
        View::share('homepage', $this->homepage);
        View::share('setting', $this->setting);
        View::share('menucategories', $this->menucategories);

    }
    public function home()
    {
        return $this->page_home($this->homepage);
    }
    public function page(Request $request)
    {
        if($request->id!="" && $request->id!=0){
            $page = Page::find($request->id);
            if ($page) {

                //Home Page
                if($page->homepage==1){
                   return $this->page_home($page);
                //Standart Page
                }else if($page->page_type_id == 1){
                    return $this->page_standart($page);
                //Sponsored Page
                }else if($page->page_type_id == 2){
                    return $this->page_sponsor($page);
                //Team Page
                }else if($page->page_type_id == 3){
                    return $this->page_team($page);
                //Contact Page
                }else if($page->page_type_id == 4){
                    return $this->page_contact($page);
                }else if($page->page_type_id == 5){
                    return $this->page_tourgroup($page);
                }else if($page->page_type_id == 6){
                    return $this->page_tour($page,$request);
                }else if($page->page_type_id == 7){
                    return $this->tour_category($page, $request);
                }else if($page->page_type_id == 8){
                    return $this->page_category_dfp($page, $request);
                  }else if($page->page_type_id == 9){
                      return $this->page_category_dfp_detail($page, $request);
                }else if($page->page_type_id == 10){
                    return $this->page_daily_tour($page, $request);
                  }else if($page->page_type_id == 12){
                      return $this->page_blog($page, $request);
                    }else if($page->page_type_id == 13){
                        return $this->page_about($page, $request);
                    }
                else{
                    return abort(404);
                }

            } else {
                return abort(404);
            }
        }else{
            return abort(404);
        }
    }
    public function pagedetail(Request $request)
    {
        if($request->id!="" && $request->id!=0 && $request->id2!="" && $request->id2!=0){
            $page = Page::find($request->id);
            if ($page) {

                if($page->page_type_id == 6 || $page->page_type_id == 10){

                    $params['tour_id'] = $request->id2;
                    $tour = req_get('detail',$params);
                    //dd($tour);
                    return $this->page_tour_detail($page,$tour,$request,$params);
                }else if($page->page_type_id == 8)
                {
                    $pagecategory = $page->pagecategories()->find($request->id2);
                    //dd($pagecategory->pagecategorydetails);

                    return $this->page_category_dfp_detail($page,$pagecategory,$request);
                }
                else{
                    return abort(404);
                }

            } else {
                return abort(404);
            }
        }else{
            return abort(404);
        }
    }
    public function pagepost(Request $request)
    {
        if($request->id!="" && $request->id!=0){
            $page = Page::find($request->id);
            if ($page) {
                $r=$request->all();

                if($r['form_type']=="contact_form"){
                    return $this->contact_form($page,$request);
                }else {
                    return abort(404);
                }

            } else {
                return abort(404);
            }
        }else{
            return abort(404);
        }
    }
    public function page_blog($page)
    {
//         View::share('title', $page->title.' - '.$this->title);
//         View::share('meta_description', $page->meta_description);
//         View::share('meta_tags', $page->meta_tags);
//         $pageitem = $page;
//         $this->pagetopactive($pageitem);
//
//         $blogs = Blog::where('status',1)->where('lang_id', $this->lang);
//         View::share('activemenu', $this->activemenu);
//
//         $template = template_check();
//
//         $page_path = '/pages/pageblog/list';          // kontrol edilecek dosya uzantısı
//         $files = template_path_check($page_path);           // helpera kontrol için giden dosya
// dd($blogs);
        // return view($files, compact('page','blogs'));
        return redirect()->route('site.blog.index');
    }
    protected function page_home($page)
    {
        //dd($page);

        $template = template_check();

        $tourpage = Page::where('lang_id',$this->lang->id)->where('page_type_id',6)->first();


        View::share('title', env('APP_NAME',$page->title.' - '.$this->title));
        View::share('meta_description', env('META_DESCRIPTION',$page->meta_description));
        View::share('meta_tags', env('META_TAGS',$page->meta_tags));
        View::share('activemenu', $this->activemenu);

        $sliders = Slider::where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();
        $categories = Category::where('lang_id',$this->lang->id)->where('parent_id',0)->where('status',1)->orderBy('order')->get();

        $page_path = '/pages/homepage/content';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya


        return view($files, compact('page','sliders','categories'));

    }
    protected function page_standart($page)
    {
        View::share('title', $page->title.' - '.$this->title);
        View::share('meta_description', $page->meta_description);
        View::share('meta_tags', $page->meta_tags);
        $pageitem = $page;
        $this->pagetopactive($pageitem);

        View::share('activemenu', $this->activemenu);

        $template = template_check();

        $page_path = '/pages/pagestandart/content';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya

        return view($files, compact('page'));
    }

    protected function page_team($page)
    {
        View::share('title', $page->title.' - '.$this->title);
        View::share('meta_description', $page->meta_description);
        View::share('meta_tags', $page->meta_tags);
        $pageitem = $page;
        $this->pagetopactive($pageitem);

        View::share('activemenu', $this->activemenu);
        $teams = $page->teams;
        $template = template_check();

        $page_path = '/pages/pageteam/content';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya
        //dd($page_path);
        return view($files, compact('page','teams'));
    }

    protected function page_sponsor($page)
    {
        View::share('title', $page->title.' - '.$this->title);
        View::share('meta_description', $page->meta_description);
        View::share('meta_tags', $page->meta_tags);
        $pageitem = $page;
        $this->pagetopactive($pageitem);

        View::share('activemenu', $this->activemenu);
        $sponsors = $page->sponsors;
        $template = template_check();



        $page_path = '/pages/pagesponsor/content';          // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya

        return view($files, compact('page','sponsors'));
    }
    public function page_contact()
    {
        // View::share('title', $page->title.' - '.$this->title);
        // View::share('meta_description', $page->meta_description);
        // View::share('meta_tags', $page->meta_tags);
        // $pageitem = $page;
        // $this->pagetopactive($pageitem);
        //
        // View::share('activemenu', $this->activemenu);
        //
        // $template = template_check();
        //
        // $page_path = '/pages/pagecontact/content';          // kontrol edilecek dosya uzantısı
        // $files = template_path_check($page_path);           // helpera kontrol için giden dosya
        //
        // return view($files, compact('page'));
        return redirect()->route('contact');
    }
    public function page_about()
    {

        return redirect()->route('about');
    }
    protected function pagetopactive($page)
    {
        if($page->toppages){
            $this->pagetopactive($page->toppages);
            $this->activemenu[] = $page->page_id;
        }
    }
    protected function contact_form($page,$request)
    {
        //dd($request->all());
        $post = new ContactFormPost;
        //$request = new MyRequest();
        $rules = $post->rules();
        $attributes = $post->attributes();
        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($attributes);
        //dd($validator->fails());
        if ($validator->fails()) {
            return redirect()->route('a.page',['name'=>str_slug($page->title),'id'=>$page->id])
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $r = $request->all();

            $data = array(
                'namesurname'   => $r['namesurname'],
                'phone'         => $r['phone'],
                'email'         => $r['email'],
                'massage'       => $r['massage'],
                'lang'          => $this->lang->name,
                'subject'       => env('APP_NAME')." - Contact Form",
            );
            $email =env('SYSTEM_MAIL');
            Notification::route('mail', $email)->notify(new ContactMail($data));
            /*$send = Mail::send('emails.contact', $data , function ($message)
            {
                $message->subject(env('APP_NAME')." - Contact Form");
                $message->to(env('SYSTEM_MAIL'));
            });*/
            Session::flash('form_success');
            return redirect()->route('a.page',['name'=>str_slug($page->title),'id'=>$page->id]);
        }
        //$this->assertEquals(false, $fails);
    }
    protected function page_tourgroup($page)
    {
        $tourpage = Page::where('lang_id',$this->lang->id)->where('page_type_id',6)->first();
        //dd($page);
        View::share('title', $page->title.' - '.$this->title);
        View::share('meta_description', $page->meta_description);
        View::share('meta_tags', $page->meta_tags);
        $pageitem = $page;
        $this->pagetopactive($pageitem);

        View::share('activemenu', $this->activemenu);
        $tourapi = array();
        foreach ($page->tourgroups as $touritem) {
            $tourapi = array_merge(json_decode($touritem->lists),$tourapi);
        }
        $tourgroups = $tourapi;

        for($i=0; $i<count($tourgroups); $i++)
        {
            $params['tour_id'] = $tourgroups[$i];
            $tour_details[$i] = req_get('detail',$params);
        }
        $tour_detail = array();
        for($i=0; $i<count($tour_details); $i++)
        {
            if ($tour_details[$i]->success == true) {
                $tour_detail[$i]['ID']                  = $tour_details[$i]->tourInfo->ID;
                $tour_detail[$i]['Name']                = $tour_details[$i]->tourInfo->Name;
                $tour_detail[$i]['Discount_Flat']       = $tour_details[$i]->tourInfo->Discount_Flat;
                $tour_detail[$i]['Duration']            = $tour_details[$i]->tourInfo->Duration;
                $tour_detail[$i]['Highlights']          = $tour_details[$i]->tourInfo->Highlights;
                $tour_detail[$i]['Price_Default']       = $tour_details[$i]->tourInfo->Price_Default;
                $tour_detail[$i]['Tour_Route']          = $tour_details[$i]->tourInfo->Tour_Route;
                $tour_detail[$i]['Tour_Start_City']     = $tour_details[$i]->tourInfo->Tour_Start_City;
                $tour_detail[$i]['Tour_End_City']       = $tour_details[$i]->tourInfo->Tour_End_City;
                $tour_detail[$i]['Image_Long']          = $tour_details[$i]->tourInfo->Image_Long;
                $tour_detail[$i]['Invoice_Currency']    = $tour_details[$i]->tourInfo->Invoice_Currency;
                $tour_detail[$i]['_config_data_']       = $tour_details[$i]->_config_data_;
            }
        }

        $template = template_check();
        //return view($template->template_path.'.pages.pagetourgroup.content', compact('page','tour_detail','tourpage'));

        $page_path = '/pages/pagetourgroup/content';          // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya

        return view($files, compact('page','tour_detail','tourpage'));
    }
    protected function page_tour($page,$request)
    {

        View::share('title', $page->title.' - '.$this->title);
        View::share('meta_description', $page->meta_description);
        View::share('meta_tags', $page->meta_tags);
        $pageitem = $page;
        $this->pagetopactive($pageitem);

        View::share('activemenu', $this->activemenu);
        $get = $request->all();

        $pageurl ="";
        if (isset($get['tour_name_like']) && $get['tour_name_like']!="") {
            $pageurl.='tour_name_like='.$get['tour_name_like'].'&';
        }else{
            $get['tour_name_like'] = "";
        }
        if (isset($get['continent_id']) && $get['continent_id']!="") {
            $pageurl.='continent_id='.$get['continent_id'].'&';
        }else{
            $get['continent_id'] = "";
        }
        if (isset($get['country_id']) && $get['country_id']!="") {
            $pageurl.='country_id='.$get['country_id'].'&';
        }else{
            $get['country_id'] = "";
        }
        if (isset($get['style_id']) && $get['style_id']!="") {
            $pageurl.='style_id='.$get['style_id'].'&';
        }else{
            $get['style_id'] = "";
        }
        if (isset($get['duration']) && $get['duration']!="") {
            $pageurl.='duration='.$get['duration'].'&';
        }else{
            $get['duration'] = "";
        }
        if (isset($get['budget']) && $get['budget']!="") {
            $pageurl.='budget='.$get['budget'].'&';
        }else{
            $get['budget'] = "";
        }
        if (isset($get['offset']) && $get['offset']!="") {
            //$url.='offset='.$get['offset'].'&';
        }else{
            $get['offset'] = 0;
        }
        if (isset($get['limit']) && $get['limit']!="") {
            //$url.='limit='.$get['limit'].'&';
        }else{
            $get['limit'] = 12;
        }
        $get['city_name'] = "";

        $tours =  req_get('search', $get);
        /*$tours = Tour::where('name', 'like', '%'.$get['tour_name_like'].'%')
        ->where('continent_id',$get['continent_id'])
        ->where('tour_category_id',$get['country_id'])
        ->get();
        dd($tours);*/
        //dd($tours);


        $number_of_pages = ceil($tours->count/$get['limit']);


        if (Cache::has(env('CACHE_ID').'list')){
            $list =  Cache::get(env('CACHE_ID').'list');
            //dd($imgurl);
        } else {
            $list = req_get('lists');
            Cache::put(env('CACHE_ID').'list', $list, (60*24*1));
        }
        //dd($list);
        $continents = TourContiment::all();
        if ($continents) {

            foreach ($continents as $continent) {
                $tourfilter['contiment_id'][$continent->id] = $continent->name;
            }
        }else{
            $tourfilter['contiment_id'] = array();
        }
        $countries = TourCountry::all();
        if ($countries) {
            foreach ($countries as $country) {
                $tourfilter['countries'][$country->id] = $country->name;
            }
        }else{
            $tourfilter['countries'] = array();
        }
        $durations = TourDuration::all();
        if ($durations) {
            foreach ($durations as $duration) {
                $tourfilter['duration'][$duration->id] = $duration->duration_type;
            }
        }else{
            $tourfilter['duration'] = array();
        }
        //dd($tourfilter);
        $tourfilter['budget'] = budgetlist();

        /*if ($get['continent_id']!="") {
            $tourfilter['countries'] = TourCountry::where('tour_continent_id',$get['continent_id'])->pluck("name","id");
        }else{
            $tourcountries = TourCountry::pluck("name","id");
            $tourfilter['countries'] = $tourcountries;
        }
        $tourfilter['duration'] = TourDuration::pluck("name","duration_type");*/
        if ($get['country_id']!="") {
            $params['country_id'] = $get['country_id'];
            $style = req_get('get_cities_subcategories_by_country',$params);
            if ($style->success) {
                if ($style->styles) {
                    foreach ($style->styles as $styleitem) {
                        $tourfilter['style'][$styleitem->ID] = $styleitem->Name;
                    }
                } else {
                    $tourfilter['style'][] = "";
                }


            }else{
                $tourfilter['style'][] = "";
            }
        }else{
            $tourfilter['style'][] = "";
        }

        $tourfilter['budget'] = budgetlist();

        $tourfilter['get'] = $get;


        //$tours['item']->paginate(15);
        //return view('pages.pagetour.content', compact('page','tours','tourfilter'));
        $template = template_check();
        //return view($template->template_path.'.pages.pagetour.content', compact('page','tours','tourfilter', 'number_of_pages','pageurl'));

        $page_path = '/pages/pagetour/content';          // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya

        return view($files, compact('page','tours','tourfilter', 'number_of_pages','pageurl'));

    }
    protected function page_tour_detail($page,$tour,$request,$params)
    {
        if($tour){

            View::share('title', $page->title.' - '.$tour->tourInfo->Title_SEO);
            View::share('meta_description', $tour->tourInfo->Description_SEO);
            View::share('meta_tags', $tour->tourInfo->Keywords_SEO);
            $pageitem = $page;
            $this->pagetopactive($pageitem);

            View::share('activemenu', $this->activemenu);

            $coordinates = $tour->tourInfo->Coordinates;
            if ($coordinates!="") {
                $coordinates = str_replace("{","[",$coordinates);
                    $coordinates = str_replace("}","]",$coordinates);
                    $coordinates = str_replace('"lat":','',$coordinates);
                    $coordinates = str_replace('"lng":','',$coordinates);
            }
            /*if (!isset($tour['tourGalleryItems'])) {
                $tour['tourGalleryItems']=null;
            }*/


            $template = template_check();
            //return view($template->template_path.'.pages.pagetour.detail', compact('page','tour','coordinates'));


            $page_path = '/pages/pagetour/detail';          // kontrol edilecek dosya uzantısı
            $files = template_path_check($page_path);           // helpera kontrol için giden dosya

            return view($files, compact('page','tour','coordinates'));

        }else{
            //dd($request->session()->has('toursendmail'.$params['tour_id']));
            if ($request->session()->has('toursendmail'.$params['tour_id'])==false) {

                $tourmessage = array(
                    'subject' => env('APP_NAME')." - Tour No Info ",
                    'tour_id' => $params['tour_id'],
                    'email' => env('SYSTEM_MAIL'),
                    'from_name'  => env('MAIL_FROM_NAME'),
                );
                $email ="software@travelshopturkey.com";
                $notourMail = new NoTourMail($tourmessage);
                Notification::route('mail', $email)->notify($notourMail);
                $email ="web@travelshopturkey.com";
                $notourMail = new NoTourMail($tourmessage);
                Notification::route('mail', $email)->notify($notourMail);
                //Notification::send($user, new NoTourMail($tourmessage));
                activity()->log($params['tour_id'].' ID tour not available.');
                $request->session()->put('toursendmail'.$params['tour_id'], 1);
            }
            return Redirect::back()->withErrors(["This page can't be reached. Please try again later."]);

        }

    }

    public function tour_category($page, $request)
    {
        // $req = $request->all();
        // //dd($req);
        // View::share('title', $page->title.' - '.$this->title);
        // View::share('meta_description', $page->meta_description);
        // View::share('meta_tags', $page->meta_tags);
        // $pageitem = $page;
        // $this->pagetopactive($pageitem);
        //
        // View::share('activemenu', $this->activemenu);
        //
        // $product = Product::where('id',$request->id)->where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->first();
        // $images = ProductImage::where('product_id',$request->id)->get();
        //
        //
        //
        // $template = template_check();
        //
        // $page_path = '/pages/pagecategory/list';          // kontrol edilecek dosya uzantısı
        // $files = template_path_check($page_path);              // helpera kontrol için giden dosya
        //
        // return view($files, compact('page','product','images'));
        return redirect(route('a.categories'));
    }

    public function page_category_dfp($page, $request)
    {


        // View::share('title', $page->title.' - '.$this->title);
        // View::share('meta_description', $page->meta_description);
        // View::share('meta_tags', $page->meta_tags);
        // $pageitem = $page;
        // $this->pagetopactive($pageitem);
        //
        //
        // View::share('activemenu', $this->activemenu);
        // $pagecategories = $page->pagecategories;
        //
        // $template = template_check();
        // //return view($template->template_path.'.pages.pagecategorypdf.content', compact('page','pagecategories'));
        //
        // if($request->id){
        //
        //   $page_path = '/pages/pagecategory/category';          // kontrol edilecek dosya uzantısı
        //   $files = template_path_check($page_path);           // helpera kontrol için giden dosya
        //
        //
        //   $categories = Category::where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();
        //   $products = Product::where('lang_id',$this->lang->id)->where('cat_id',$request->id)->where('status',1)->orderBy('order')->get();
        //   $images = array();
        //   foreach($products as $product){
        //     $images[] = ProductImage::where('product_id',$request->id)->first();
        //   }
        //
        //   // $images = ProductImage::all();
        //
        //   return view($files, compact('page','categories','product','images'));
        //
        //
        // }else{
        //
        //
        // $page_path = '/pages/pagecategory/list';          // kontrol edilecek dosya uzantısı
        // $files = template_path_check($page_path);           // helpera kontrol için giden dosya
        //
        //
        // $categories = Category::where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();
        // $products = Product::where('lang_id',$this->lang->id)->where('status',1)->orderBy('order')->get();
        // $images = ProductImage::all();
        //
        //
        //
        //
        //
        //
        // return view($files, compact('page','pagecategories','categories','products','images'));

      // }
      return redirect(route('categories'));
    }
    public function page_category_dfp_detail(Request $request)
    {

      //
      //
      // $page = Page::find(7);
      //   //dd($pagecategory);
      //   View::share('title', $page->title.' - '.$this->title);
      //   View::share('meta_description', $page->meta_description);
      //   View::share('meta_tags', $page->meta_tags);
      //
      //   $pageitem = $page;
      //   $this->pagetopactive($pageitem);
      //
      //   View::share('activemenu', $this->activemenu);
      //   //$pagecategories = $page->pagecategories;
      //
      //   $template = template_check();
      //   //return view($template->template_path.'.pages.pagecategorypdf.detail', compact('page','pagecategory'));
      //
      //   $page_path = '/pages/pagecategory/content';          // kontrol edilecek dosya uzantısı
      //   $files = template_path_check($page_path);           // helpera kontrol için giden dosya
      //
      //
      //
      //
      //   $category = Category::where('id',$request->id)->where('lang_id',$this->lang->id)->where('status',1)->first();
      //   $products = Product::where('lang_id',$this->lang->id)->where('cat_id',$request->id)->where('status',1)->orderBy('order')->get();
      //   $images = ProductImage::all();
      //
      //
      //
      //   return view($files, compact('category','images','products'));
      return redirect(route('a.categories'));
    }
    public function page_category_dfp_download(Request $request)
    {
        $category_dfp = $request->all();
        $download = PageCategoryDetail::find($category_dfp['id']);
        //dd($download);
        $path = str_limit(str_slug($download->title),50).'.pdf';
        header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$path");
		header("Content-Type: application/zip");
		header("Content-Transfer-Encoding: binary");

		// read the file from disk
		readfile(public_path() .$download->pdf);
        //return response()->file(public_path() .$download->pdf, $headers);
    }

    public function page_daily_tour($page, $request)
    {
        View::share('title', $page->title.' - '.$this->title);
        View::share('meta_description', $page->meta_description);
        View::share('meta_tags', $page->meta_tags);
        $pageitem = $page;
        $this->pagetopactive($pageitem);

        View::share('activemenu', $this->activemenu);

        $get = $request->all();

        $pageurl ="";
        if (isset($get['tour_name_like']) && $get['tour_name_like']!="") {
            $pageurl.='tour_name_like='.$get['tour_name_like'].'&';
        }else{
            $get['tour_name_like'] = "";
        }
        if (isset($get['continent_id']) && $get['continent_id']!="") {
            $pageurl.='continent_id='.$get['continent_id'].'&';
        }else{
            $get['continent_id'] = "";
        }
        if (isset($get['country_id']) && $get['country_id']!="") {
            $pageurl.='country_id='.$get['country_id'].'&';
        }else{
            $get['country_id'] = "";
        }
        if (isset($get['style_id']) && $get['style_id']!="") {
            $pageurl.='style_id='.$get['style_id'].'&';
        }else{
            $get['style_id'] = "";
        }
        if (isset($get['duration']) && $get['duration']!="") {
            $pageurl.='duration='.$get['duration'].'&';
        }else{
            $get['duration'] = "";
        }
        if (isset($get['budget']) && $get['budget']!="") {
            $pageurl.='budget='.$get['budget'].'&';
        }else{
            $get['budget'] = "";
        }
        if (isset($get['offset']) && $get['offset']!="") {
            //$url.='offset='.$get['offset'].'&';
        }else{
            $get['offset'] = 0;
        }
        if (isset($get['limit']) && $get['limit']!="") {
            //$url.='limit='.$get['limit'].'&';
        }else{
            $get['limit'] = 12;
        }
        $get['city_name'] = "";
        $get['daily_tours'] = 1;
        //dd($get);
        $tours =  req_get('search', $get);
        //dd($tours);

        $number_of_pages = ceil($tours->count/$get['limit']);

        if (Cache::has(env('CACHE_ID').'list')){
            $list =  Cache::get(env('CACHE_ID').'list');
            //dd($imgurl);
        } else {
            $list = req_get('lists');
            Cache::put(env('CACHE_ID').'list', $list, (60*24*1));
        }
        //dd($list);

        foreach ($list->continents as $continent) {
            $tourfilter['contiment_id'][$continent->ID] = $continent->Name;
        }
        foreach ($list->countries as $country) {
            $tourfilter['countries'][$country->ID] = $country->Name;
        }
        //dd($list->durations);
        foreach ($list->durations as $duration) {
            $tourfilter['duration'][$duration->id] = $duration->duration_type;
        }

        /*if ($get['continent_id']!="") {
            $tourfilter['countries'] = TourCountry::where('tour_continent_id',$get['continent_id'])->pluck("name","id");
        }else{
            $tourcountries = TourCountry::pluck("name","id");
            $tourfilter['countries'] = $tourcountries;
        }
        $tourfilter['duration'] = TourDuration::pluck("name","duration_type");*/
        if ($get['country_id']!="") {
            $params['country_id'] = $get['country_id'];
            $style = req_get('get_cities_subcategories_by_country',$params);

            if ($style->success) {
                foreach ($style->styles as $styleitem) {
                    $tourfilter['style'][$styleitem->ID] = $styleitem->Name;
                }
            }
        }else{
            $tourfilter['style'][] = "";
        }

        $tourfilter['budget'] = budgetlist();

        $tourfilter['get'] = $get;

        //return view('pages.pagedailytour.content', compact('page','tours','tourfilter', 'number_of_pages'));

        $template = template_check();
        $page_path = '/pages/pagedailytour/content';          // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya

        return view($files, compact('page','tours','tourfilter', 'number_of_pages'));
    }

}
