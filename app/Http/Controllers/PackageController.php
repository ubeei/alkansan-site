<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Package;
use App\Models\Lang;
use App\Models\Page;
use App\Models\Setting;
use View;
use Session;
use Auth;
use App\Models\Payment;
use App\Notifications\PackageNotification;


class PackageController extends Controller
{
    protected $langs;
    protected $lang;
    protected $breadcrumb;
    protected $pages;
    protected $title;
    protected $meta_description;
    protected $meta_tags;
    protected $activemenu = array();
    protected $settings;
    protected $currencies;
    protected $homepage;

    public function __construct() 
    {
        $this->middleware('auth');
        
       
        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->langs = Lang::where('status',1)->get();
        $this->title = env('APP_NAME');
        $this->meta_description = env('META_DESCRIPTION');
        $this->meta_tags = env('META_TAGS');
        $this->homepage = Page::where('homepage',1)->where('lang_id',$this->lang->id)->first();
        
        $this->topmenus = Page::where('topmenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();
        $this->bottommenus = Page::where('submenu',1)->where('lang_id',$this->lang->id)->where('status',1)->where('page_id',0)->orderBy('order')->get();

        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();

        

        View::share('topmenus', $this->topmenus);
        View::share('bottommenus', $this->bottommenus);
        View::share('homepage', $this->homepage);
        View::share('setting', $this->setting);

    }

    public function index(Package $package)
    {
        $totalprice = $package->discount_price*$package->total_use;
        $page_path = '/payment/index';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);           // helpera kontrol için giden dosya
        $code = strtoupper(substr(bin2hex(openssl_random_pseudo_bytes(20)), 0, 10));
        
        //dd(Session::all());
        $options = new \Iyzipay\Options();
        /* $options->setApiKey("sandbox-afXhZPW0MQlE4dCUUlHcEopnMBgXnAZI");
        $options->setSecretKey("sandbox-wbwpzKIiplZxI3hh5ALI4FJyAcZKL6kq");
        $options->setBaseUrl("https://sandbox-api.iyzipay.com"); */

        $options->setApiKey("1wI7YwTr0zn9cesTdzznvXkRUT6Hp7dw");
        $options->setSecretKey("s5e6BPMeHgpZUi9IWmukLDM7Vjt07M4r");
        $options->setBaseUrl("https://api.iyzipay.com");
                
        $request = new \Iyzipay\Request\CreateCheckoutFormInitializeRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId("1");
        $request->setPrice($totalprice);
        $request->setPaidPrice($totalprice);
        $request->setCurrency(\Iyzipay\Model\Currency::TL);
        $request->setBasketId($code);
        $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::LISTING);
        $request->setCallbackUrl(route('site.package.result',['id'=>$package->id]));
        $request->setEnabledInstallments(array(2, 3));

        $buyer = new \Iyzipay\Model\Buyer();

        $buyer->setId($code);
        $name = explode(' ',Auth::user()->name);
        $lastname = "";
        foreach ($name as $key => $value) {
            if($key>0){
                $lastname.=$value.' ';
            }
        }
        $buyer->setName($name[0]);
        $buyer->setSurname($lastname);
        $buyer->setGsmNumber(Auth::user()->phone);
        $buyer->setEmail(Auth::user()->email);
        $buyer->setIdentityNumber(Auth::user()->identity_number);
        /* $buyer->setLastLoginDate("2015-10-05 12:43:35");
        $buyer->setRegistrationDate("2013-04-21 15:12:09"); */
        $buyer->setRegistrationAddress(Auth::user()->address);
        /* $buyer->setIp("85.34.78.112"); */
        if (Auth::user()->state->name!="") {
            $buyer->setCity(Auth::user()->state->name);
        } else {
            $buyer->setCity('istanbul');
        }
        
        if (Auth::user()->country->name!="") {
            $buyer->setCountry(Auth::user()->country->name);
        } else {
            $buyer->setCountry('Turkey');            
        }
        
        /* $buyer->setZipCode("34732"); */

        $request->setBuyer($buyer);
        $shippingAddress = new \Iyzipay\Model\Address();
        $shippingAddress->setContactName("Fatura");
        $shippingAddress->setCity(Auth::user()->state->name);
        $shippingAddress->setCountry(Auth::user()->country->name);
        $shippingAddress->setAddress(Auth::user()->address);
        $shippingAddress->setZipCode(Auth::user()->zipcode);
        $request->setShippingAddress($shippingAddress);

        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName("Fatura");
        $billingAddress->setCity(Auth::user()->state->name);
        $billingAddress->setCountry(Auth::user()->country->name);
        $billingAddress->setAddress(Auth::user()->address);
        $billingAddress->setZipCode(Auth::user()->zipcode);
        $request->setBillingAddress($billingAddress);
        
        $basketItems = array();
        $firstBasketItem = new \Iyzipay\Model\BasketItem();
        $firstBasketItem->setId("1");
        $firstBasketItem->setName("Hizmet");
        $firstBasketItem->setCategory1("Paket");
        $firstBasketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
        $firstBasketItem->setPrice($totalprice);
        $basketItems[0] = $firstBasketItem;


        $request->setBasketItems($basketItems); 

        $checkoutFormInitialize = \Iyzipay\Model\CheckoutFormInitialize::create($request, $options);

        $page_path = 'pages/pagepackage/payment';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path);  

        return view($files, compact('package','checkoutFormInitialize'));
    }

    public function result(Package $package, Request $request)
    {
        $options = new \Iyzipay\Options();
        /* $options->setApiKey("sandbox-afXhZPW0MQlE4dCUUlHcEopnMBgXnAZI");
        $options->setSecretKey("sandbox-wbwpzKIiplZxI3hh5ALI4FJyAcZKL6kq");
        $options->setBaseUrl("https://sandbox-api.iyzipay.com"); */

        $options->setApiKey("1wI7YwTr0zn9cesTdzznvXkRUT6Hp7dw");
        $options->setSecretKey("s5e6BPMeHgpZUi9IWmukLDM7Vjt07M4r");
        $options->setBaseUrl("https://api.iyzipay.com");

        $r = $request->all();
        $requestiyzico = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
        $requestiyzico->setLocale(\Iyzipay\Model\Locale::TR);
        $requestiyzico->setToken($r['token']);

        $checkoutForm = \Iyzipay\Model\CheckoutForm::retrieve($requestiyzico, $options);
        
        if($checkoutForm->getpaymentStatus() == 'SUCCESS'){
            
            //dd($dt);
            $insert = array(
                'package_id'        => $package->id,
                'user_id'           => Auth::user()->id,
                'code'              => $checkoutForm->getbasketId(),
                'payment_status_id' => 1,
                'price'             => $checkoutForm->getprice(),
                'paid_price'        => $checkoutForm->getpaidPrice(),
                'discount_price'    => $checkoutForm->getprice(),
                'total_use'         => $package->total_use,
                'installment'       => $checkoutForm->getinstallment(),
                'card_type'         => $checkoutForm->getcardType(),
                'card_association'  => $checkoutForm->getcardAssociation(),
                'card_family'       => $checkoutForm->getcardFamily(),
                'last_four_digits'  => $checkoutForm->getlastFourDigits(),
                'raw_result'        => $checkoutForm->getrawResult(),
                'status'            => 1,
            );
            $payment = Payment::create($insert);

            $payment->user = Auth::user();
            
            $payment->email = env('SYSTEM_MAIL');

            $payment->notify(new PackageNotification($payment));
            return redirect()->route('package.success',['code' => $checkoutForm->getbasketId()]);
            
        }else if($checkoutForm->getpaymentStatus() == 'FAILURE'){
            return redirect()->route('site.package.index',['id'=>$package->id])
                        ->withErrors($checkoutForm->geterrorMessage())
                        ->withInput();
        }else{
            return redirect()->route('site.package.index',['id'=>$package->id]);
        }
    }
    public function success(Request $request)
    {
        $r = $request->all();
        $code = $r['code'];

        $page_path = 'pages/pagepackage/success';             // kontrol edilecek dosya uzantısı
        $files = template_path_check($page_path); 

        return view($files, compact('code'));
    }
}
