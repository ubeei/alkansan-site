<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Session;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $r = $request->all();
        $coupon = Coupon::where('code',$r['coupon'])->where('status',1)->first();
        //session()->put('coupon', $coupon->id);
        if ($coupon) {
            $date = date('Y-m-d H:i');
            if ($coupon->startdate<$date && $date<$coupon->finishdate) {

                
                $countcount = $coupon->appointments()->count();
                if ($coupon->number_of_use>$countcount) {
                    Session::put('coupon', $coupon);
                    Session::save();
                } else {
                    Session::flash('couponerror', "Kodun kullanım adedi dolmuştur.");            
                }  
            } else {
                Session::flash('couponerror', "Kodun kullanım tarihi geçmiştir.");   
            }
        }else{
            Session::flash('couponerror', "Geçerli bir kod giriniz.");
        }
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Session::pull('coupon');
        Session::save();
    }
}
