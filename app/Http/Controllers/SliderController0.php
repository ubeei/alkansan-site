<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lang;
use View;
use App\Http\Requests\SliderPost;
use App\Http\Requests\SlideImagePost;
use Session;
use App\Models\Setting;
use Intervention\Image\ImageManager;
use Storage;
use File;
define("ABSPATH", 1);


class SliderController extends Controller
{
    protected $lang;
    protected $breadcrumb;
    protected $setting;

    public function __construct()
    {
        // Fetch the Site Settings object
        $this->middleware('auth');

        $this->lang = Lang::where('short',\App::getLocale())->first();
        $this->setting = Setting::where('lang_id',$this->lang->id)->pluck('setting_value','setting_veriable')->all();

        $this->breadcrumb[] =array('page' => "Home",'Link' => 'dashboard');

        View::share('breadcrumb', $this->breadcrumb);




		//add_action('plugins_loaded', array( 'RevSliderFront', 'createDBTables' )); //add update checks
       // add_action('plugins_loaded', array( 'RevSliderPluginUpdate', 'do_update_checks' )); //add update checks

        // temporary force no admin to load plugins as for frontend
       /*  if ($this->input->get('client_action') == 'preview_slider' && $this->input->get('only_markup') == 'true') {
            forceNoAdmin(true);
        } */
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $sliders = Slider::where('lang_id',$this->lang->id)->get();

        return view('admin.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderPost $request)
    {
        //dd($request['image']);
        $result = $request->all();
        $button=array();
        foreach ($result['button_name'] as $key => $value) {
            $button[] = array('button_name' => $result['button_name'][$key], 'button_url'=>$result['button_url'][$key] );
        }
        $result['button']  = json_encode($button);
        $result['lang_id'] = $this->lang->id;
        $result['order']   = 100;
        $result['status']  = 1;
        $result['image']   = $request['image'];
      

        $slider = new Slider;
        $slider->create($result);

        Session::flash('status', 1);

        return redirect()->route('slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('admin.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(SliderPost $request, Slider $slider)
    {
        $result = $request->all();

        $button=array();
        foreach ($result['button_name'] as $key => $value) {
            $button[] = array('button_name' => $result['button_name'][$key], 'button_url'=>$result['button_url'][$key] );
        }
        $result['button'] = json_encode($button);

        $slider->update($result);

        Session::flash('status', 1);

        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->delete();
        Session::flash('status', 1);

        return redirect()->route('slider.index');
    }
    public function order(Request $request)
    {
        foreach ($request->order as $key => $value) {
            if (is_numeric($value)) {
                $slider = Slider::find($key);
                $slider->order = $value;
                $slider->update();
            }
        }
        Session::flash('status', 1);

        return redirect()->route('slider.index');
    }
    public function status(Slider $slider)
    {
        if ($slider->status==1) {
            $slider->status=0;
        } else {
            $slider->status=1;
        }
        $slider->update();
        Session::flash('status', 1);

        return redirect()->route('slider.index');

    }

    public function image_upload(SlideImagePost $request)
    {
        //dd($request->all());
        if ($request->file()) {
            $path = $request->file()['upload_image'];
            $coe =$path->getClientOriginalExtension();
            $manager = new ImageManager(array('driver' => 'gd'));
            $resize = $manager->make($path);
            if($this->setting['slider_image']!=""){
                $ex = explode('x',$this->setting['slider_image']);
                $x = ($ex[0]==0) ? null : $ex[0];
                $y = ($ex[1]==0) ? null : $ex[1];
                $resize->resize($x,$y, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }
            $resize->encode($coe);


            $hash = md5($resize->__toString().time());
            $path_url = "upload/slider_img/{$hash}.".$coe;
            $resize->save(public_path($path_url));
            $url = "/" . $path_url;

            return array('error'=>0 ,'url'=>$url);
        }else{
            return array('error'=>1,'message' => 'error');
        }
    }
}
