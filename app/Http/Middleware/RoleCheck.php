<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;

class RoleCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        if (Auth::check()) {
            $role_count = Auth::user()->roles()->where('name',$role)->count();
            if ($role_count>0) {
                return $next($request);            
            }else{
                return redirect()->route('index');
            }

        }

        return redirect()->route('login');
    }
}
