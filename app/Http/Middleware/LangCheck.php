<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Lang;
//use App\Models\Currency;
use Session;
use View;
use Config;

class LangCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(!Session::has('locale'))
        {
            $l = Lang::where('short',app()->getLocale())->first();
            Session::put('locale', $l->short);
        }
        /*if(!Session::has('currency'))
        {   
            $l = Currency::where('iso_code',config('app.currency'))->first();
            Session::put('currency', $l->iso_code);
        }*/

        //dd(Session::get('locale'));
        app()->setLocale(Session::get('locale'));
        config(['app.currency'=>Session::get('currency')]);
        config(['app.locale'=>Session::get('locale')]);
        

        //$langs = Lang::all();
        $langs = Lang::where('status', 1)->get();


        $lang = Lang::where('short',\App::getLocale())->first();
        /*$currencies = Currency::where('status',1)->get();
        $currency = Currency::where('iso_code',config('app.currency'))->first();*/
        View::share('langs', $langs);
        View::share('lang', $lang);
        /*View::share('currencies', $currencies);
        View::share('currency', $currency);*/
        
        
        return $next($request);
        
    }
}
