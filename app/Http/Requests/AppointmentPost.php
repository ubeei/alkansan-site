<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
            return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "date"              => "required",
            "time"              => "required",
            "extra"             => "required",
            "note"              => "",
        ];
    }
    public function attributes(){

        return [
            "date"              => trans('site.appointmnent_date'),
            "time"              => trans('site.appointmnent_time'),
            "extra"             => trans('site.appointmnent_time'),
            "note"              => trans('site.appointmnent_note'),
        ];
    }
    
}
