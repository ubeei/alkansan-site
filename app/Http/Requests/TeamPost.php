<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 
            'image'             => 'required',
            "page_id"           => 'required',
            "first_name"        => 'required',
            "last_name"         => 'required',
            "department"        => 'required',
            "email"             => 'required|email',
            "phone"             => '',
            "linkedin"          => '',
            "googleplus"        => '',
            "skype"             => '',
        ];
    }
    public function attributes(){

        return [
            'image'             => 'Image',
            'page_id'           => 'Page ID',
            'first_name'        => 'First Name ',
            'last_name'         => 'Last Name',
            'department'        => 'Department',
            'email'             => 'E-mail',
            'phone'             => 'Phone',
            'linkedin'          => 'Linkedin',
            'googleplus'        => 'Google Plus',
            'skype'             => 'Skype',
        ];

   }
}
