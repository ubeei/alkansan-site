<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'namesurname'   => 'required',
            "phone"         => '',
            "email"         => 'required|email',
            "massage"       => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ];
    }

    public function attributes(){

        return [
            'namesurname'       => trans('site.contact_namesurname'),
            'phone'             => trans('site.contact_phone'),
            'email'             => trans('site.contact_email'),
            'massage'           => trans('site.contact_massage'),
            'g-recaptcha-response' => trans('site.g_recaptcha_respons')
        ];

   }
}
