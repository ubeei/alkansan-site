<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'meta_desc'     => '',
            'meta_keyword'  => '',
            'code'          => 'required',
            'brand'         => 'required',
            'short_description' => 'required',
            'pdf'           => 'required',
            'family_pdf'    => 'required',
            'description'   => 'required',
            'images'        => 'required',
            'property'      => 'required',
            'category_id'   => 'required',
        ];
    }

    public function attributes(){

        return [
            'name'          => 'Ürün Adı',
            'meta_desc'     => 'Meta Desc',
            'meta_keyword'  => 'Meta Keyword',
            'code'          => 'Ürün Kodu',
            'brand'         => 'Marka',
            'short_description' => 'Kısa Açıklama',
            'pdf'           => 'PDF',
            'family_pdf'    => 'PDF Ailesi',
            'description'   => 'Açıklama',
            'images'        => 'Resimler',
            'property'      => 'Özellik',
            'category_id'   => 'Kategori',
        ];
    }
}
