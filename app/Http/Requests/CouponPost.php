<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "code"              => 'required',
            "coupon_type_id"    => 'required',
            "value"             => 'required',
            'number_of_use'     => 'required',
            'startdate'         => 'required',
            'finishdate'        => 'required',
        ];
    }

    public function attributes(){

        return [
            'code'              => 'Kuppn',
            'coupon_type_id'    => 'Kupon Tipi',
            'value'             => 'Değer',
            'number_of_use'     => 'Kullanım Adeti',
            'startdate'         => 'Başlangıç Tarihi',
            'finishdate'        => 'Bitiş Tarihi',
        ];
    }
}
