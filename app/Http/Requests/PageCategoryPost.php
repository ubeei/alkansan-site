<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageCategoryPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "title"             => 'required',
            "meta_desc"         => 'required',
            "meta_keyword"      => 'required',
            "description"       => 'required',
            'image'             => 'required',
            'pdf'               => '',
            "page_id"           => 'required'
        ];
    }
    public function attributes(){

        return [
            'title'             => 'Title',
            'meta_desc'         => 'Meta Desc.',
            'meta_keyword'      => 'Meta Key.',
            'description'       => 'Açıklama',
            'image'             => 'Image',
            'pdf'               => 'PDF',
            'page_id'             => 'Page',
        ];
    }
}
