<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "title"             => 'required',
            "meta_desc"         => 'required',
            "meta_keyword"      => 'required',
            "description"       => '',
            'image'             => '',
            'pdf'               => '',
            "parent_id"         => 'required'
        ];
    }
    public function attributes(){

        return [
            'title'             => 'Başlık',
            'meta_desc'         => 'Meta Desc.',
            'meta_keyword'      => 'Meta Key.',
            'description'       => 'Açıklama',
            'image'             => 'Resim',
            'pdf'               => 'PDF',
            'parent_id'         => 'Kategori',
        ];
    }
}
