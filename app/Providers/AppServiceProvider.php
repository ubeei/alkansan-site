<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        $this->app["form"]->component('dText',template_path_check('/form_components/form_components/text'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dDateTime',template_path_check('/form_components/form_components/datetime'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dSubmit',template_path_check('/form_components/form_components/submit'),['name','label_name']);
        $this->app["form"]->component('dTextarea',template_path_check('/form_components/form_components/textarea'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dSelect',template_path_check('/form_components/form_components/select'),['name','label_name','list' => null,'placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dSelectm',template_path_check('/form_components/form_components/select_m'),['name','label_name','list' => null,'placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dRadiobox',template_path_check('/form_components/form_components/radiobox'),['name','label_name','list' => null,'attributes']);
        $this->app["form"]->component('dCheckbox',template_path_check('/form_components/form_components/checkbox'),['name','label_name','list' => null,'attributes']);
        $this->app["form"]->component('dCheckboxone',template_path_check('/form_components/form_components/checkboxone'),['name','label_name','value' => null,'is_checked'=>null,'attributes']);
        $this->app["form"]->component('dFile',template_path_check('/form_components/form_components/file'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dTextnumber',template_path_check('/form_components/form_components/numbertext'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);


        $this->app["form"]->component('dText2',template_path_check('/form_components/form_components2/text'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dDateTime2',template_path_check('/form_components/form_components2/datetime'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dSubmit2',template_path_check('/form_components/form_components2/submit'),['name','label_name']);
        $this->app["form"]->component('dSelectm2',template_path_check('/form_components/form_components2/selectm'),['name','label_name','list' => null,'placeholder'=> null,'value' => null,'attributes']);

        $this->app["form"]->component('dTextarea2',template_path_check('/form_components/form_components2/textarea'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dSelect2',template_path_check('/form_components/form_components2/select'),['name','label_name','list' => null,'placeholder'=> null,'value' => null,'attributes']);
        $this->app["form"]->component('dRadiobox2',template_path_check('/form_components/form_components2/radiobox'),['name','label_name','list' => null,'attributes']);
        $this->app["form"]->component('dCheckbox2',template_path_check('/form_components/form_components2/checkbox'),['name','label_name','list' => null,'attributes']);
        $this->app["form"]->component('dPassword2', template_path_check('/form_components/form_components2/password'), ['name','label_name','placeholder'=> null, 'value' => null, 'attributes' => []]);

        
        $this->app["form"]->component('dFile2',template_path_check('/form_components/form_components2/file'),['name','label_name','placeholder'=> null,'value' => null,'attributes']);

    }
}
