<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Payment extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id', 'package_id', 'payment_status_id', 'code', 'total_use' ,'price', 'paid_price', 'discount_price' , 'installment', 'card_type' ,'card_association', 'card_family', 'last_four_digits', 'raw_result','status'
    ];
    public function packages()
    {
        $this->hasMany('App\Models\Package');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function package()
    {
        return $this->belongsTo('App\Models\Package');
    }
}
