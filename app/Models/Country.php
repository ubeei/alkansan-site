<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Country extends Model
{
    use Cachable;
    //
    public $timestamps = false;   

    public function states()
    {
        return $this->hasMany('App\Models\State');
    }
}
