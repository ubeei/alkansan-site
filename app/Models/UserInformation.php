<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    //
    public $timestamps = false;

    protected $fillable =  ["user_id", "company_information_id" , "job_title", "agency_name" , "mobile", "office_phone", "country_id", "state_id", "city_id", "postal_code", "address", "site", "trade_association", "trade_association_no", 'discount_rate'];

    public function company_information()
    {
        return $this->hasOne('App\Models\CompanyInformation', 'id', 'company_information_id');
    }
}
