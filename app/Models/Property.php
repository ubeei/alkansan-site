<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = ["lang_id","name","order","status" ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
} 
