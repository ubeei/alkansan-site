<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    //
    protected $fillable = ["title","content","status","order",'page_content_type_id','html','css'];

    public function page_content_type()
    {
        return $this->belongsTo('App\Models\PageContentType');
    }
    public function content_files()
    {
        return $this->hasMany('App\Models\ContentFile');

    }
}
