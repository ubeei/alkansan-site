<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $fillable = ["title","path","content","lang_id","status"];


    public function images()
    {
        return $this->hasMany('App\Models\BlogImages');

    }
}
