<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    //
    protected $fillable = ["lang_id","page_id","image","name","url",'order','status'];

    /*public function contents()
    {
        return $this->morphMany('App\PageContent','page_contentable');
    }*/
}
