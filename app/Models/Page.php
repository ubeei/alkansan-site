<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $fillable = ["lang_id","page_id","title","meta_desc","meta_keyword","redirect_url","page_type_id","homepage","topmenu","submenu",'page_img','status','order'];

    public function page_type()
    {
        return $this->belongsTo('App\Models\PageType');
    }
    public function subpages() {
        return $this->hasMany(Page::class, 'page_id', 'id')->where('status',1)->orderBy('order');
    }

    public function toppages() {
        return $this->hasOne(Page::class, 'id', 'page_id')->where('status',1)->orderBy('order');
    }
    public function contents()
    {
        return $this->morphMany('App\Models\PageContent','page_contentable')->where('status',1)->orderBy('order');
    }
    public function contentsadmin()
    {
        return $this->morphMany('App\Models\PageContent','page_contentable')->orderBy('order');
    }

    public function teams()
    {
        return $this->hasMany('App\Models\Team')->where('status',1)->orderBy('order');
    }

    public function sponsors()
    {
        return $this->hasMany('App\Models\Sponsor')->where('status',1)->orderBy('order');
    }
    public function tourgroups()
    {
        return $this->hasMany('App\Models\TourGroup')->where('status',1)->orderBy('order');
    }
    public function pagecategories()
    {
        return $this->hasMany('App\Models\PageCategory')->where('status',1)->orderBy('order');
    }
    public function blogs()
    {
        return $this->morphMany('App\Models\PageContent','page_contentable')->where('status',1)->orderBy('order');
    }

}
/*class SubPage extends Model
{
    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}*/
