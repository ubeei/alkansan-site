<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [ 'code','coupon_type_id','value','number_of_use','startdate','finishdate','status'];

    public function appointments()
    {
        return $this->hasMany('App\Models\Appointment');

    }
}
