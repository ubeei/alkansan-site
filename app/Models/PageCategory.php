<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageCategory extends Model
{
    //
    protected $fillable = ["lang_id","page_id","meta_desc","meta_keyword","title","image","pdf","description",'order','status'];

    public function pagecategorydetails()
    {
        return $this->hasMany('App\Models\PageCategoryDetail')->where('status',1)->orderBy('order');
    }
}
