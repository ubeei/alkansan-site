<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use \Swis\Laravel\Fulltext\Indexable;
    //
    protected $fillable = [
        'lang_id','name','slug','meta_desc','meta_keyword','code','brand','short_description','pdf','family_pdf','description','order','status'
    ];

    protected $indexContentColumns = ['name','meta_keyword'];
    protected $indexTitleColumns = ['name'];

    public function product_images()
    {
        return $this->hasMany(ProductImage::class);
    }
    public function product_properties()
    {
        return $this->hasMany(ProductProperty::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
