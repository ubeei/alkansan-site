<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Appointment extends Model
{
    use Notifiable;
    protected $table = 'appointments';
    protected $fillable = [
        'appointment_time', 'user_id', 'code', 'items', 'note', 'price', 'paid_price', 'installment', 'card_type' ,'card_association', 'card_family', 'last_four_digits', 'raw_result','status', 'coupon_id','discount'
    ];
    protected $dates = ['appointment_time'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon');
    }
}
