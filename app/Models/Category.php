<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = ["lang_id","parent_id","meta_desc","meta_keyword","title","image","pdf","description",'order','status'];

    public function pagecategorydetails()
    {
        return $this->hasMany('App\Models\PageCategoryDetail')->where('status',1)->orderBy('order');
    }

    public function subcategories() {
        return $this->hasMany(Category::class, 'parent_id', 'id')->where('status',1)->orderBy('order');
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class);
    }
    public function products() {
        return $this->belongsToMany(Product::class)->where('status',1)->orderBy('order');
    }
}
