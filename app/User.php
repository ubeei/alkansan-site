<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'identity_number', 'country_id', 'state_id', 'address', 'zipcode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany("App\Models\Role","role_user");
    }

    public static function role_check($role)
    {
        $role = $this->roles()->where('user_id',$this->id)->where('name',$role)->count();
        return ($role) ? true : false ;
    }

    public function user_informations()
    {
        return $this->hasOne('App\Models\UserInformation');
    }
    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }
    public function appointments()
    {
        return $this->hasMany('App\Models\Appointment');
    }
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

}
